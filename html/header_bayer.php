<?php
$countCart = 0;
if (isset($_COOKIE['cart_code'])) {
    $res = $this->db->query('SELECT `quantity` FROM `cart` WHERE `cart_code`="'.$this->db->escape($_COOKIE['cart_code']).'"');
    foreach ($res->rows as $row) {
        $countCart+= $row['quantity'];
    }
}
?>

<!-- >>>>> HEADER >>>>> -->
<div class="header">
    <div class="headerLogo"></div>
    <div class="headerItem navCategory <?=((!isset($_GET['option']) || $_GET['option']=='main') ? 'active' : '')?>" onclick="location.href='/'"><span id="countCat" class="counter"></span></div>
    <div class="headerItem navGoods <?=((isset($_GET['option']) && $_GET['option']=='catalog') ? 'active' : '')?>" onclick="location.href='/?option=catalog'">
        <span id="countCatalog" class="counter"><?=$countCart?></span>
    </div>
    <div class="headerItem navShops <?=((isset($_GET['option']) && $_GET['option']=='cart') ? 'active' : '')?>"  onclick="location.href='/?option=cart'"></div>
    <div class="headerMenu" onclick="toggle(modalMenu)"><div></div></div>
</div>
<div class="headerCover"></div>
<!-- <<<<< HEADER <<<<< -->

<script>
    var url_option = "<?=@$_GET['option']?>";
    $(document).ready(ads());
    function ads() {
        if($('#countCatalog').html()=='0') {
            $('#countCatalog').css('display', 'none');
            $('.navShops').css('display', 'none');
        }
        recountCat();
    }

    function recountCat() {
        count =0;
        var brands = $.cookie("brands");
        if (brands != undefined) {
            brands = JSON.parse(brands);
            count = brands.length;
        }

        var cats = $.cookie("cats");
        if (cats != undefined) {
            cats = JSON.parse(cats);
            if (cats.length>0 && url_option=='catalog') {
                $('.navGoods').append('<div class="navFilter" onclick="toggle(modalFilter);"></div>');
                $('.navGoods').removeAttr('onclick');
            }
            count = count+cats.length;
        }


        if (count>0)  {
            $("#countCat").css("display","block");
            $("#countCat").html(count);
            $('.navGoods').css('display', 'block')
        }
        else {
            $("#countCat").css("display","none");
            $('.navGoods').css("display","none");
        }
    }
</script>