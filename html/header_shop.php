<?php
$res = $this->db->query('select count(id) as count from orders where viewed=0 and id_shop='.$_SESSION['id_shop']);
$new_orders = $res->row['count'];

$res = $this->db->query('select count(id) as count from prices where (quantity=0 OR `price`=0) and id_shop='.$_SESSION['id_shop']);
$zero = $res->row['count'];

// if price list is off or
//   the price was not set for one product at least or
//   all products` quantity is zero
// then we show  the message bellow
$res = $this->db->query('select `price_on` FROM `shop` WHERE id='.$_SESSION['id_shop']);
$price_on = $res->row['price_on'];
$res = $this->db->query('select `id` from prices where quantity>0 AND `price`>0 and id_shop='.$_SESSION['id_shop'].' LIMIT 1');
$at_least_one = $res->num_rows;
?>


<!-- >>>>> HEADER >>>>> -->
<div class="header">
    <div class="headerLogo"></div>
    <div class="headerItem navOrders <?php if ($_GET['option']=='orders') echo 'active'; ?>" onclick="location.href='/?option=orders'"><?php if ($new_orders>0) { ?><span class="counter"><?=$new_orders?></span><?php } ?></div>
    <div class="headerItem navPrice  <?php if ($_GET['option']=='goods')  echo 'active'; ?>" onclick="location.href='/?option=goods'"><?php if ($zero>0) { ?><span class="counter"><?=$zero?></span><?php } ?></div>
    <div class="headerItem navCatalog  <?php if ($_GET['option']=='brands' || $_GET['option']=='addgoods')  echo 'active'; ?>" onclick="location.href='/?option=brands'"></div>
    <div class="headerMenu" onclick="toggle(modalMenu)"><div></div></div>
</div>
<div class="headerCover"></div>
<?php if ($price_on===0 || $at_least_one===0) {?>
<div class="alert" id="modalAlert" onclick="toggle(modalAlert)">Прайс-лист выключен или закончились товары</div>
<?php } ?>
<!-- <<<<< HEADER <<<<< -->