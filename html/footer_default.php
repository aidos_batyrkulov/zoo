<script>
    var page_number = 2;
    var page_loading = false;
    var page_end = false;

    window.onscroll = function ()  {
        var clientHeight = document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight;
        var documentHeight = document.documentElement.scrollHeight ? document.documentElement.scrollHeight : document.body.scrollHeight;
        var scrollTop = window.pageYOffset ? window.pageYOffset : (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

        if(pagination && (((documentHeight - clientHeight)-((documentHeight - clientHeight)/100*35)) <= scrollTop)) {
            if (page_loading==false && page_end==false) {
                page_loading = true;
                $.ajax({
                    url: $(location).attr('href'),
                    data: "page=" + page_number,
                    success: function (data, textStatus, XHR) {
                        if (data.indexOf("PAGE_END") + 1) {
                            page_end = true;
                        } else {
                            page_number++;
                            $(addPaginationNewElementsTo).append(data);
                        }
                    },
                    complete: function( XHR, textStatus ) {page_loading= false;}
                });
                if(callBackFunAfterPagination!=false) callBackFunAfterPagination();
            }
        }
    }
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134640973-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-134640973-1');
</script>
</body>
</html>