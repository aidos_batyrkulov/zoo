<?php
if (session_status()===PHP_SESSION_ACTIVE) {
    if (!empty(@$_SESSION['msg_ok'])) $msg = $_SESSION['msg_ok'];
    elseif (!empty(@$_SESSION['msg_error'])) $msg = $_SESSION['msg_error'];
    unset($_SESSION['msg_ok']);
    unset($_SESSION['msg_error']);
}
?>
<!DOCTYPE html><html lang="ru">
<!-- head_default-BEGIN -->
<head>
    <title><?php echo $this->metaTitle;?></title>
    <meta name="description" content="<?php echo $this->metaDescription; ?>">
    <link href="favicon.png" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1, minimum-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta property="og:title" content="Онлайн сервис поиска и заказа зоотоваров" />
    <meta property="og:description" content="Онлайн-сервис поиска и заказа товаров в зоомагазинах Украины" />
    <meta property="og:image" content="http://zooskop.com/og.png" />
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">
    <meta property="og:url" content="https://zooskop.com/">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="ZooSkop.com"/>

    <?php if (@$_SESSION['role']== 'shop') { ?>
        <link href="/css/shop.css" type="text/css" rel="stylesheet">
    <?php } else if (@$_SESSION['role']=='user') { ?>
        <link href="/css/user.css" type="text/css" rel="stylesheet">
    <?php } else if (@$_SESSION['role']=='admin') { ?>
        <link href="/css/admin.css" type="text/css" rel="stylesheet">
    <?php } else  { ?>
        <link href="/css/style.css" type="text/css" rel="stylesheet">
        <link href="/css/loading.css" type="text/css" rel="stylesheet">
    <?php }  ?>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <?php echo $this->pageScript; ?>

    <script>
        onerror = errorHandler
        function errorHandler(message, url, line)
        {
            out  = "К сожалению, обнаружена ошибка.\n\n";
            out += "Ошибка: " + message + "\n";
            out += "URL: "   + url + "\n";
            out += "Строка: "  + line + "\n\n";
            out += "Щелкните на кнопке OK для продолжения работы.\n\n";
            alert(out);
            return true;
        }

        addPaginationNewElementsTo = 'body';
        callBackFunAfterPagination = false;

        dont_close =  false; // except ajax, this var can be used for everything
        ajaxing = false;
        pagination = true;
        window.onbeforeunload = function(e) {   if (ajaxing==true || dont_close==true) return 'Подождите немного, данные сохраняются!';   };

        $(document).ajaxError(function() {alert('Проверьте подключение к интернету')});
        $(document).ajaxSend(function() { ajaxing =true; });
        $(document).ajaxStop(function() { ajaxing =false; });
        $.ajaxSetup({
            dataType: 'text',
            cache: false,
            contentType: 'application/x-www-form-urlencoded',
            type: 'POST',
            timeout: 10000,
            global: true
        });

        $(document).ready(function () {
            <?php if (!empty($msg)) {  ?>
            alert('<?=$msg?>');
            <?php }  ?>
        });
    </script>

</head>
<!-- head_default-END-->

<body>

