<?php
abstract class frame {
	protected $db;
	protected $mail;
	protected $security;
	protected $geo;
	protected $sms;

	protected $metaTitle;
	protected $metaDescription;
	protected $pageScript;
	protected $page=1;


	public function __construct () {
		$this->db= db::getObject(config::$db_host,config::$db_user,config::$db_password,config::$db_name,config::$db_charset);
		$this->mail= new mail();
		$this->security= new security();
		$this->geo = new geo();
		$this->sms = new sms();
		if (isset($_POST['page']))
		    $this->page =  intval($_POST['page']);
	}

	protected function getLimitByPage() {
	    $page = $this->page;
	    $page.='0';
	    $start = intval($page)-10;

	    return ' LIMIT '.$start.', 10 ';
    }


	protected function get_head() {
		include("html/head_default.php");
	}

	abstract function get_content();

	protected function get_header() {
	    if (@$_SESSION['role']=='admin') {
            include("html/header_admin.php");
        } else if (@$_SESSION['role']=='user') {
            include("html/header_user.php");
        } else if (@$_SESSION['role']== 'shop') {
            include("html/header_shop.php");
        } else {
            include("html/header_bayer.php");
        }
	}



	protected function get_menu() {
        if (!isset($_SESSION['role'])) {
            include("html/menu_bayer.php");
        } else if ($_SESSION['role']=='user') {
            include("html/menu_user.php");
        } else if ($_SESSION['role']=='shop') {
            include("html/menu_shop.php");
        } else if ($_SESSION['role']=='admin') {
            include("html/menu_admin.php");
        }
	}

	protected function get_footer() {
		include("html/footer_default.php");
	}

	public function get_body() {
		$this->inspection();
		ob_start();
		$this->get_content();
		$content = ob_get_contents();
        ob_end_clean();
		
		ob_start();
		$this->get_head();
		$head = ob_get_contents();
        ob_end_clean();
		
		ob_start();
		$this->get_header();
		$header = ob_get_contents();
        ob_end_clean();


		ob_start();
		$this->get_menu();
		$menu = ob_get_contents();
        ob_end_clean();
		
		ob_start();
		$this->get_footer();
		$footer = ob_get_contents();
        ob_end_clean();
		
		echo $head;
		echo $header;
        echo $menu;
	 	echo $content;
		echo $footer;
	}
	
	
	private function inspection() {/*
        if (@$_SESSION['role']=='admin') {

        } else if (@$_SESSION['role']=='user') {
			
		} else if (@$_SESSION['role']== 'shop') {
			
		} else {
			if (isset($_COOKIE['cart_code']) && @$_GET['option']!='confirm') {
				$res= $this->db->query('select id from 	`orders` where confirmed=0 and cart_code="'.$this->db->escape($_COOKIE['cart_code']).'"  order by id desc limit 1');
				if ($res->num_rows>0) {
					header('location: /?option=confirm');
					exit;
				}
			}
		}*/
	}
}
