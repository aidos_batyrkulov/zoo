<?php
class info extends frame {
    private $shop;

    public function get_content() {
        $this->metaTitle='Магазин | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        echo $this->showData();
    }

    

    private function getData() {
        $res = $this->db->query('select * from shop where id='.$_SESSION['id_shop']);
        $this->shop = $res->row;

        $res = $this->db->query('select * from discounts where id_shop='.$_SESSION['id_shop']);
        $this->shop['discounts'] = $res->rows;
    }

    
    private function  showData() {
        ob_start();
        ?>
        <?php
        $shop= $this->shop;
        ?>
                <div class="item">
                    <span class="shopName"><?=$shop['name']?><sup><?=$shop['id']?></sup></span>
                    <div class="rate">
                        <div style="width:75%"></div>
                        <span>75</span>
                    </div>
                    <span class="shopAddress"><?=$shop['city']?>, <?=$shop['street_home']?></span>
                </div>
                <?php if ($shop['type']=='uni' || $shop['ex_radius']>0 || $shop['td_get_order_until']>0 || $shop['tm_get_order_until']>0 || $shop['ukr_pay']!='') { ?>
                    <div class="item">
                        <?php if ($shop['type']=='uni') { ?>
                        <div class="shopDelivery pickup">
                            <div class="first"></div>
                            <div class="second">
                                <span><?=$shop['city']?>, <?=$shop['street_home']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['ex_radius']>0) { ?>
                        <div class="shopDelivery express">
                            <div class="first"></div>
                            <div class="second">
                                <span>радиус <?=$shop['ex_radius']?>м</span><br>
                                <span><?=$shop['ex_min_summ']?></span>
                                <span><?=$shop['ex_price']?></span>
                                <span><?=$shop['ex_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['td_get_order_until']>0) { ?>
                        <div class="shopDelivery today">
                            <div class="first"></div>
                            <div class="second">
                                <?php if (mb_strlen($shop['td_get_order_until'])==3) $shop['td_get_order_until'] = '0'.$shop['td_get_order_until']; ?>
                                <span>прием заказов до <?=mb_substr($shop['td_get_order_until'],0,2);?><sup><?=mb_substr($shop['td_get_order_until'],2,2);?></sup></span><br>
                                <span><?=$shop['td_min_summ']?></span>
                                <span><?=$shop['td_price']?></span>
                                <span><?=$shop['td_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['tm_get_order_until']>0) { ?>
                        <div class="shopDelivery tomorrow">
                            <div class="first"></div>
                            <div class="second">
                                <?php if (mb_strlen($shop['tm_get_order_until'])==3) $shop['tm_get_order_until'] = '0'.$shop['tm_get_order_until']; ?>
                                <span>прием заказов до <?=mb_substr($shop['tm_get_order_until'],0,2);?><sup><?=mb_substr($shop['tm_get_order_until'],2,2);?></sup></span><br>
                                <span><?=$shop['tm_min_summ']?></span>
                                <span><?=$shop['tm_price']?></span>
                                <span><?=$shop['tm_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['ukr_pay']!='') { ?>
                        <div class="shopDelivery post">
                            <div class="first"></div>
                            <div class="second">
                                <span><?=(($shop['ukr_pay']=='after') ? 'наложенный платеж' : 'предоплата')?></span><br>
                                <span><?=$shop['ukr_min_summ']?></span>
                                <span>тариф+<?=$shop['ukr_price']?></span>
                                <span><?=$shop['ukr_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                    </div>
                <?php } ?>

                <?php if (count($shop['discounts'])>0) { ?>
                    <div class="item">
                    <?php foreach ($shop['discounts'] as $discount) { ?>
                    <div class="shopDiscount">
                        <span><?=$discount['discount']?></span>
                        <span><?=$discount['summ']?></span>
                    </div>
                    <?php } ?>
                    </div>
                <?php } ?>
        <?php
        $html = ob_get_clean();
        return $html;
    }



    public static function tryToActive($id_shop) {
        $db= db::getObject(config::$db_host,config::$db_user,config::$db_password,config::$db_name,config::$db_charset);
        $res = $db->query('SELECT `active` FROM `shop` WHERE `id`='.intval($id_shop).' LIMIT 1');
        if ($res->num_rows>0 && $res->row['active']==0) {
            $res1 = $db->query('SELECT `id` FROM `prices` WHERE `id_shop`='.intval($id_shop).' AND `price`>0 AND `quantity`>0 LIMIT 1');
            $res2 = $db->query('SELECT * FROM `shop` WHERE `id`='.intval($id_shop).' LIMIT 1');
            $res3 = $db->query('SELECT COUNT(`id`) AS `count` FROM `orders` WHERE `id_shop`='.intval($id_shop).' AND `viewed`=0 AND `delType`<>"shop"');
            $res4 = $db->query('SELECT `balance`, `active` FROM `seller` WHERE `id`=(SELECT `id_seller` FROM `shop` WHERE `id`='.intval($id_shop).' LIMIT 1 )');

            if ($res1->num_rows>0 && $res3->row['count']<config::$shop_viewed_limit
                && $res2->row['price_on']==1 && $res4->row['active']==1
                && intval($res4->row['balance'])>(config::$seller_balance_minus_limit+config::$order_cost_for_seller)
                && ($res2->row['d1_start']>0 || $res2->row['d2_start']>0 || $res2->row['d3_start']>0 || $res2->row['d4_start']>0 || $res2->row['d5_start']>0 || $res2->row['d6_start']>0 || $res2->row['d7_start']>0 )
                && ($res2->row['type']=="uni" || $res2->row['ex_radius']>0 || $res2->row['td_get_order_until']>0 || $res2->row['tm_get_order_until']>0 || $res2->row['ukr_pay']!=="")
            ) {
                $db->query('UPDATE `shop` SET `active`=1 WHERE `id`='.intval($id_shop));
            }
        }
    }
}