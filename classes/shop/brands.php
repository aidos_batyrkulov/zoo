<?php
class brands extends frame {
    private $brands;

    public function get_content() {
        $this->metaTitle='Бренды | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        echo $this->showData();

    }

    private function getData() {
        $res = $this->db->query('select `id` from `brand`  ORDER BY `level` DESC');
        $this->brands= $res->rows;
    }


    private function showData() {
        ob_start();
        ?>
        <div class="partBrand brand">
        <?php foreach($this->brands as $br) { ?>
            <img src="brand/<?=$br['id']?>.png" onclick="location.href='/?option=addgoods&brand=<?=$br['id']?>'">
        <?php } ?>
        </div>
        <?php
        $html = ob_get_clean();
        return $html;

    }


}