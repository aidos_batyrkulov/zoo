<?php
class goods extends frame {
    private $shop;
    private $pds=[];
    private $count;


    public function get_content() {
        include_once 'info.php';
        require_once 'PHPExcel.php';
        if (isset($_POST['pd_code'])) $this->del_upd();
        if (isset($_POST['price_on'])) $this->price_on();
        if (isset($_FILES['ex'])) $this->excel();
        if (isset($_POST['load'])) $this->load_ex();
        if (isset($_POST['clear_price'])) $this->clear_price();

        $this->metaTitle='Товары | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        $this->initData();
        $this->setData();
        echo $this->showData();

    }



    private  function getData() {
        $res = $this->db->query('select `price_on` FROM `shop` WHERE `id`='.$_SESSION['id_shop']);
        $this->shop = $res->row;

        $res = $this->db->query('select COUNT(`id`) AS `count` from `prices` where `id_shop`='.$_SESSION['id_shop']);
        $this->count = $res->row['count'];

        $res = $this->db->query('select * from `prices` where `id_shop`='.$_SESSION['id_shop'].' ORDER BY `id` DESC '.$this->getLimitByPage());
        $this->pds = $res->rows;
        // get pd name,  option name, brand name, id
        foreach ($this->pds as &$pd) {
            $res = $this->db->query('select name from `group` where id=(select id_subgr from pd_subgrs where pd_code="'.$pd['pd_code'].'")');
            $pd['subgr_name'] = $res->row['name'];
            $res = $this->db->query('select id,name,photo from `product` where id=(select id_pd from pd_subgrs where pd_code="'.$pd['pd_code'].'")');
            $pd['name'] = $res->row['name'];
            $pd['id']= $res->row['id'];
            $pd['photo']= $res->row['photo'];
            $res = $this->db->query('select name from `brand` where id=(select brand from product where id=(select id_pd from pd_subgrs where pd_code="'.$pd['pd_code'].'"))');
            $pd['brand_name'] = $res->row['name'];
        }
        unset($pd);
    }


    private  function initData() {

    }

    private function showData() {
        ob_start();
        ?>
        <?php if ($this->page==1) { ?>
        <div class="btnCover">
            <form method="post" action="/?option=goods" enctype="multipart/form-data">
                <div class="btnChoice">
                    <label><input name="ex" type="file"></label>
                </div>
                <div class="btnLoad">
                    <input type="submit" value=""/>
                </div>
            </form>
            <form method="post" id="form-load" action="/?option=goods">
                <div class="btnDownload">
                    <input type="hidden" name="load"/>
                    <input type="button" onclick="document.getElementById('form-load').submit()"   value=""/>
                </div>
            </form>
            <form method="post" id="form-delete" action="/?option=goods" >
                <div class="btnDelete">
                    <input type="hidden" name="clear_price"/>
                    <input type="button"  onclick="document.getElementById('form-delete').submit()"   value=""/>
                </div>
            </form>
        </div>


        <div class="item">
            <span class="price"><?=$this->count?></span>
            <label class="checkbox"><input id="main-check" type="checkbox"  <?=(($this->shop['price_on']==1) ? 'checked="checked"' : '')?>/><div class="checked"></div></label>
            <?php } ?>
            <?=(($this->page>1 && count($this->pds)==0) ? 'PAGE_END' : '')?>
            <?php foreach ($this->pds as $key => $pd ) { ?>
                <div class="table <?=(($pd['quantity']==0) ? 'empty' : '')?>" onclick="setModalData(this)" data-price="<?=$pd['price']?>" data-quantity="<?=$pd['quantity']?>" data-pd-code="<?=$pd['pd_code']?>" data-id-pd="<?=$pd['id']?>" data-brand-name="<?=$pd['brand_name']?>" data-pd-name="<?=$pd['name']?>" data-subgr-name="<?=$pd['subgr_name']?>" data-pd-photo="<?=$pd['photo']?>">
                    <div class="no"><?=($key+1)?></div>
                    <div class="name">
                        <span><?=$pd['brand_name']?></span>
                        <span><?=$pd['name']?></span>
                        <span><?=$pd['subgr_name']?></span>
                    </div>
                    <div class="details">
                        <span><?=$pd['pd_code']?></span>
                        <span id="qtty"><?=$pd['quantity']?></span>
                        <span id="price"><?=$pd['price']?></span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->page>1) exit; ?>
        </div>


        <div class="priceEdit" id="modalEdit" style="display:none;" >
            <div class="delete" onclick="deletePd()"></div>
            <div class="close" onclick="toggle(modalEdit)"></div>
            <img>
            <div class="specifications">
                <div>
                    <span id="modalBrand"></span>
                    <span id="pd-name" ></span>
                </div>
                <div>
                    <span id="pd-code"></span>
                    <span id="pd-subgr-name"></span>
                </div>
            </div>
            <input required="required" id="qtty" type="text" placeholder="1 шт."/>
            <input required="required" id="price" type="text" placeholder="177,89 грн"/>
            <input type="submit" value="Сохранить" onclick="updatePd()"/>
        </div>


        <script>
            var pd_code;
            var price;
            var quantity;
            $(document).ready(go());

            function go() {
                addPaginationNewElementsTo = '.item';
                callBackFunAfterPagination = window['reNumbering'];
                $('body').on("click", "#main-check", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=goods', 'price_on='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                });
            }

            function setModalData(el) {
                el = $(el);
                pd_code = el.attr('data-pd-code');
                price = el.attr('data-price');
                quantity = el.attr('data-quantity');

                m = $('#modalEdit');
                m.find('img').attr('src','catalog/'+el.attr('data-pd-photo')+'.png');
                m.find('#modalBrand').html(el.attr('data-brand-name'));
                m.find('#pd-name').html(el.attr('data-pd-name'));
                m.find('#pd-subgr-name').html(el.attr('data-subgr-name'));
                m.find('#pd-code').html(pd_code);
                m.find('#qtty').val(quantity);
                m.find('#price').val(price);

                toggle(modalEdit);
            }

            function deletePd() {
                the_ajax('delete=on');
            }

            function updatePd() {
                m = $('#modalEdit');
                quantity = m.find('#qtty').val();
                price = m.find('#price').val();
                the_ajax('update=on');
            }

            function the_ajax(label_data) {
                $.post('/?option=goods', label_data+'&pd_code='+pd_code+'&qtty='+quantity+'&price='+price, function () {
                    var el = $('div').find("[data-pd-code='" + pd_code + "']");
                    if (label_data==='delete=on') {
                        el.css('display', 'none');
                    } else {
                        el.find('#qtty').html(quantity);
                        el.find('#price').html(price);
                    }
                    toggle(modalEdit);
                });
            }

            function reNumbering() {
                var number = 1;
                $('body').find('.no').each(function () {
                    $(this).html(number);
                    number++;
                });
            }
        </script>
        <?php
        $html = ob_get_clean();
        return $html;

    }

    private function setData() {

    }

    private function del_upd() {
        $pd_code = intval($_POST['pd_code']);
        $qtty = intval($_POST['qtty']);
        $price = round(floatval($_POST['price']), 2);

        $sql='';
        if (isset($_POST['update']) && $qtty>-1 && $price>-1) {
            $sql ='UPDATE `prices` SET `quantity`='.$qtty.', `price`='.$price;
        } else if (isset($_POST['delete'])) {
            $sql ='DELETE FROM `prices` ';
        }
        $sql.=' WHERE `pd_code`="'.$pd_code.'" AND `id_shop`='.$_SESSION['id_shop'];
        $this->db->query($sql);
        info::tryToActive($_SESSION['id_shop']);
        exit;
    }

    private function price_on() {
        $this->db->query('UPDATE `shop` SET `price_on`='.(($_POST['price_on']==1) ? '1' : '0'). ' WHERE `id`='.$_SESSION['id_shop']);
        if ($_POST['price_on']!=1) {
            $this->db->query('UPDATE `shop` SET `active`=0 WHERE `id`='.$_SESSION['id_shop']);
        } else {
            info::tryToActive($_SESSION['id_shop']);
        }
        exit;
    }

    private function excel() {
        if ($_FILES['ex']['error']==UPLOAD_ERR_OK && (
                (preg_match("/.xlsx\$/i", $_FILES['ex']['name'])) ||
                (preg_match("/.xlsm\$/i", $_FILES['ex']['name']))  ||
                (preg_match("/.xls\$/i", $_FILES['ex']['name'])) ) &&
            ($_FILES['ex']['size']<=10728640) ) {

            $inputFileType = PHPExcel_IOFactory::identify($_FILES['ex']['tmp_name']);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($_FILES['ex']['tmp_name']);
            $ar = $objPHPExcel->getActiveSheet()->toArray();

            $i=0;
            $res_ar = [];
            foreach($ar as $str) {
                if (intval($str[0])>0 && intval($str[1])>0 && floatval($str[2]) >0 ) {
                    $res_ar[$i]['code']= intval($str[0]);
                    $res_ar[$i]['quantity']= intval($str[1]);
                    $res_ar[$i]['price']= floatval($str[2]);
                    $i++;
                }
            }

            if (count($res_ar)>0) {
                $to_email= array();
                $to_base = array();

                $c_sql='';
                for($i=0; $i<count($res_ar); $i++) {
                    $c_sql.=($i!=0 ? ',' : '').' "'.$res_ar[$i]['code'].'" ';
                }
                $sql ='select `pd_code` from `pd_subgrs` where `pd_code` in ('.$c_sql.') ';
                $res= $this->db->query($sql);

                if ($res->num_rows==0) {
                    foreach($res_ar as $r) {
                        $to_email[]=$r['code'];
                    }
                } else {
                    $i=0;
                    foreach($res_ar as $res_array) {
                        $found=false;
                        foreach($res->rows as $row) {
                            if ($res_array['code']==$row['pd_code']) {
                                $to_base[$i]['code'] = $res_array['code'];
                                $to_base[$i]['quantity'] = $res_array['quantity'];
                                $to_base[$i]['price'] = $res_array['price'];
                                $i++;
                                $found=true;
                                break;
                            }
                        }
                        if ($found==false)
                            $to_email[] = $res_array['code'];
                    }
                }

                if (count($to_base)>0 ) {
                    $c_sql='';
                    for($i=0; $i<count($to_base); $i++) {
                        $c_sql.=($i!=0 ? ' , ' : '').' ('.$_SESSION['id_shop'].', "'.intval($to_base[$i]['code']).'", '.intval($to_base[$i]['quantity']).', '.floatval($to_base[$i]['price']).' ) ';
                    }
                    $this->db->query('delete from `prices` where `id_shop`='.$_SESSION['id_shop']);
                    $this->db->query('insert into `prices` (`id_shop`,  `pd_code`, `quantity`, `price`) values '.$c_sql);
                    include_once 'info.php';
                    info::tryToActive($_SESSION['id_shop']);

                    $res= $this->db->query('SELECT name from files where id_shop='.$_SESSION['id_shop']);
                    if ($res->num_rows>0) {
                        unlink($_SERVER['DOCUMENT_ROOT'].'/'.$res->row['name']);
                        $this->db->query('delete from files where id_shop='.$_SESSION['id_shop']);
                    }
                }
                $res= $this->db->query('select email from admin_emails where type="new_code"');
                if ((count($to_email)> 0)  && ($res->num_rows>0)) {
                    $con='';
                    for($i=0; $i<count($to_email); $i++) {
                        $con.=($i!=0 ? ' , ' : '').' '.$to_email[$i];
                    }
                    $message ='Продавец пытался установить цены на следующие отсутствующие товары (штрих-код): '.$con;
                    $header = 'Рекомендуем добавить эти товары на сайт!';
                    $mail = $res->row['email'];
                    $this->mail->send_mail($mail, $header, $message);
                }
                $_SESSION['msg_ok']='Добавлено товаров: '.count($to_base);

            }  else {
                $_SESSION['msg_error']= 'Пустой файл или содержить некорректные данные';
            }

        } else {
            $_SESSION['msg_error']= 'Некорректный файл или файл весит больше 10мб';
        }
    }

    private function load_ex() {
        $res= $this->db->query('SELECT pd_code, quantity, price FROM  prices WHERE id_shop='.$_SESSION['id_shop']);

        if ($res->num_rows>0) {
            $phpexcel = new PHPExcel();

            $page = $phpexcel->setActiveSheetIndex(0);
            $page->setCellValue("A1", "Code");
            $page->setCellValue("B1", "Quantity");
            $page->setCellValue("C1", "Price");
            for($i=0; $i<$res->num_rows; $i++) {
                $page->setCellValue("A".($i+2), $res->rows[$i]['pd_code']);
                $page->setCellValue("B".($i+2), $res->rows[$i]['quantity']);
                $page->setCellValue("C".($i+2), $res->rows[$i]['price']);
            }

            $page->setTitle("Price list");
            $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
            $rd = abs(rand(111111111111,999999999999));
            $n = "files/admin/excel/".$rd.".xlsx";
            $objWriter->save($n);
            $res= $this->db->query('SELECT name from files where id_shop='.$_SESSION['id_shop']);
            if ($res->num_rows>0) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/'.$res->row['name']);
                $this->db->query('delete from files where id_shop='.$_SESSION['id_shop']);
            }
            $sql = 'insert into files set `name`="'.$n.'", `id_shop`='.$_SESSION['id_shop'].', `time`='.time();
            $this->db->query($sql);
            header('location: /'.$n);
        }	 else {
            $_SESSION['msg_error']= 'Ваш прайс пуст!';
        }

    }

    private function clear_price() {
        $this->db->query('DELETE FROM `prices` WHERE `id_shop`='.$_SESSION['id_shop']);
        $this->db->query('UPDATE `shop` SET `active`=0 WHERE `id`='.$_SESSION['id_shop']);
        $res= $this->db->query('SELECT name from files where id_shop='.$_SESSION['id_shop']);
        if ($res->num_rows>0) {
            unlink($_SERVER['DOCUMENT_ROOT'].'/'.$res->row['name']);
            $this->db->query('delete from files where id_shop='.$_SESSION['id_shop']);
        }
    }


}