<?php
class addgoods extends frame {
    private $pds = [];

    public function get_content() {
        if (isset($_POST['pd_code'])) $this->setting();

        $this->metaTitle = 'Каталог | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription = '&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        $this->initData();
        $this->setData();
        echo $this->showData();

    }


    private  function getData() {
        $res = $this->db->query('SELECT * FROM `product`  WHERE `brand`='.intval($_GET['brand']).' ORDER BY `level` ASC '.$this->getLimitByPage());
        foreach ($res->rows as &$row) {
            $br = $this->db->query('select id, name from brand where id='.$row['brand']);
            $res2 = $this->db->query('select pd_code from pd_subgrs where id_pd='.$row['id'].' and pd_code<>"" ');
            foreach ($res2->rows as &$row2) {
                $res3 = $this->db->query('select id, name from `group` where id_super<>0 and id in (select id_subgr from pd_subgrs where pd_code="'.$row2['pd_code'].'") ');
                $row2['subgr'] = $res3->row;
                $res4 = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `prices` WHERE `id_shop`='.$_SESSION['id_shop'].' AND `pd_code`="'.$row2['pd_code'].'" ');
                $row2['checked'] = $res4->row['count'];
            }
            unset($row2);
            $row['variants'] = $res2->rows;
            $row['brand'] = $br->row;
        }
        unset($row);

        $this->pds = $res->rows;
    }

   

    private function initData() {

    }

    private function setData() {
        
    }

    private function showData() {
        ob_start();
        ?>
        <?php if (count($this->pds)==0) echo  (($this->page>1) ? 'PAGE_END' : 'Пусто'); ?>
        <?php foreach($this->pds as $pd) {  ?>
           <div class="item">
			<div class="catalogProduct">
				<img src="catalog/<?=$pd['photo']?>.png">
				<div class="name">
					<span><?=$pd['brand']['name']?></span>
					<span><?=$pd['name']?></span>
				</div>
			</div>
            <?php foreach($pd['variants'] as $v) { ?>
			    <div class="catalogUPC">
				    <span><?=$v['pd_code']?></span>
				    <span><?=$v['subgr']['name']?></span>
				    <label class="checkbox"><input id="checking"  type="checkbox" data-pd-code="<?=$v['pd_code']?>"  <?=(($v['checked']!=0) ? 'checked' : '')?> /><div class="checked"></div></label>
			    </div>
            <?php } ?>
		</div>
        <?php } ?>
        <?php if ($this->page>1) exit; ?>
        
        <script>
            
            $(document).ready(go());

            function go() {
                $('body').on("click", "#checking", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=addgoods', 'pd_code='+th.attr('data-pd-code')+'&active='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                });
            }
        </script>
        
        <?php
        $html = ob_get_clean();
        return $html;
    }


    private function setting() {
        $pd_code = intval($_POST['pd_code']);
        $active = intval($_POST['active']);
        if ($active===1) {
            $res =$this->db->query('SELECT `id` FROM `prices` WHERE `pd_code`="'.$pd_code.'" AND `id_shop`='.$_SESSION['id_shop'].' LIMIT 1');
            if ($res->num_rows==0) {
                $res =$this->db->query('INSERT INTO `prices` SET `quantity`=0, `price`=0, `pd_code`="'.$pd_code.'", `id_shop`='.$_SESSION['id_shop']);
            }
        } else {
            $this->db->query('DELETE FROM   `prices` WHERE `pd_code`="'.$pd_code.'" AND `id_shop`='.$_SESSION['id_shop']);
        }
        exit;
    }


}