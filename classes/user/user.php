<?php
class user extends frame {
    private $days =[];
    
    /*
    $days[]['date']
           ['big_summ']
           ['orders'][]
    */

    public function get_content() {
        if (isset($_POST['ajax'])) {$this->getData($_POST['last_order_id']); if (count($this->days)>0) exit(json_encode($this->days)); else exit(0);}
        $this->metaTitle='Кабинет | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        echo $this->showData();
        $this->setData();

    }

    // this func will write to days array
    private function getData($last_order_id=0) {
        $last_order_id  = intval($last_order_id);
        $orders = [];
        $res = $this->db->query('SELECT * FROM `orders` WHERE `confirmed`=1 AND `id_shop` IN (SELECT `id` FROM `shop` WHERE `id_seller`='.$_SESSION['id'].') '.(($last_order_id>0) ? ' AND `id`<'.$last_order_id : '').' ORDER BY `time` DESC LIMIT 30');
        if ($res->num_rows>0) {
            $orders = $res->rows;
            foreach ($orders as &$order) {
                $res= $this->db->query('SELECT `name`, `country`, `city`, `street_home` FROM `shop` WHERE `id`='.$order['id_shop']);
                $order['shop_data'] = $res->row;
                $this->initData($order);
                
                $date = date('d.m.Y', $order['time']);
                $new_day = true;
                foreach ($this->days as &$day ) {
                    if ($day['date'] == $date ) {
                        $day['orders'][] = $order;
                        $day['big_summ'] = bcadd($day['big_summ'], $order['summ'], 2);
                        $new_day = false;
                        break;
                    }
                }
                unset($day);
                if ($new_day == true) {
                    $new_arr = [];
                    $new_arr['date'] = $date;
                    $new_arr['orders'][] = $order;
                    $new_arr['big_summ'] = $order['summ'];
                    $this->days[] = $new_arr;
                }
            }
            unset($order);
        }
    }

    // The func is used by getData()
    private function initData(&$order) {
        $delText='';
        switch ($order['delType']) {
            case 'ex': $delText='Экспресс доставка '.(($order['delPrice']==0) ? '(бесплатно)' : '('.$order['delPrice'].' грн)');break;
            case 'td': $delText='Доставка сегодня '.(($order['delPrice']==0) ? '(бесплатно)' : '('.$order['delPrice'].' грн)');break;
            case 'tm': $delText='Доставка на завтра '.(($order['delPrice']==0) ? '(бесплатно)' : '('.$order['delPrice'].' грн)');break;
            case 'ukr': $delText='Пересылка по Украине '.(($order['delPrice']==0) ? '(бесплатно)' : '(тариф + '.$order['delPrice'].' грн)');break;
            case 'shop': $delText='Самовывоз';break;
        }
        $delText.=' из магазина '.$order['shop_data']['name'];
        $adr ='';
        if ($order['delType']== 'shop') $adr = $order['shop_data']['city'].', '.$order['shop_data']['street_home'];
        else $adr = $order['city'].', '.$order['street'].', '.$order['home_number'];
        $order['adr'] = $adr;
        $order['delText'] = $delText;
        $order['hour'] = date('H', $order['time']);
        $order['minute'] = date('i', $order['time']);
    }

    private function showData() {
        ob_start();
        ?>
        <span id="days"></span>

        <script>
            json_days ='<?=((count($this->days)>0) ? json_encode($this->days) : 0) ?>';
            days = ((json_days!=0) ? $.parseJSON(json_days) : 0);
            printDaysAndOrders();
            /*
            The func will print days and orders which are not printed
            */
            function printDaysAndOrders() {
                if (days!=0) {
                    for (i=0; i<days.length; i++) {
                        if($('#'+days[i].date).length) {
                            for (o=0; o<days[i].orders.length; o++) {
                                if(!($('#'+days[i].orders[o].id).length)) {
                                    html+='<div class="item orders" id="'+days[i].orders[o].id+'">\n' +
                                        '\t\t\t\t<div class="time">\n' +
                                        '\t\t\t\t\t<span>'+days[i].orders[o].hour+'<sup>'+days[i].orders[o].minute+'</sup><br>'+days[i].date+'</span>\n' +
                                        '\t\t\t\t\t<span>'+days[i].orders[o].id+'</span>\n' +
                                        '\t\t\t\t</div>\n' +
                                        '\t\t\t\t<div class="delivery">\n' +
                                        '\t\t\t\t\t<span>'+days[i].orders[o].adr+'</span>\n' +
                                        '\t\t\t\t\t<span>'+days[i].orders[o].delText+'</span>\n' +
                                        '\t\t\t\t</div>\n' +
                                        '\t\t\t\t<div class="summ">\n' +
                                        '\t\t\t\t\t<span>'+days[i].orders[o].summ+'</span>\n' +
                                        '\t\t\t\t\t<span>'+days[i].orders[o].seller_paid+'</span>\n' +
                                        '\t\t\t\t</div>\n' +
                                        '\t\t\t</div>\n';
                                    $('#'+days[i].date).append(html);
                                    days[i].big_simm+=days[i].orders[o].summ;
                                    days[i].big_simm = days[i].big_simm.toFixed(2);
                                    $('#'+days[i].date).filter('.total').html(days[i].big_simm);
                                }
                            }
                        } else {
                            var html = '<div class="part" id="'+days[i].date+'">\n';
                            for (o=0; o<days[i].orders.length; o++) {
                                html+='<div class="item orders" id="'+days[i].orders[o].id+'">\n' +
                                    '\t\t\t\t<div class="time">\n' +
                                    '\t\t\t\t\t<span>'+days[i].orders[o].hour+'<sup>'+days[i].orders[o].minute+'</sup><br>'+days[i].date+'</span>\n' +
                                    '\t\t\t\t\t<span>'+days[i].orders[o].id+'</span>\n' +
                                    '\t\t\t\t</div>\n' +
                                    '\t\t\t\t<div class="delivery">\n' +
                                    '\t\t\t\t\t<span>'+days[i].orders[o].adr+'</span>\n' +
                                    '\t\t\t\t\t<span>'+days[i].orders[o].delText+'</span>\n' +
                                    '\t\t\t\t</div>\n' +
                                    '\t\t\t\t<div class="summ">\n' +
                                    '\t\t\t\t\t<span>'+days[i].orders[o].summ+'</span>\n' +
                                    '\t\t\t\t\t<span>'+days[i].orders[o].seller_paid+'</span>\n' +
                                    '\t\t\t\t</div>\n' +
                                    '\t\t\t</div>\n';
                            }
                            html+= '<span class="total">'+days[i].big_summ+'</span>\n' +
                                '</div>\n';
                            $('#days').append(html);
                        }
                    }
                } else {
                    $('#days').append('пусто');
                }
            }

            /*
            The func takes array of new days and  merge the global days array and new
             */
            function addNewDays(new_days) {
                for (i=0; i<new_days.length; i++) {
                    var added = false;
                    for (i2=0; i2<days.length; i2++) {
                        if (new_days[i].date==days[i2].date) {
                            days[i2].orders = days[i2].orders.concat(new_days[i].orders);
                            added = true;
                            break;
                        }
                    }
                    if (added==false) {
                        days.push(new_days[i]);
                    }
                }
            }
        </script>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function setData() {

    }


}






















/*
    public function getOrdersByTimeMinMax($min, $max=0) {
        $orders = [];
        $res = $this->db->query('SELECT * FROM `orders` WHERE `confirmed`=1 AND `id_shop` IN (SELECT `id` FROM `shop` WHERE `id_seller`='.$_SESSION['id'].') AND `time`>='.$min. ' AND `time`<'.$max.' ORDER BY `time` DESC');
        if ($res->num_rows>0) {
            $orders = $res->rows;
            foreach ($orders as &$order) {
                $res= $this->db->query('SELECT `name`, `country`, `city`, `street_home` FROM `shop` WHERE `id`='.$order['id_shop']);
                $order['shop_data'] = $res->row;
            }
            unset($order);
        }
        return $orders;
    }
    
    
    
    
    private function getData($day_back=0) {
        $day_back = intval($day_back);
        $min = strtotime('today');
        $min = $min - 86400 * $day_back;
        $max = $min +86400;
        $new_arr = [];
        $new_arr = $this->getOrdersByTimeMinMax($min, $max);
        if (count($new_arr)>0) {
            $this->day[] = $new_arr;
        }
    }
    
    
    */