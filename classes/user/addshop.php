<?php
class addshop extends frame {
    public function get_content() {
        if ($_SERVER['REQUEST_METHOD']=='POST') {$this->setData(); exit;}
        $this->metaTitle='Добавить магазин | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        echo $this->showData();
    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <h1>Новый магазин</h1>
        <form method="post" action="/?option=addshop">
            <div class="item addShopName">
                <input name="name" required="required" type="text" placeholder="Название магазина"/>
                <input id="phone" name="phone" required="required" type="text" placeholder="Телефон магазина"/>
            </div>
            <div class="item addShopPassword">
                <input name="pass" required="required" type="password" placeholder="пароль"/>
                <input name="pass2" required="required" type="password" placeholder="пароль (еще раз)"/>
            </div>
            <script type="text/javascript" src="/js/mapbox.js"></script>
            <link href='/css/mapbox.css' rel='stylesheet' />
            <div class="item addShopMap" style="height: 250px !important;" id="map">

            </div>
            <input type="hidden" name="map_label" id="map_label">
            <div class="item addShopCity">
                <input name="city" id="city" required="required" type="text" placeholder="Город"/>
            </div>
            <div class="item addShopAddress">
                <textarea name="street_home" id="str_home" required="required" placeholder="Адрес магазина" maxlength="200"></textarea>
            </div>
            <input type="submit" value="Создать магазин"/>
        </form>
        <!-- <<<<< CONTENT <<<<< -->


        <script>
            $( document ).ready(function() {
                $("#phone").mask("+38(099)999-99-99", {completed: function(){}});
                var lngLat;
                var m =0;

                mapboxgl.accessToken = 'pk.eyJ1Ijoiem9vc2tvcCIsImEiOiJjam92bmp1MnAwamdoM3BvZDJhNWtsM2NhIn0.J4nUkH5CmjS6Z7bFmaQF2w';

                var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/light-v9',
                    center: [30.518010486901,50.444378333058],
                    zoom: 7
                });


                map.on('mousemove', function (e) {
                    lngLat = e.lngLat;
                });

                $('body').on("click", "#map", function(event){
                    // lngLat is like "LngLat(-23.3443,34.3434)" so we have to do like  "-23.3443,34.3434"
                    var newArr = String(lngLat).split(',');
                    var newArr2 = newArr[0].split('(');
                    var newArr3 = newArr[1].split(')');
                    var newArr4 = newArr3[0].split(' ');
                    var my_lng_lat =  newArr2[1]+','+newArr4[1];
                    $('#map_label').val(my_lng_lat);

                    var el = document.createElement('div');
                    el.className = 'marker';
                    if (m!=0) {m.remove();}
                    m = new mapboxgl.Marker(el);
                    m.setLngLat(lngLat);
                    m.addTo(map);

                    show_adr(my_lng_lat);
                });

                function show_adr(lngLat) {
                    var arr;
                    $.post('/map.php', 'map_label='+lngLat, function(data) {
                        if ((data.indexOf('CANNOT_READ_ADR')+1)==0) {
                            arr = $.parseJSON(data);
                            $('#city').val(arr.city);
                        } else {
                            $('#city').val('');
                            $('#city').attr('placeholder','Заполните вручную');
                        }
                    }, 'text');
                }
            });
        </script>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function setData() {
        if ((mb_strlen($_POST['name'])>0 && mb_strlen($_POST['name'])<=32)
        && ((mb_strlen($_POST['phone'])>4) && (mb_strlen($_POST['phone'])<20))
        && (($_POST['pass']===$_POST['pass2']) && (preg_match('/^[a-z0-9]*$/i' ,$_POST['pass'])) && (mb_strlen($_POST['pass'])>=6 && mb_strlen($_POST['pass'])<=32))
        && (!empty($_POST['map_label']))
        && (!empty($_POST['city']))
        && (!empty($_POST['street_home']))) {
            // Is there a shop with the same name?
            $res = $this->db->query('SELECT `name` FROM `shop` WHERE `name`="'.$this->db->escape($_POST['name']).'" ');
            if ($res->num_rows==0) {
                $arr = explode(',',$_POST['map_label']);
                $lng = floatval($arr[0]);
                $lat = floatval($arr[1]);
                $this->db->query('INSERT INTO `shop` SET `id_seller`='.$_SESSION['id'].', `name`="'.$this->db->escape($_POST['name']).'",  `phone`="'.$this->db->escape($_POST['phone']).'",  `password`="'.$this->db->escape($this->security->pass($_POST['pass'])).'",  `map_label`="'.$lng.','.$lat.'",  `city`="'.$this->db->escape($_POST['city']).'",  `street_home`="'.$this->db->escape($_POST['street_home']).'", `time`='.time());
                header('location: /?option=change&id='.$this->db->insertId());
                exit;
            }
        }
        header('location: /?option=addshop');
    }

}