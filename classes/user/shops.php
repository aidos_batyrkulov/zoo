<?php
class shops extends frame {
    private $shops=[];

    public function get_content() {
        $this->metaTitle=' Мои магазины | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        echo $this->showData();
        if ($this->page>1) exit;
        $this->setData();

    }

    private function getData() {
        $res = $this->db->query('SELECT * FROM `shop` WHERE `id_seller`='.$_SESSION['id'].' '.$this->getLimitByPage());
        $this->shops = $res->rows;
        if (count($this->shops)>0) {
            foreach ($this->shops as &$shop) {
                $res = $this->db->query('SELECT * FROM `discounts` WHERE `id_shop`='.$shop['id']);
                $shop['discounts'] = $res->rows;
            }
            unset($shop);
        }
    }

    private  function initData() {

    }

    private function showData() {
        ob_start();
        ?>
        <?php if (count($this->shops)==0) echo  (($this->page>1) ? 'PAGE_END' : 'Вы еще не добавили магазинов'); ?>
        <?php foreach ($this->shops as $shop)  { ?>
            <div class="part">
                <div class="item">
                    <span class="shopName"><?=$shop['name']?><sup><?=$shop['id']?></sup></span>
                    <div class="rate">
                        <div style="width:75%"></div>
                        <span>75</span>
                    </div>
                    <span class="shopAddress"><?=$shop['city']?>, <?=$shop['street_home']?></span>
                </div>
                <?php if ($shop['type']=='uni' || $shop['ex_radius']>0 || $shop['td_get_order_until']>0 || $shop['tm_get_order_until']>0 || $shop['ukr_pay']!='') { ?>
                    <div class="item">
                        <?php if ($shop['type']=='uni') { ?>
                        <div class="shopDelivery pickup">
                            <div class="first"></div>
                            <div class="second">
                                <span><?=$shop['city']?>, <?=$shop['street_home']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['ex_radius']>0) { ?>
                        <div class="shopDelivery express">
                            <div class="first"></div>
                            <div class="second">
                                <span>радиус <?=$shop['ex_radius']?>м</span><br>
                                <span><?=$shop['ex_min_summ']?></span>
                                <span><?=$shop['ex_price']?></span>
                                <span><?=$shop['ex_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['td_get_order_until']>0) { ?>
                        <div class="shopDelivery today">
                            <div class="first"></div>
                            <div class="second">
                                <?php if (mb_strlen($shop['td_get_order_until'])==3) $shop['td_get_order_until'] = '0'.$shop['td_get_order_until']; ?>
                                <span>прием заказов до <?=mb_substr($shop['td_get_order_until'],0,2);?><sup><?=mb_substr($shop['td_get_order_until'],2,2);?></sup></span><br>
                                <span><?=$shop['td_min_summ']?></span>
                                <span><?=$shop['td_price']?></span>
                                <span><?=$shop['td_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['tm_get_order_until']>0) { ?>
                        <div class="shopDelivery tomorrow">
                            <div class="first"></div>
                            <div class="second">
                                <?php if (mb_strlen($shop['tm_get_order_until'])==3) $shop['tm_get_order_until'] = '0'.$shop['tm_get_order_until']; ?>
                                <span>прием заказов до <?=mb_substr($shop['tm_get_order_until'],0,2);?><sup><?=mb_substr($shop['tm_get_order_until'],2,2);?></sup></span><br>
                                <span><?=$shop['tm_min_summ']?></span>
                                <span><?=$shop['tm_price']?></span>
                                <span><?=$shop['tm_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                        <?php if ($shop['ukr_pay']!='') { ?>
                        <div class="shopDelivery post">
                            <div class="first"></div>
                            <div class="second">
                                <span><?=(($shop['ukr_pay']=='after') ? 'наложенный платеж' : 'предоплата')?></span><br>
                                <span><?=$shop['ukr_min_summ']?></span>
                                <span>тариф+<?=$shop['ukr_price']?></span>
                                <span><?=$shop['ukr_free']?></span>
                            </div>
                        </div>
                        <?php  } ?>
                    </div>
                <?php } ?>

                <?php if (count($shop['discounts'])>0) { ?>
                    <div class="item">
                    <?php foreach ($shop['discounts'] as $discount) { ?>
                    <div class="shopDiscount">
                        <span><?=$discount['discount']?></span>
                        <span><?=$discount['summ']?></span>
                    </div>
                    <?php } ?>
                    </div>
                <?php } ?>
                <input type="submit" onclick="location.href='/?option=change&id=<?=$shop['id']?>'" value="Редактировать"/>
            </div>
        <?php } ?>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function setData() {

    }


}