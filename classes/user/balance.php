<?php
class balance extends frame {
    private $balance;

    public function get_content() {
        if (isset($_POST['promo_code'])) $this->usePromo();
        $this->metaTitle=' | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        echo $this->showData();

    }

    private function getData() {
        $res = $this->db->query('SELECT `balance` FROM  `seller` WHERE `id`='.$_SESSION['id']);
        $this->balance = $res->row['balance'];
    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <h1 class="balanceTotal"><?=$this->balance?></h1>
        <div class="minPart">
            <div class="minItem balancePay">
                <span></span>
                <input required="required" type="text" placeholder="0.00 грн"/>
            </div>
            <input type="submit" value="Пополнить"/>
        </div>

        <div class="minPart">
            <form method="post" action="/?option=balance">
                <div class="minItem balanceBonus">
                    <span></span>
                    <input required="required" type="text" name="promo_code" placeholder="промо-код"/>
                </div>
                <input type="submit" value="Активировать"/>
            </form>
        </div>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function usePromo() {
        $this->db->query('UPDATE `promo` SET `id_seller`='.$_SESSION['id'].' WHERE `id_seller`=0 AND `code`="'.$this->db->escape($_POST['promo_code']).'"');
        if ($this->db->affectedRows()>0)  {
            $res = $this->db->query('SELECT `summ` FROM  `promo` WHERE `code`="'.$this->db->escape($_POST['promo_code']).'"');
            $this->db->query('UPDATE `seller` SET `balance`=balance+'.$res->row['summ'].' WHERE `id`='.$_SESSION['id']);
        }
    }


}