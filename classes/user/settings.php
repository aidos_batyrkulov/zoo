<?php
class settings extends frame {
    private $user;

    public function get_content() {
        $this->metaTitle=' Настройки | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        if ($_SERVER['REQUEST_METHOD']=='POST')
            $this->setData();
        $this->getData();
        $this->initData();
        echo $this->showData();

    }

    private function setData() {
        $sql = '';
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $sql = ' `email`="'.$this->db->escape($_POST['email']).'" ';
        }
        if (mb_strlen($_POST['phone'])>4 && mb_strlen($_POST['phone'])<20) {
            $sql.= (($sql!='') ? ',' : '' ).' `phone`="'.$this->db->escape($_POST['phone']).'"';
        }
        if (($_POST['pass']===$_POST['pass2']) && (preg_match('/^[a-z0-9]*$/i' ,$_POST['pass'])) && (mb_strlen($_POST['pass'])>=6 && mb_strlen($_POST['pass'])<=32)) {
            $sql.= (($sql!='') ? ',' : '' ).' `password`="'.$this->db->escape($this->security->pass($_POST['pass'])).'"';
        }
        if ($sql!='') {
            $this->db->query('UPDATE `seller` SET '.$sql.' WHERE `id`='.$_SESSION['id']);
        }
    }

    private function getData() {
        $res = $this->db->query('SELECT * FROM `seller` WHERE `id`='.$_SESSION['id']);
        $this->user= $res->row;
    }

    private function initData() {

    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <form method="post" action="/?option=settings">
            <h1>СПД <?=$this->user['fio']?></h1>
            <div class="item settingsContact">
                <input id="phone" name="phone"  type="text" value="<?=$this->user['phone']?>"/>
                <input name="email"  type="text" value="<?=$this->user['email']?>"/>
            </div>
            <div class="item settingsPassword">
                <input name="pass"  type="password" placeholder="новый пароль"/>
                <input name="pass2" type="password" placeholder="новый пароль (еще раз)"/>
            </div>
            <input type="submit" value="Сохранить изменения"/>
        </form>

        <script type="text/javascript">
            $("#phone").mask("+38(099)999-99-99", {completed: function(){}});
        </script>
            <!-- <<<<< CONTENT <<<<< -->

        <?php
        $html = ob_get_clean();
        return $html;
    }


}