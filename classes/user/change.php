<?php
class change extends frame {
    private $shop;

    public function get_content() {
        if (isset($_GET['delete'])) {$this->deleteData(); exit;}
        if ($_SERVER['REQUEST_METHOD']=='POST') {$this->setData(); exit;}
        $this->metaTitle=' Редактировать магазин | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        $this->initData();
        echo $this->showData();
    }

    private function getData() {
        $res = $this->db->query('SELECT * FROM `shop` WHERE `id_seller`='.$_SESSION['id'].' AND `id`='.intval(@$_GET['id']));
        if ($res->num_rows==0) {header('location: /'); exit;}
        $res2 = $this->db->query('SELECT * FROM `discounts` WHERE `id_shop`='.$res->row['id']);
        $res->row['discounts'] = $res2->rows;
        $this->shop = $res->row;
    }

    private function initData() {

    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <form method="post" action="/?option=change">
            <div class="item changeContacts">
                <input name="new_pass" type="password" placeholder="новый пароль"/>
                <input name="new_pass2"  type="password" placeholder="новый пароль (еще раз)"/>
                <input id="phone" name="phone" type="text" value="<?=$this->shop['phone']?>"/>
                <div class="deleteShop"  onclick="location.href='/?option=change&delete=on&id=<?=$this->shop['id']?>'" ></div>
            </div>
            <div class="item changeTimetable">
                <div></div>
            <?php
            $day_name = array("MO","TU","WE","TH","FR","SA","SU");
            $day =1;
            foreach ($day_name as $item) { ?>
                <div class="<?=$item?> <?=(($this->shop['d'.$day.'_start']==0) ? 'off' : '')?>">
                    <select name="d<?=$day?>_from" class="from">
                        <option <?php if ($this->shop['d'.$day.'_start']==0) echo 'selected'; ?> value="0">закрыто</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==700) echo 'selected'; ?> value="700">07:00</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==730) echo 'selected'; ?> value="730">07:30</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==800) echo 'selected'; ?> value="800">08:00</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==830) echo 'selected'; ?> value="830">08:30</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==900) echo 'selected'; ?> value="900">09:00</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==930) echo 'selected'; ?> value="930">09:30</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==1000) echo 'selected'; ?> value="1000">10:00</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==1030) echo 'selected'; ?> value="1030">10:30</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==1100) echo 'selected'; ?> value="1100">11:00</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==1130) echo 'selected'; ?> value="1130">11:30</option>
                        <option <?php if ($this->shop['d'.$day.'_start']==1200) echo 'selected'; ?> value="1200">12:00</option>
                    </select>
                    <select name="d<?=$day?>_to" class="to">
                        <option <?php if ($this->shop['d'.$day.'_end']==0) echo 'selected'; ?> value="0">закрыто</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1400) echo 'selected'; ?> value="1400">14:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1430) echo 'selected'; ?> value="1430">14:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1500) echo 'selected'; ?> value="1500">15:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1530) echo 'selected'; ?> value="1530">15:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1600) echo 'selected'; ?> value="1600">16:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1630) echo 'selected'; ?> value="1630">16:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1700) echo 'selected'; ?> value="1700">17:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1730) echo 'selected'; ?> value="1730">17:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1800) echo 'selected'; ?> value="1800">18:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1830) echo 'selected'; ?> value="1830">18:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1900) echo 'selected'; ?> value="1900">19:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==1930) echo 'selected'; ?> value="1930">19:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==2000) echo 'selected'; ?> value="2000">20:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==2030) echo 'selected'; ?> value="2030">20:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==2100) echo 'selected'; ?> value="2100">21:00</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==2130) echo 'selected'; ?> value="2130">21:30</option>
                        <option <?php if ($this->shop['d'.$day.'_end']==2200) echo 'selected'; ?> value="2200">22:00</option>
                    </select>
                </div>
                <?php $day++; ?>
            <?php } ?>
            </div>

            <div class="item changeDelivery">
                <div class="pickup <?php if ($this->shop['type']=='internet') echo 'off'; ?>">
                    <select name="shop">
                        <option  <?php if ($this->shop['type']=='internet') echo 'selected'; ?>  value="0">нет</option>
                        <option  <?php if ($this->shop['type']=='uni') echo 'selected'; ?>       value="1">есть</option>
                    </select>
                </div>
            </div>
            <div class="item changeDelivery">
                <div class="express <?php if ($this->shop['ex_radius']==0) echo 'off'; ?>">
                    <select name="ex_radius">
                        <option value="0">нет</option>
                        <option <?php if ($this->shop['ex_radius']==100) echo ' selected ';?> value="100">100м</option>
                        <option <?php if ($this->shop['ex_radius']==200) echo ' selected ';?> value="200">200м</option>
                        <option <?php if ($this->shop['ex_radius']==300) echo ' selected ';?> value="300">300м</option>
                        <option <?php if ($this->shop['ex_radius']==400) echo ' selected ';?> value="400">400м</option>
                        <option <?php if ($this->shop['ex_radius']==500) echo ' selected ';?> value="500">500м</option>
                        <option <?php if ($this->shop['ex_radius']==600) echo ' selected ';?> value="600">600м</option>
                        <option <?php if ($this->shop['ex_radius']==700) echo ' selected ';?> value="700">700м</option>
                        <option <?php if ($this->shop['ex_radius']==800) echo ' selected ';?> value="800">800м</option>
                        <option <?php if ($this->shop['ex_radius']==900) echo ' selected ';?> value="900">900м</option>
                        <option <?php if ($this->shop['ex_radius']==1000) echo ' selected ';?> value="1000">1000м</option>
                    </select>
                </div>
                <div class="delMin">
                    <input name="ex_min_summ"  type="text" placeholder="0.00 грн" value="<?=(($this->shop['ex_min_summ']>0) ? $this->shop['ex_min_summ'] : '')?>"/>
                </div>
                <div class="delCost">
                    <input name="ex_price"  type="text" placeholder="0.00 грн"  value="<?=(($this->shop['ex_price']>0) ? $this->shop['ex_price'] : '')?>"/>
                </div>
                <div class="delFree">
                    <input name="ex_free"  type="text" placeholder="0.00 грн"  value="<?=(($this->shop['ex_free']>0) ? $this->shop['ex_free'] : '')?>"/>
                </div>
            </div>
            <div class="item changeDelivery">
                <div class="today <?php if ($this->shop['td_get_order_until']==0) echo 'off'; ?>">
                    <select name="td_get_order_until">
                        <option value="0">нет</option>
                        <option <?php if ($this->shop['td_get_order_until']==1200) echo ' selected ';?> value="1200">до 12:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1300) echo ' selected ';?> value="1300">до 13:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1400) echo ' selected ';?> value="1400">до 14:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1500) echo ' selected ';?> value="1500">до 15:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1600) echo ' selected ';?> value="1600">до 16:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1700) echo ' selected ';?> value="1700">до 17:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1800) echo ' selected ';?> value="1800">до 18:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==1900) echo ' selected ';?> value="1900">до 19:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==2000) echo ' selected ';?> value="2000">до 20:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==2100) echo ' selected ';?> value="2100">до 21:00</option>
                        <option <?php if ($this->shop['td_get_order_until']==2200) echo ' selected ';?> value="2200">до 22:00</option>
                    </select>
                </div>
                <div class="delMin">
                    <input name="td_min_summ"  type="text" placeholder="0.00 грн" value="<?=(($this->shop['td_min_summ']>0) ? $this->shop['td_min_summ'] : '')?>"/>
                </div>
                <div class="delCost">
                    <input name="td_price"  type="text" placeholder="0.00 грн"    value="<?=(($this->shop['td_price']>0) ? $this->shop['td_price'] : '')?>"/>
                </div>
                <div class="delFree">
                    <input name="td_free"  type="text" placeholder="0.00 грн"     value="<?=(($this->shop['td_free']>0) ? $this->shop['td_free'] : '')?>"/>
                </div>
            </div>
            <div class="item changeDelivery">
                <div class="tomorrow <?php if ($this->shop['tm_get_order_until']==0) echo 'off'; ?>">
                    <select name="tm_get_order_until">
                        <option value="0">нет</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1200) echo ' selected ';?> value="1200">до 12:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1300) echo ' selected ';?> value="1300">до 13:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1400) echo ' selected ';?> value="1400">до 14:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1500) echo ' selected ';?> value="1500">до 15:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1600) echo ' selected ';?> value="1600">до 16:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1700) echo ' selected ';?> value="1700">до 17:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1800) echo ' selected ';?> value="1800">до 18:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==1900) echo ' selected ';?> value="1900">до 19:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==2000) echo ' selected ';?> value="2000">до 20:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==2100) echo ' selected ';?> value="2100">до 21:00</option>
                        <option <?php if ($this->shop['tm_get_order_until']==2200) echo ' selected ';?> value="2200">до 22:00</option>
                    </select>
                </div>
                <div class="delMin">
                    <input name="tm_min_summ"  type="text" placeholder="0.00 грн" value="<?=(($this->shop['tm_min_summ']>0) ? $this->shop['tm_min_summ'] : '')?>"/>
                </div>
                <div class="delCost">
                    <input name="tm_price"  type="text" placeholder="0.00 грн"    value="<?=(($this->shop['tm_price']>0) ? $this->shop['tm_price'] : '')?>"/>
                </div>
                <div class="delFree">
                    <input name="tm_free"  type="text" placeholder="0.00 грн"     value="<?=(($this->shop['tm_free']>0) ? $this->shop['tm_free'] : '')?>"/>
                </div>
            </div>
            <div class="item changeDelivery">
                <div class="post  <?php if ($this->shop['ukr_pay']=='') echo 'off'; ?>">
                    <select name="ukr_pay">
                        <option value="0">нет</option>
                        <option <?php if ($this->shop['ukr_pay']=='after') echo ' selected ';?> value="after">наложка</option>
                    </select>
                </div>
                <div class="delMin">
                    <input name="ukr_min_summ"  type="text" placeholder="0.00 грн" value="<?=(($this->shop['ukr_min_summ']>0) ? $this->shop['ukr_min_summ'] : '')?>"/>
                </div>
                <div class="delCost">
                    <input name="ukr_price"  type="text" placeholder="0.00 грн"    value="<?=(($this->shop['ukr_price']>0) ? $this->shop['ukr_price'] : '')?>"/>
                </div>
                <div class="delFree">
                    <input name="ukr_free"  type="text" placeholder="0.00 грн"     value="<?=(($this->shop['ukr_free']>0) ? $this->shop['ukr_free'] : '')?>"/>
                </div>
            </div>

            <div class="item changeDiscount">
                <div></div>
                <?php $i=0; ?>
                <?php foreach ($this->shop['discounts'] as $dis) { ?>
                    <div>
                        <input type="hidden" name="dis_<?=$i?>_id" value="<?=$dis['id']?>">
                        <select name="dis_<?=$i?>">
                            <option value="0">0 %</option>
                            <option <?php if ($dis['discount']==1) echo ' selected ';?> value="1">1 %</option>
                            <option <?php if ($dis['discount']==2) echo ' selected ';?> value="2">2 %</option>
                            <option <?php if ($dis['discount']==3) echo ' selected ';?> value="3">3 %</option>
                            <option <?php if ($dis['discount']==4) echo ' selected ';?> value="4">4 %</option>
                            <option <?php if ($dis['discount']==5) echo ' selected ';?> value="5">5 %</option>
                            <option <?php if ($dis['discount']==6) echo ' selected ';?> value="6">6 %</option>
                            <option <?php if ($dis['discount']==7) echo ' selected ';?> value="7">7 %</option>
                            <option <?php if ($dis['discount']==8) echo ' selected ';?> value="8">8 %</option>
                            <option <?php if ($dis['discount']==9) echo ' selected ';?> value="9">9 %</option>
                        </select>
                        <input name="dis_<?=$i?>_summ" type="text" value="<?=$dis['summ']?>" />
                    </div>
                    <?php $i++; ?>
                <?php } ?>
                <div class="off">
                    <select name="new_dis">
                        <option value="0">0 %</option>
                        <option value="1">1 %</option>
                        <option value="2">2 %</option>
                        <option value="3">3 %</option>
                        <option value="4">4 %</option>
                        <option value="5">5 %</option>
                        <option value="6">6 %</option>
                        <option value="7">7 %</option>
                        <option value="8">8 %</option>
                        <option value="9">9 %</option>
                    </select>
                    <input name="new_dis_summ" type="text" placeholder="0.00"/>
                </div>
            </div>
            <input type="hidden" name="id_shop" value="<?=$_GET['id']?>">
            <input type="submit" value="Сохранить изменения"/>
        </form>

        <script type="text/javascript">
            $("#phone").mask("+38(099)999-99-99", {completed: function(){}});
        </script>
        <!-- <<<<< CONTENT <<<<< -->


        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function setData() {
        // redirect if the shop doesn't exist
        $res = $this->db->query('SELECT `id` FROM `shop` WHERE `id`='.intval($_POST['id_shop']).' AND  `id_seller`='.$_SESSION['id']. ' LIMIT 1');
        if ($res->num_rows==0) $this->redirect();

        // pass
        if (($_POST['new_pass']===$_POST['new_pass2']) && (preg_match('/^[a-z0-9]*$/i' ,$_POST['new_pass'])) && (mb_strlen($_POST['new_pass'])>=6 && mb_strlen($_POST['new_pass'])<=32)) {
            $this->db->query('UPDATE `shop` SET `password`="'.$this->db->escape($this->security->pass($_POST['new_pass'])).'" WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
        }
        //phone
        if ((mb_strlen($_POST['phone'])>4) && (mb_strlen($_POST['phone'])<20)) {
            $this->db->query('UPDATE `shop` SET `phone`="'.$this->db->escape($_POST['phone']).'" WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
        }

        // work time
        $ok = true;
        $for_sql ='';
        for ($i=1; $i<8; $i++) {
            if ((@$_POST['d'.$i.'_from']==0 && @$_POST['d'.$i.'_to']!=0) || (@$_POST['d'.$i.'_to']==0 && @$_POST['d'.$i.'_from']!=0)  || (@$_POST['d'.$i.'_to']<0 || @$_POST['d'.$i.'_from']<0)) {
                $ok =false;
                break;
            }

            if ($_POST['d'.$i.'_from']>0 && $_POST['d'.$i.'_to']>0) {
                $from_hour = intval(mb_substr(intval($_POST['d'.$i.'_from']),0,-2));
                $from_minute= intval(mb_substr(intval($_POST['d'.$i.'_from']),-2));
                $to_hour = intval(mb_substr(intval($_POST['d'.$i.'_to']),0,-2));
                $to_minute= intval(mb_substr(intval($_POST['d'.$i.'_to']),-2));

                if (!  ($from_hour<=12 && $from_hour>=7 && $to_hour<=22 && $to_hour>=14
                    && ($from_minute===30 || $from_minute===00) && ($to_minute===30 || $to_minute===00)) ) {
                    $ok =false;
                    break;
                }
            }

            $for_sql.=(($i!=1) ? ' , ' : '').' d'.$i.'_start='.@$_POST['d'.$i.'_from'];
            $for_sql.=' , d'.$i.'_end='.@$_POST['d'.$i.'_to'];
        }
        if ($ok) {
            $this->db->query('UPDATE `shop` SET '.$for_sql.' WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
        }

        if (((@$_POST['ex_radius']==0) || (@$_POST['ex_radius']==100) || (@$_POST['ex_radius']==200) || (@$_POST['ex_radius']==300) || (@$_POST['ex_radius']==400) || (@$_POST['ex_radius']==500) || (@$_POST['ex_radius']==600) || (@$_POST['ex_radius']==700) || (@$_POST['ex_radius']==800) || (@$_POST['ex_radius']==900) || (@$_POST['ex_radius']==1000) )) {
            if (($_POST['ex_radius']==0) || (abs($_POST['ex_min_summ'])==0)) {
                $sql_set ='  ex_radius=0, ex_min_summ=0, ex_price=0, ex_free=0 ';
                $this->db->query('update shop set '.$sql_set.'  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
            } else {
                $sql_set =' ex_radius='.intval($_POST['ex_radius']).', ex_min_summ='.abs($_POST['ex_min_summ']).', ex_price='.abs(round(floatval(@$_POST['ex_price']),2)).', ex_free='.abs(round(floatval(@$_POST['ex_free']),2));
                $res= $this->db->query('SELECT `map_label`  FROM `shop`  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
                // one can turn ex_del on only if the map_label was set
                if ($res->row['map_label']!='')
                    $this->db->query('update shop set '.$sql_set.'  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
            }
        }

        $td_hour = intval(mb_substr(intval($_POST['td_get_order_until']),0,-2));
        $td_minute= intval(mb_substr(intval($_POST['td_get_order_until']),-2));
        if ($td_hour>=12 && $td_hour<=22 && ($td_minute==30 || $td_minute==00)) {
            if ((@$_POST['td_get_order_until']==0) || (abs(@$_POST['td_min_summ'])==0)) {
                $sql_set =' td_get_order_until=0, td_min_summ=0, td_price=0, td_free=0 ';
            } else {
                $sql_set =' td_get_order_until='.intval($_POST['td_get_order_until']).', td_min_summ='.abs($_POST['td_min_summ']).',td_price='.abs(round(floatval(@$_POST['td_price']),2)).', td_free='.abs(round(floatval(@$_POST['td_free']),2));
            }
            $this->db->query('update shop set '.$sql_set.'  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
        }

        $tm_hour = intval(mb_substr(intval($_POST['tm_get_order_until']),0,-2));
        $tm_minute= intval(mb_substr(intval($_POST['tm_get_order_until']),-2));
        if ($tm_hour>=12 && $tm_hour<=22 && ($tm_minute==30 || $tm_minute==00)) {
            if ((@$_POST['tm_get_order_until']==0) || (abs(@$_POST['tm_min_summ'])==0)) {
                $sql_set =' tm_get_order_until=0, tm_min_summ=0, tm_price=0, tm_free=0 ';
            } else {
                $sql_set =' tm_get_order_until='.intval($_POST['tm_get_order_until']).',  tm_min_summ='.abs($_POST['tm_min_summ']).',tm_price='.abs(round(floatval(@$_POST['tm_price']),2)).', tm_free='.abs(round(floatval(@$_POST['tm_free']),2));
            }
            $this->db->query('update shop set '.$sql_set.'  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);
        }

        if ((abs(intval(@$_POST['ukr_min_summ']))==0) || ($_POST['ukr_pay']=='0')) {
            $sql_set =' ukr_min_summ=0, ukr_pay="", `ukr_price`=0,  ukr_free=0 ';
        } else {
            $sql_set =' ukr_min_summ='.abs($_POST['ukr_min_summ']).', ukr_pay="'.(($_POST['ukr_pay']=='after') ? 'after' :  '').'", `ukr_price`='.abs(intval($_POST['ukr_price'])).', ukr_free='.abs(round(floatval(@$_POST['ukr_free']),2));
        }
        $this->db->query('update shop set '.$sql_set.'  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);

        //shop type
        $this->db->query('update shop set `type`="'.(($_POST['shop']==0) ? 'internet' : 'uni').'"  WHERE `id`='.intval($_POST['id_shop']).' AND `id_seller`='.$_SESSION['id']);

        // new discount
        if (@$_POST['new_dis_summ']>0 && @$_POST['new_dis']>0 && @$_POST['new_dis']<=9 ) {
            $this->db->query('insert into discounts set  discount='.intval($_POST['new_dis']).', summ='.round(floatval($_POST['new_dis_summ']),2).', id_shop='.intval($_POST['id_shop']));
        }
        // exist discounts
        for ($i=0; $i<30; $i++) {
            // if dis isset
            if (isset($_POST['dis_'.$i])) {
                // if dis is valid
                if ((@$_POST['dis_'.$i.'_summ']>=0 && @$_POST['dis_'.$i]>=0) && @$_POST['dis_'.$i]<=9 ) {
                    if (@$_POST['dis_'.$i]>0 && @$_POST['dis_'.$i.'_summ']>0) {
                        $sql ='update discounts set discount='.intval($_POST['dis_'.$i]).', summ='.round(floatval($_POST['dis_'.$i.'_summ']),2).'  where id='.intval($_POST['dis_'.$i.'_id'].' and id_shop='.intval($_POST['id_shop']));
                    } else {
                        $sql ='delete from discounts where id='.intval($_POST['dis_'.$i.'_id'].' and id_shop='.intval($_POST['id_shop']));
                    }
                    $this->db->query($sql);
                }
            } else
                break;
        }

        $this->redirect();
    }

    private function deleteData() {
        $this->db->query('DELETE FROM  `shop`  WHERE `id`='.intval($_GET['id']).' AND `id_seller`='.$_SESSION['id']);
        $this->redirect();
    }

    private function redirect() {
        header('location: /?option=shops');
        exit;
    }


}