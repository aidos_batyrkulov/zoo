<?php
class feedbacks extends frame {
    private $comments;

    public function get_content() {
        $this->metaTitle=' Отзывы | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->getData();
        echo $this->showData();
        if ($this->page>1) exit;
    }

    private function getData() {
        $res = $this->db->query('SELECT * FROM `comment` WHERE id_shop IN (SELECT `id` FROM `shop` WHERE `id_seller`='.$_SESSION['id'].') '.$this->getLimitByPage());
        $this->comments = $res->rows;
    }

    private function showData() {
        ob_start();
        ?>
        <?php if (count($this->comments)==0) echo (($this->page>1) ? 'PAGE_END' : 'Пусто')?>
        <?php foreach ($this->comments as $comment) { ?>
            <div class="item feedbacks">
                <div><span><?=date('d.m.Y', $comment['time'])?></span></div>
                <div class="rate">
                    <div style="width:<?=$comment['ball']?>%"></div>
                    <span><?=$comment['ball']?></span>
                </div>
                <div>
		    			<span>
		    			    <?=$comment['comment']?>
                        </span>
                    <span><?=$comment['shop_name']?>, <?=$comment['shop_adr']?></span>
                </div>
            </div>
        <?php } ?>
        <?php
        $html = ob_get_clean();
        return $html;
    }


}