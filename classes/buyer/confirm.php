<?php
class confirm extends frame {
    private $seller_phone;

	public function get_content() {
		$this->metaTitle='Заказ | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
		$this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

		$this->getData();
		echo $this->showData();
	}

	private  function getData() {
	    @session_start();
	    if (isset($_SESSION['confirm_seller_phone'])) {
            $this->seller_phone = $_SESSION['confirm_seller_phone'];
            unset($_SESSION['confirm_seller_phone']);
        }
	    else header('location: /');
    }

    private  function showData()  {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <div class="form confirm">
            <span><?=$this->seller_phone?></span>
        </div>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }
}