<?php
class register extends frame {
    public function get_content() {
        $this->metaTitle='Регистрация | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Бесплатная регистрация зоомагазина, &#9989;легкая настройка, &#9989;загрузка зоотоваров одним exel-файлом! &#9989;7 минут и Ваш товар видят Покупатели';

        $this->pageScript='<script src="//zooskop.com/js/jquery.maskedinput.min.js" type="text/javascript"></script>';



        if (isset($_POST['email'])) {
            $error=array();

            if ((!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))||(strlen($_POST['email'])<6)||(strlen($_POST['email'])>50)) {
                $error['email']='<br>Вы указали некорректный адрес эл.почты. Максимальная длина эл.почты 50 символ, минимальная - 6';
            } else {
                $res= $this->db->query('select `id` from `sellers` where email="'.$this->db->escape($_POST['email']).'" limit 1');
                if ($res->num_rows>0) {
                    $error['email'] = '<br>Данный эл.почта занята!';
                }
            }
            if ((!preg_match('/^[a-z0-9]*$/i' ,$_POST['password']))||(strlen($_POST['password'])<6)||(strlen($_POST['password'])>32)) {
                $error['password']='<br>Максимальная длина пароля 32 символ, минимальная - 6. Доступно буквы лат.алф. и цифры';
            }

            if ($_POST['password']!==$_POST['password2']) {
                $error['password2']='<br>Пароли не совпадают!';
            }

            if ((strlen($_POST['phone'])<4)||(strlen($_POST['phone'])>20)) {
                $error['phone']='<br>Максимальная длина номера 20 символ, минимальная - 4';
            }

            if (strlen($_POST['fio'])<1) {
                $error['shop']='<br>Введите название Вашего магазина';
            }

            if (count($error)<1) {
                $sql='INSERT INTO `seller`  set time='.time().', `password`="'.$this->db->escape($this->security->pass($_POST['password'])).'",  
				`email`="'.$this->db->escape($_POST['email']).'",  `phone`="'.$_POST['phone'].'", `fio`="'.$this->db->escape($_POST['fio']).'" ';
                $res=$this->db->query($sql);
                session_start();
                $_SESSION['id'] = $this->db->insertId();
                $_SESSION['email'] =$_POST['email'];

                $res= $this->db->query('select email from admin_emails where type="new_seller"');
                if ($res->num_rows>0) {
                    $message ='Зарегистрирован новый продавец: '.$_SESSION['email'];
                    $header = 'Новый продавец ('.date('d-m-y H:i').')';
                    $mail = $res->row['email'];
                    $this->mail->send_mail($mail, $header, $message);
                }
                header('location: /?option=login');
            } else {
                $error_text='
				<div class="error-win">';
                foreach($error as  $err) {
                    $error_text.=$err;
                }
                $error_text.='</div>';
            }
        }

        echo @$error_text.'


		<div class="form register">
			<form method="post" action="/?option=register">
			    <input name="fio" required="required" type="text" placeholder="ФИО или название Организации"/>
			    <input name="email" required="required" type="text" placeholder="Электронная почта"/>
				<input name="password" required="required" type="password" placeholder="Пароль"/>
				<input name="password2" required="required" type="password" placeholder="Подтвердить пароль"/>
				<input name="phone" required="required" id="phone" type="text" placeholder="Телефон"/>
				<span><a href="term.html">Условия Использования</a></span>
				<input type="submit" value="Регистрация на сайте"/>
			</form>
		</div>

		<script>
			$("#phone").mask("+38(099)999-99-99", {completed: function(){}});
		</script>
		';
    }


}
