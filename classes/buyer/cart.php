<?php
class cart extends frame {
    private $shops; // shops will be ordered with 3 steps: 1-polnota, 2-delType (if the buyer set geo), 3-rating
    private $lngLat = []; // it's an array which has two elements: lng, lat
    private $cart_pds = [];
    /*
     * input data:
     *  cookie: black_list, *cart_code
     *  get:
     *  post:
     */
    public function get_content() {
        if (isset($_POST['check_sms_code'])) {exit($this->checkSmsCode());}
        if (isset($_POST['order'])) {exit($this->order());}
        if (isset($_POST['pd_code']) && isset($_POST['label'])) {exit($this->quantity());}
        if (isset($_POST['sms_phone'])) {exit($this->send_sms());}

        $this->metaTitle='Магазины | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->pageScript='<script src="//zooskop.com/js/jquery.maskedinput.min.js" type="text/javascript"></script>';

        // access
        $redirect = false;
        if (!isset($_COOKIE['cart_code']) || mb_strlen(@$_COOKIE['cart_code'])!=10) {
            $redirect = '/';
        } else {
            $res = $this->db->query('select id from cart where cart_code="'.$this->db->escape($_COOKIE['cart_code']).'" limit 1');
            if ($res->num_rows<1) {
                $redirect = '/';
            }
        }
        if ($redirect!=false) {
            header('location: '.$redirect);
            exit;
        }

        $this->getData();
        echo  $this->showData();
    }

    public function getData() {
        $this->lngLat =  $this->getLngLat();
        $ids = $this->get_shop_ids($this->get_black_list());
        $this->shops = $this->get_shops_data($this->order_with_delType_and_rating($ids));
        $this->cart_pds = $this->get_cart_pds();
    }

    public  function showData() {
        ob_start();
        ?>
        <!-- <<<<< MODAL ORDER <<<<< -->
        <div class="modal order" id="modalOrder" style="display:none;">
            <div class="item">
                <div id="step1">
                    <input type="hidden" id="sh-index">
                    <div class="close" onclick="toggle(modalOrder)"></div>
                    <label class="shop">
                        <input class="check" type="radio" name="delType" value="shop" id="ch">
                        <div class="nRadio"></div>
                        <span class="type"></span>
                    </label>
                    <label class="express">
                        <input class="check" type="radio" name="delType" value="ex" id="ch">
                        <div class="nRadio"></div>
                        <span class="type"></span>
                        <span class="price"></span>
                        <span class="free"></span>
                        <span class="min"></span>
                    </label>
                    <label class="today">
                        <input class="check" type="radio" name="delType"  value="td" id="ch">
                        <div class="nRadio"></div>
                        <span class="type"></span>
                        <span class="price"></span>
                        <span class="free"></span>
                        <span class="min"></span>
                    </label>
                    <label class="tomorrow">
                        <input class="check" type="radio" name="delType"  value="tm" id="ch">
                        <div class="nRadio"></div>
                        <span class="type"></span>
                        <span class="price"></span>
                        <span class="free"></span>
                        <span class="min"></span>
                    </label>
                    <label class="country">
                        <input class="check" type="radio" name="delType"  value="ukr"  id="ch">
                        <div class="nRadio"></div>
                        <span class="type"></span>
                        <span class="price">70</span>
                        <span class="free">250.00</span>
                        <span class="min">600.00</span>
                    </label>
                    <input required="required" type="text" id="buyer_phone" placeholder="Телефон"/>
                    <input type="button" id="btn_send_sms" value="Продолжить"/>
                </div>
                <div id="step2" style="display:none;">
                    <input required="required" type="text" id="buyer_name" placeholder="Ваше имя"/>
                    <input required="required" type="text" id="city" placeholder="Город"/>
                    <textarea maxlength="500" id="adr" placeholder="Адрес доставки:" ></textarea>
                    <input required="required" id="sms_code" type="text" placeholder="СМС код"/>
                    <input required="required" type="text" id="email" placeholder="E-mail для отзыва (не обязательно)"/>
                    <input type="button" id="btn_send_order" value="Заказать!"/>
                </div>
            </div>
        </div>
        <!-- <<<<< MODAL ORDER <<<<< -->


        <!-- >>>>> CONTENT >>>>> -->
        <div class="geoSearch"></div>

        <span id="repaint"></span>

        <!-- <<<<< END CONTENT <<<<<<< -->

        <script>
            phone_valid = false;
            shIndex = false;
            pd = false;
            sh = false;

            $(document).ready(function () {
                json_pd = '<?=json_encode($this->cart_pds)?>';
                json_sh = '<?=(count($this->shops)>0 ? json_encode($this->shops) : 0) ?>';

                if (json_sh!='0') {
                    pd = $.parseJSON(json_pd);
                    sh = $.parseJSON(json_sh);
                }
                repaint();

                $("body").on("click", ".minus", function(event){  new_qtty($(this).attr("data-pd-code"), "-");     });
                $("body").on("click", ".plus", function(event){  new_qtty($(this).attr("data-pd-code"), "+");     });
                $('body').on("click", "#btn_send_sms", function(event){  send_sms();});
                $('body').on("click", "#btn_send_order", function(event){  send_order();});
                $('body').on("click", ".geoSearch", function(event){  geo();});
                $("#buyer_phone").mask("+38(099)999-99-99", {completed: function(){phone_valid=true; }});
            });

            function no_shops() {
                $("#repaint").html('<h3 class="alarm">В магазинах нерабочее время, обычно магазины работают с 7:00 до 21:00</h3>');
            }

            function geo() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var lngLat = position.coords.longitude+','+position.coords.latitude;
                        $.ajax({url: '/map.php', data: 'map_label='+lngLat, global: false, success: function(data) {
                            if ((data.indexOf('CANNOT_READ_ADR')+1)==0) {
                                arr = $.parseJSON(data);
                                $.cookie("map_label", lngLat, {expires: 90, path: "/"});
                                $.cookie("city", arr.city, {expires: 90, path: "/"});
                                document.location.href = '/?option=cart';
                            } else  alert('Не удалось получить адрес по geo координатам!');
                        }});
                    });
                } else alert('Вы не дали разрешение на чтение GEO координатов или ваш браузер не поддерживает GEO!');
            }

            function new_qtty(pd_code, label) {
                for (i=0; i<pd.length; i++) {
                    if (pd[i].pd_code==pd_code) {
                        $.ajax( {
                            url: "/?option=cart",
                            data: "pd_code="+pd_code+"&label="+((label=='+') ? 'plus' : 'minus'),
                            success: function(data, textStatus, XHR) {
                                var to_url = false;

                                if (label=="+") pd[i].quantity++;
                                else pd[i].quantity--;

                                // cleaning products which have zero quantity
                                var new_pd = [];
                                var i3=0;
                                for (i2=0; i2<pd.length; i2++) {
                                    if (pd[i2].quantity>0) {
                                        new_pd[i3]= [];
                                        new_pd[i3] = pd[i2];
                                        i3++;
                                    }
                                }
                                if (new_pd.length>0) {
                                    // now there will be only those shops which have at least one product with enough quantity
                                    var new_sh = [];
                                    var g4 = 0;
                                    for (g=0; g<sh.length; g++) {
                                        var break2  = false;
                                        for (g2=0; g2<sh[g].prices.length; g2++) {
                                            for (g3=0; g3<new_pd.length; g3++) {
                                                if ((sh[g].prices[g2].pd_code==new_pd[g3].pd_code) && (sh[g].prices[g2].quantity>=new_pd[g3].quantity)) {
                                                    new_sh[g4] = sh[g];
                                                    g4++;
                                                    break2=true;
                                                    break;
                                                }
                                            }
                                            if (break2===true) break;
                                        }
                                    }
                                    sh = new_sh;
                                    new_sh=null;

                                    pd = null;
                                    pd = [];
                                    pd = new_pd;
                                    new_pd = null;
                                    repaint();
                                } else {
                                    $("#repaint").html("");
                                    $.removeCookie("map_label", {path: "/"});
                                    $.removeCookie("city", {path: "/"});
                                    to_url = '/?option=catalog';
                                }
                                if (to_url!==false) {
                                    document.location.href = to_url;
                                }
                            }
                        });
                        break;
                    }
                }
            }

            function init_shop(sh) {
                if (sh.meter==100000) {
                    sh.adr_class = 'city';
                    sh.adr_text = sh.city;
                } else if (sh.meter==1000000 || sh.meter==1000001) {
                    sh.adr_class= 'country';
                    sh.adr_text = sh.city;
                } else {
                    sh.adr_class = 'near';
                    sh.adr_text = sh.meter;
                }

                var big_summ=0;
                for (i2=0; i2<pd.length; i2++) {
                    var price=0;
                    for (i3=0; i3<sh.prices.length; i3++) {
                        if ((sh.prices[i3].pd_code==pd[i2].pd_code) && (sh.prices[i3].quantity>=pd[i2].quantity)) {
                            price=sh.prices[i3].price;
                            break;
                        }
                    }
                    var summ =0;
                    summ = price*pd[i2].quantity;
                    big_summ = +big_summ+summ;
                }

                var dis_big_summ=0;
                var dis_require_summ = 0;
                var dis_is= 0;
                if (sh.dis!=0) {
                    for (i4=0; i4<sh.dis.length; i4++) {
                        if (big_summ>=sh.dis[i4].summ) {
                            dis_is = sh.dis[i4].discount;
                            dis_require_summ = sh.dis[i4].summ;
                            var v = big_summ / 100;
                            var v2 = v * dis_is;
                            dis_big_summ=  big_summ-v2;
                            break;
                        }
                    }
                }

                sh.dis_is = dis_is;
                sh.dis_require_summ = dis_require_summ;

                sh.big_summ = big_summ;
                sh.dis_big_summ = dis_big_summ;
                sh.total_html = '<span>'+big_summ.toFixed(2)+'</span> \n';
                if (dis_big_summ>0) {
                    sh.total_html+= '<span>'+dis_big_summ.toFixed(2)+'</span>';
                }

                return sh;
            }

            function html_pds(sh) {
                var html ='';
                for(a=0; a<pd.length; a++) {
                    var zero = 'zero';
                    var price =0;
                    var summ=0;
                    for(b=0; b<sh.prices.length; b++) {
                        if ((pd[a].pd_code==sh.prices[b].pd_code) && (sh.prices[b].quantity>=pd[a].quantity)) {
                            zero = '';
                            price = sh.prices[b].price;
                            summ = sh.prices[b].price*pd[a].quantity;
                            break;
                        }
                    }
                    html+= '<div class="product '+zero+'">\n' +
                    '            <img src="catalog/'+pd[a].photo+'.png">\n' +
                    '            <div class="specifications">\n' +
                    '                <div>\n' +
                    '                    <span>'+pd[a].brand+'</span>\n' +
                    '                    <span>'+pd[a].name+'</span>\n' +
                    '                </div>\n' +
                    '                <div>\n' +
                    '                    <span>'+pd[a].pd_code+'</span>\n' +
                    '                    <span>'+pd[a].subgr_name+'</span>\n' +
                    '                </div>\n' +
                    '            </div>\n' +
                    '            <div class="cost">\n' +
                    '                <span>'+price+'</span>\n' +
                    '                <div class="minus" data-pd-code="'+pd[a].pd_code+'"></div>\n' +
                    '                <span>'+pd[a].quantity+'</span>\n' +
                    '                <div class="plus"  data-pd-code="'+pd[a].pd_code+'"></div>\n' +
                    '                <span>'+summ.toFixed(2)+'</span>\n' +
                    '            </div>\n' +
                    '        </div>';
                }

                return html;
            }

            function repaint() {
                big_count =0;
                if (json_sh=='0' || sh.length==0) {
                    no_shops();
                    return;
                }
                for (f=0; f<pd.length; f++) {big_count+=(+pd[f].quantity);}
                var html='';
                for (i=0; i<sh.length; i++) {
                    var shop = init_shop(sh[i]);
                    sh[i] = shop;
                    html+='<div class="cell">\n' +
                        '            <div class="offer">\n' +
                        '                <div class="time">\n' +
                        '                    <span>'+shop.start.hour+'<sup>'+shop.start.minute+'</sup></span>\n' +
                        '                    <span>'+shop.end.hour+'<sup>'+shop.end.minute+'</sup></span>\n' +
                        '                </div>\n' +
                        '                <div class="rate">\n' +
                        '                    <div style="width:'+shop.rating+'%"></div>\n' +
                        '                    <span>'+shop.rating+'</span>\n' +
                        '                </div>\n' +
                        '                <div class="shopAddress '+shop.adr_class+'">\n' +
                        '                    <span>'+shop.adr_text+'</span>\n' +
                        '                </div>\n' +
                        '                '+html_pds(shop)+'  \n' +
                        '                <input  data-sh-index="'+i+'" type="button" onclick="modalSh($(this).attr(\'data-sh-index\'));"  value="Заказать товары"/>\n' +
                        '                <div class="total">\n' +
                        '                    '+shop.total_html+' \n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '        </div>';

                }
                $('#countCatalog').html(big_count);
                $("#repaint").html(html);
            }

            function modalSh(i) {
                var s = sh[i];
                shIndex = i;

                var shop = $('.shop');
                var delTypeElements = [];
                delTypeElements.push($('.express'));
                delTypeElements.push($('.today'));
                delTypeElements.push($('.tomorrow'));
                delTypeElements.push($('.country'));

                $('input:radio').prop('checked', false);
                $('#sh-index').val(i);
                toggle(modalOrder);
                // shop
                if (s.type=='internet') shop.css('display', 'none');
                else  shop.css('display', 'block');

                for (h=0; h<delTypeElements.length; h++) {
                    var delTypeActive = 0;
                    var delTypePrice =0;
                    var delTypeMinSumm =0;
                    var delTypeFree = 0;
                    switch (h) {
                        case 0: delTypeActive = s.delTypes.ex; delTypePrice= s.ex_price; delTypeMinSumm = s.ex_min_summ; delTypeFree= s.ex_free; break;
                        case 1: delTypeActive = s.delTypes.td; delTypePrice= s.td_price; delTypeMinSumm = s.td_min_summ; delTypeFree= s.td_free; break;
                        case 2: delTypeActive = s.delTypes.tm; delTypePrice= s.tm_price; delTypeMinSumm = s.tm_min_summ; delTypeFree= s.tm_free; break;
                        case 3: delTypeActive = s.delTypes.ukr;delTypePrice= s.ukr_price; delTypeMinSumm = s.ukr_min_summ; delTypeFree= s.ukr_free; break;
                    }
                    // if the shop doesn't support current type of the delivery
                    if (delTypeActive!=1) delTypeElements[h].css('display', 'none');
                    else {
                        delTypeElements[h].css('display', 'block');
                        delTypeElements[h].children('span').filter('.price').html(delTypePrice);
                        delTypeElements[h].children('span').filter('.min').html(delTypeMinSumm);
                        delTypeElements[h].children('span').filter('.free').html(delTypeFree);
                        if (s.big_summ<delTypeMinSumm) {
                            delTypeElements[h].children('span').filter('.min').css('display', 'block');
                            delTypeElements[h].children('span').filter('.free').css('display', 'none');
                            delTypeElements[h].children('input').attr('disabled','disabled');
                        } else {
                            delTypeElements[h].children('span').filter('.min').css('display', 'none');
                            delTypeElements[h].children('input').removeAttr('disabled');
                            if (delTypeFree==0)
                                delTypeElements[h].children('span').filter('.free').css('display', 'none');
                            else if ((delTypeFree>0) && (s.big_summ>=delTypeFree)) {
                                delTypeElements[h].children('span').filter('.free').css('display', 'none');
                                delTypeElements[h].children('span').filter('.price').css('display', 'none');
                            }
                        }
                    }
                }
            }

            function send_sms() {
                delType = false;
                phone = false;
                delPrice = 0;
                delMinSumm = 0;
                delFree = 0;

                if ($('input[name=delType]:checked').length>0) {delType = $('input[name=delType]:checked').val();}
                if (phone_valid==true) {phone = $('#buyer_phone').val();}

                if (delType!==false && phone!==false) {
                    if (delType!='shop') {
                        delPrice =   $('input[name=delType]:checked').parent().children('span').filter('.price').html();
                        delMinSumm = $('input[name=delType]:checked').parent().children('span').filter('.min').html();
                        delFree =    $('input[name=delType]:checked').parent().children('span').filter('.free').html();
                    }
                    $.ajax( {
                        url: "/?option=cart",
                        data: 'sms_phone='+phone,
                        success: function(data, textStatus, XHR) {
                            if (data.indexOf('NUMBER_BANNED')+1) {
                                $.removeCookie("map_label", {path: "/"});
                                $.removeCookie("city", {path: "/"});
                                document.location.href = '/';
                            } else if (data.indexOf('WAS_SENT')+1){
                                toggle(step1);
                                if ($.cookie('city') != undefined) {
                                    $('#city').val($.cookie('city'));
                                    $('#city').prop( 'disabled', true);
                                }
                                if (delType=='shop') {
                                    $('#city').hide();
                                    $('#adr').hide();
                                }
                                toggle(step2);
                            } else {
                                alert('Не удалось отправить смс, попробуйте еще раз');
                            }

                        }
                    });
                } else   {
                    var  error_text='';
                    if (phone==false) error_text='Введите номер телефона корректно';
                    if (delType==false) error_text='Выберите тип доставки или выберите самовызов';
                    alert(error_text);
                }

            }

            function send_order() {
                buyer_name = false;
                city = false;
                adr = false;
                sms_code = false;
                email = false;

                if ($('#buyer_name').val().length>0) buyer_name = $('#buyer_name').val();
                if ($('#city').val().length>0 || delType=='shop') city = $('#city').val();
                if ($('#adr').val().length>0 || delType=='shop') adr = $('#adr').val();
                if ($('#email').val().length>0) email = $('#email').val();
                if ($('#sms_code').val().length>0) sms_code = $('#sms_code').val();

                if (buyer_name!==false && city!==false && adr!==false && sms_code!==false) {
                    $.post('/?option=cart', 'check_sms_code='+sms_code, function (data) {
                        if (data.indexOf('CONFIRMED')+1) {
                            var prices = sh[shIndex].prices;
                            var new_prices = [];
                            for (i=0; i<prices.length; i++) {
                                for (i2=0; i2<pd.length; i2++) {
                                    if (prices[i].pd_code==pd[i2].pd_code && prices[i].quantity>=pd[i2].quantity) {
                                        var a_price = prices[i];
                                        a_price.quantity = pd[i2].quantity;
                                        new_prices.push(a_price);
                                        break;
                                    }
                                }
                            }

                            var new_shop={
                                id: sh[shIndex].id,
                                map_label: sh[shIndex].map_label,
                                city: sh[shIndex].city
                            };
                            var delTypeData = {
                                type: delType,
                                price: delPrice,
                                min_summ: delMinSumm,
                                free: delFree
                            };
                            var discount = {
                                discount: sh[shIndex].dis_is,
                                summ: sh[shIndex].dis_require_summ
                            };
                            var delAdrData = {
                                buyer_name: buyer_name,
                                city: city,
                                adr: adr,
                                email: email
                            };

                            var order = {
                                shop: new_shop,
                                prices: new_prices,
                                delTypeData: delTypeData,
                                discount: discount,
                                delAdrData: delAdrData
                            };
                            var order_json = JSON.stringify(order);
                            $.post('/?option=cart', 'order='+encodeURIComponent(order_json), function (data) {
                                ajaxing = false;
                                /*
                                * Ajaxing is turned off here because of the firefox calls
                                * the ajaxStop only after this functions runs to the end.
                                * So, this function never runs to the end because
                                * anyway it redirects. If the ajaxing is not turned off here
                                * then firefox always asks not to close the page.
                                * */
                                if (data.indexOf('ORDERED')+1) {
                                    document.location.href = '/?option=confirm';
                                } else  {
                                    alert('Заказ не принят, так как магазин обновил данные');
                                    document.location.href = '/?option=cart';
                                }
                            });
                        } else  alert('Не верный смс-код')
                    });
                } else alert('Заполните данные');
            }

        </script>


        <?php
        $html = ob_get_clean();
        return $html;
    }










    /*
	 * The function returns ids  of 10+- shops which are open and selling those products
	 * which are in the buyer cart.
     * The returned ids are ordered with the 1st step (polnota)
     * If the buyer didn't set geo then we'll show only those shops which support ukr delivery or with uni type
	 */
    private  function get_shop_ids($black_list) {
        $ids = [];
        $cart_code = $_COOKIE['cart_code'];
        $time = intval(date('H').date('i'));
        $dN = date('N');

        $sql_and = '';
        if (count($this->lngLat)==0)  $sql_and= ' AND (`ukr_pay`<>"" OR `type`="uni") '; // If the buyer didn't set geo

        $res= $this->db->query('
             select id from shop where (`active`=1) '.$sql_and.' and (d'.$dN.'_start<='.$time.' and d'.$dN.'_end>='.$time.') and ( id in 
                  (  select distinct id_shop from prices where '.(($black_list!='') ? ' id_shop NOT in ('.$black_list.') and ' : '').'  
                     quantity>0 and pd_code in
	                (select pd_code from cart where cart_code="'.$this->db->escape($cart_code).'")
	              ))');
        if ($res->num_rows>0) {
            $ids = $res->rows;
            for ($i=0; $i<count($ids); $i++) {
                $res = $this->db->query('select COUNT(id) as count from prices where id_shop='.$ids[$i]['id'].' and quantity>0  and pd_code in
                            (select pd_code from cart where cart_code="'.$this->db->escape($cart_code).'")');
                $ids[$i]['count'] = intval($res->row['count']);
            }
            // Array $ids is ordered with count
            usort($ids, function($a, $b){return ($b['count'] - $a['count']); });

            if (count($ids)>10) {
                $new_ids = [];
                $count = $ids[9]['count'];
                for ($i=10; $i<count($ids); $i++) {
                    if ($ids[$i]['count']==$count) $new_ids[$i] = $ids[$i];
                    else break;
                }
                for($i=0; $i<10; $i++) {
                    $new_ids[$i] = $ids[$i];
                }
                $ids = $new_ids;
            }
        }
        return $ids;
    }

    private function getLngLat() {
        $arr = [];
        if (isset($_COOKIE['map_label'])) {
            $arr = explode(',', $_COOKIE['map_label']);
            $lng = floatval($arr[0]);
            $lat = floatval($arr[1]);
            if ($lng!=0 && $lat!=0) {
                $arr['lng'] = $lng;
                $arr['lat'] = $lat;
            }
        }
        return $arr;
    }

    private function  get_cart_pds() {
        $pds = [];
        $cart_code = $_COOKIE['cart_code'];

        $res = $this->db->query('select * from `pd_subgrs` where `pd_code` in (select pd_code from cart where cart_code="'.$this->db->escape($cart_code).'")');
        for ($i=0; $i<count($res->rows); $i++) {
            $res2 = $this->db->query('select `name` from `group` where id='.$res->rows[$i]['id_subgr']);
            $res3 = $this->db->query('select `name`, `brand`, `photo` from `product` where id='.$res->rows[$i]['id_pd']);
            $res4 = $this->db->query('select `quantity` from `cart` where pd_code="'.$res->rows[$i]['pd_code'].'" and  cart_code="'.$this->db->escape($cart_code).'"');
            $res5 = $this->db->query('SELECT `name` FROM `brand` WHERE `id`='.$res3->row['brand']);
            $new_arr = [];
            $new_arr['id'] = $res->rows[$i]['id_pd'];
            $new_arr['name'] = $res3->row['name'];
            $new_arr['photo'] = $res3->row['photo'];
            $new_arr['pd_code'] = $res->rows[$i]['pd_code'];
            $new_arr['brand'] = $res5->row['name'];
            $new_arr['subgr_name'] = $res2->row['name'];
            $new_arr['quantity'] = $res4->row['quantity'];
            $pds[] = $new_arr;
        }

        return $pds;
    }

    private  function  get_black_list() {
        $black_list='';
        if (isset($_COOKIE['black_list']) && $black_list_arr = json_decode($_COOKIE['black_list'])) {
            for ($i=0; $i<count($black_list_arr); $i++) {
                $black_list.=(($i!=0) ? ',' : '').intval($black_list_arr[$i]);
            }
        }
        return $black_list;
    }

    private function get_shops_data($ids) {
        $shops=[];
        $cart_code = $_COOKIE['cart_code'];

        for ($i=0; $i<count($ids); $i++) {
            $res = $this->db->query('select * from shop where id='.$ids[$i]['id']);
            $res2 = $this->db->query('select `pd_code`, `price`, `quantity` from prices where (`id_shop`='.$ids[$i]['id'].') and (pd_code in (select pd_code from cart where cart_code="'.$this->db->escape($cart_code).'"))');
            $res3 = $this->db->query('select * from discounts where id_shop='.$ids[$i]['id'].' ORDER BY `discount` DESC ');
            $res->row['prices'] = $res2->rows;
            $res->row['dis'] = (($res3->num_rows>0) ? $res3->rows : 0);
            $res->row['meter'] = $ids[$i]['meter'];
            $res->row['delTypes'] = $this->turnOnAvailableDelTypes($res->row);
            $dN = date('N');
            $start = $res->row['d'.$dN.'_start'];
            $end = $res->row['d'.$dN.'_end'];
            if (mb_strlen($start)==3) $start= '0'.$start;
            if (mb_strlen($end)==3) $end= '0'.$end;
            $res->row['start']['hour'] = mb_substr($start,0,2);
            $res->row['start']['minute'] = mb_substr($start,2,2);
            $res->row['end']['hour'] = mb_substr($end,0,2);
            $res->row['end']['minute'] = mb_substr($end,2,2);
            $shops[]= $res->row;
        }

        return $shops;
    }

    private function turnOnAvailableDelTypes($shop) {
        // each del type will be turned on if the shop supports and it's available for the current buyer
        $delTypes = array('ex'=>0,'td'=>0, 'tm'=>0, 'ukr'=>0, 'shop'=>0);
        if  ($shop['meter']!=100000 && $shop['meter']!=1000000 &&  $shop['meter']!=1000001)
            $delTypes['ex'] = 1;
        if ($shop['meter']==100000) {
            if  ($shop['td_get_order_until']>0)
                $delTypes['td'] = 1;
            if  ($shop['tm_get_order_until']>0)
                $delTypes['tm'] = 1;
        }
        if  ($shop['meter'] == 1000000)
            $delTypes['ukr'] = 1;
        if (true)  // here instead of true we had to check if the shop type is uni but we didn't check because you know what is happening:)
            $delTypes['shop'] = 1;

        return $delTypes;
    }

    /*
     * The function returns array which includes 10 or few ids( with meter) of the shops which are ordered
     *  with delivery type (2nd step) and rating (3rd step)
     */
    private function order_with_delType_and_rating($ids) {
        $ordered_ids = [];

        if (count($this->lngLat)>0 && isset($_COOKIE['city'])) {
            $grouped_ids = []; // value of the index "count" will be used as index for the group
            foreach ($ids as $row) {
                $grouped_ids[$row['count']][] = $row['id'];
            }

            foreach ($grouped_ids as $item) {
                $ids_with_meter = [];
                foreach ($item as $id) {
                    $meter=0;
                    $res = $this->db->query('select `map_label`, `city`, `ex_radius`, `td_get_order_until`, `tm_get_order_until`, `ukr_pay`, `type`  from `shop` where `id`='.$id);
                    if (($res->row['ex_radius']>0) && ($res->row['map_label']!='') && (mb_strpos($res->row['city'], $_COOKIE['city'])!==false)) {
                        $distance = $this->geo->get_meter($this->lngLat['lng'].','.$this->lngLat['lat'], $res->row['map_label']);
                        if (($distance!==false) &&( $distance<= $res->row['ex_radius'])) {
                            $meter = $distance;
                        }
                    }
                    if (($meter==0) && ($res->row['td_get_order_until']>0 || $res->row['tm_get_order_until']>0) && (mb_stripos($res->row['city'], $_COOKIE['city'])!==false)) {
                        $meter = 100000; // =  city
                    }

                    if ($meter==0 &&  $res->row['ukr_pay']!='') {
                        $meter = 1000000; // = country
                    }
                    if ($meter==0) { // here we didn't check if the shop supports the ukr delivery type. Fix it in the future
                        $meter = 1000001; // shop
                    }

                    $new_arr = [];
                    $new_arr['id'] = $id;
                    $new_arr['meter'] = $meter;
                    $ids_with_meter[] = $new_arr;
                }
                usort($ids_with_meter, function ($a, $b) {return ($a['meter'] - $b['meter']);});

                // now we have to order city and country with rating (3rd step)
                $express = []; // we don't order this one
                $city = [];
                $country= [];
                foreach ($ids_with_meter as $value) {
                    if ($value['meter'] == 100000 || $value['meter']== 1000000) {
                        $res = $this->db->query('SELECT `rating` FROM `shop` WHERE `id`='.$value['id']);
                        $value['rating'] = $res->row['rating'];
                        if ($value['meter']==100000) $city[]= $value;
                        else $country[] = $value;
                    } else
                        $express [] = $value;
                }
                usort($city, function($a, $b){return ($b['rating'] - $a['rating']); });
                usort($country, function($a, $b){return ($b['rating'] - $a['rating']); });
                $ids_with_meter = []; // we rewrite this array
                foreach ($express as $ex)
                    $ids_with_meter[] = $ex;
                foreach ($city as $ci) {
                    unset($ci['rating']);
                    $ids_with_meter [] = $ci;
                }
                foreach ($country as $co) {
                    unset($co['rating']);
                    $ids_with_meter [] = $co;
                }


                foreach ($ids_with_meter as $arr) {
                    $ordered_ids[] = $arr;
                    if (count($ordered_ids)==10) break 2;
                }
            }
        } else {
            $ids_with_rating = [];
            foreach ($ids as $id) {
                $res = $this->db->query('SELECT `rating` FROM `shop` WHERE `id`='.$id['id']);
                $new_arr = [];
                $new_arr['id'] = $id['id'];
                $new_arr['rating'] = $res->row['rating'];
                $ids_with_rating[] = $new_arr;
            }
            usort($ids_with_rating, function($a, $b){return ($b['rating'] - $a['rating']); });
            for ($i=0; $i<count($ids_with_rating); $i++) {
                if ($i==10) break;// we get only 10 or few of them
                $res = $this->db->query('SELECT `ukr_pay`, `type` FROM `shop` WHERE `id`='.$ids_with_rating[$i]['id']);
                $new_arr = [];
                $new_arr['id'] = $ids_with_rating[$i]['id'];
                if ($res->row['ukr_pay']!='')
                    $new_arr['meter'] = 1000000;
                else
                    $new_arr['meter'] = 1000001;
                $ordered_ids[]= $new_arr;
            }
        }

        return $ordered_ids;
        /*
         * $ordered_ids[]['id']
         *               ['meter']
         */
    }

    private function quantity() {
        $pd_code = $_POST['pd_code'];
        $label = $_POST['label'];
        $cart_code = $_COOKIE['cart_code'];

        if ((mb_strlen($pd_code)<=16) && ($label=='plus' || $label=='minus') && (mb_strlen($cart_code)<=10)) {
            if ($label=='plus') {
                $res= $this->db->query('update `cart` set quantity=quantity+1 where `cart_code` = "'.$this->db->escape($cart_code).'" and `pd_code`="'.$this->db->escape($pd_code).'"');
            } else {
                $res= $this->db->query('select `quantity` from `cart` where `cart_code` = "'.$this->db->escape($cart_code).'" and `pd_code`="'.$this->db->escape($pd_code).'"');
                if ($res->num_rows>0) {
                    if ($res->row['quantity']>1) {
                        $res= $this->db->query('update `cart` set quantity=quantity-1 where `cart_code` = "'.$this->db->escape($cart_code).'" and `pd_code`="'.$this->db->escape($pd_code).'"');
                    } else {
                        $res= $this->db->query('delete from `cart` where `cart_code` = "'.$this->db->escape($cart_code).'" and `pd_code`="'.$this->db->escape($pd_code).'"');
                    }
                }
            }
        }
        return;
    }

    private function send_sms() {
        $cart_code = intval($_COOKIE['cart_code']);
        $phone = str_replace(array(' ','+','(',')','-'),'',$_POST['sms_phone']);
        $code = rand(11111,99999);


        if ($this->sms->check_phone('+'.$phone)) {
            $res= $this->db->query('SELECT `active` FROM `buyer` WHERE `phone_n`='.$phone.' LIMIT 1 ');
            if ($res->num_rows>0 && $res->row['active']==0) {
                $this->db->query('DELETE FROM `cart` WHERE `cart_code`="'.$cart_code.'"');
                return 'NUMBER_BANNED';
            }
            if ($res->num_rows==0 || (!empty($res->row['active'])) ) {
                session_start();
                $_SESSION['code'] = $code;
                $_SESSION['phone'] = $phone;
                $_SESSION['sms_confirmed'] = false;
                if ($this->sms->send_sms($phone, $code))
                    return 'WAS_SENT';
                else
                    return 'COULD_NOT_SEND';
            }
        }
    }

    private function checkSmsCode() {
        $code = intval($_POST['check_sms_code']);

        @session_start();
        if (isset($_SESSION['phone']) && isset($_SESSION['code']) && isset($_SESSION['sms_confirmed']) && $code===$_SESSION['code']) {
            $_SESSION['sms_confirmed']= true;
            unset($_SESSION['code']);
            return 'CONFIRMED';
        }
        return 'INVALID_CODE';
    }

    private function order() {
        @session_start();
        if ($_SESSION['sms_confirmed']==true) {
            $order = json_decode($_POST['order'], true);
            $dayN = date('N');
            $time = intval(date('Hi'));

            // first we check if there is a shop which sells ALL of those products with those PRICES and enough QUANTITY
            $sql_plus = '';
            // we use these vars later
            $big_summ = 0;
            $big_summ_after_dis = 0;
            foreach ($order['prices'] as $price) {
                $big_summ = bcadd($big_summ, bcmul(round(floatval($price['price']), 2), intval($price['quantity']), 2), 2);
                if ($sql_plus != '') $sql_plus .= ' OR ';
                $sql_plus .= ' (`pd_code`="' . intval($price['pd_code']) . '" AND `price`=' . round(floatval($price['price']), 2) . ' AND `quantity`>=' . intval($price['quantity']) . ' ) ';
            }
            $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `prices` WHERE `id_shop`=' . $this->db->escape($order['shop']['id']) . ' AND (' . $sql_plus . ')');
            if ($res->row['count'] == count($order['prices'])) {
                // so now we check if the shop supports that delType
                $sql_plus = '';
                if ($order['delTypeData']['type'] == 'shop') {
                    $sql_plus = ' `type`="uni" ';
                } else if ($order['delTypeData']['type'] == 'ex') {
                    $sql_plus = ' `ex_radius`>0 ';
                } else if ($order['delTypeData']['type'] == 'td') {
                    $sql_plus = ' `td_get_order_until`>' . $time;
                } else if ($order['delTypeData']['type'] == 'tm') {
                    $sql_plus = ' `tm_get_order_until`>' . $time;
                } else {
                    $sql_plus = ' `ukr_pay`<>"" ';
                }

                // here we check if the shop didn't change price, min_summ and free of the delivery
                $delType = $this->db->escape($order['delTypeData']['type']);
                $sql_plus2 = ' AND `' . $delType . '_price`=' . round(floatval($order['delTypeData']['price']), 2) . ' AND `' . $delType . '_min_summ`=' . round(floatval($order['delTypeData']['min_summ']), 2) . ' AND `' . $delType . '_free`=' . round(floatval($order['delTypeData']['free']), 2);
                if ($delType == 'shop') $sql_plus2 = '';
                // if the order has a discount then we check it here
                $sql_plus3 = '';
                if ($order['discount']['discount'] > 0) {
                    $sql_plus3 = ' AND `id` IN ( SELECT `id_shop` FROM `discounts` WHERE `id_shop`=' . intval($order['shop']['id']) . ' AND  `discount`=' . intval($order['discount']['discount']) . ' AND `summ`=' . round(floatval($order['discount']['summ']), 2) . ' )';
                    $v = bcdiv($big_summ, 100, 2);
                    $v2 = bcmul($v, $order['discount']['discount'],2);
                    $big_summ_after_dis=  bcsub($big_summ,$v2,2);
                }
                // just runing the sql
                $res = $this->db->query('
                    SELECT * FROM `shop` WHERE  `id`=' . intval($order['shop']['id']) . ' AND `active`=1 AND `map_label`="' . $this->db->escape($order['shop']['map_label']) . '" AND `city`="' . $this->db->escape($order['shop']['city']) . '" 
                    AND  ' . $sql_plus . $sql_plus2 . ' 
                    AND `d' . $dayN . '_start`<' . $time . ' AND `d' . $dayN . '_end`>' . $time . ' 
                    ' . $sql_plus3 . ' LIMIT 1 ');

                if ($res->num_rows > 0) {
                    $makeOrder = false;  // a label
                    // now we try to turn this label on
                    if ($order['delTypeData']['type'] == 'ex') {
                        $this->lngLat =  $this->getLngLat();
                        if (count($this->lngLat) > 0) {
                            $distance = $this->geo->get_meter($this->lngLat['lng'] . ',' . $this->lngLat['lat'], $res->row['map_label']);
                            if (($distance !== false) && ($distance <= $res->row['ex_radius'])) {
                                $makeOrder = true; // Because it's OK with distance
                            }
                        }
                    } else if ($order['delTypeData']['type'] == 'td' || $order['delTypeData']['type'] == 'tm') {
                        if (mb_strpos($res->row['city'], $order['delAdrData']['city']) !== false) {
                            $makeOrder = true; // Because it's Ok with city
                        }
                    } else  // if the delType is shop or ukr then we just turn the label on
                        $makeOrder = true;

                    // now we try to turn the label off
                    if ($big_summ < $order['delTypeData']['min_summ'] && $order['delTypeData']['type'] != 'shop')
                        $makeOrder = false;
                    if ($order['discount']['discount'] > 0 && $big_summ < $order['discount']['summ'])
                        $makeOrder = false;
                    if (empty($order['delAdrData']['buyer_name']) || ($order['delTypeData']['type'] != 'shop' && (empty($order['delAdrData']['city']) || empty($order['delAdrData']['adr']))) )
                        $makeOrder = false;

                    if ($makeOrder) {
                        $shop = $res->row;
                        $phone = $_SESSION['phone'];
                        unset($_SESSION['sms_confirmed']);
                        unset($_SESSION['phone']);

                        $res = $this->db->query('SELECT * FROM `buyer` WHERE `phone_n`='.$phone.' LIMIT 1');

                        $email = '';
                        if (filter_var($order['delAdrData']['email'], FILTER_VALIDATE_EMAIL)) $email = $order['delAdrData']['email'];
                        if ($res->num_rows>0) {
                            $buyer = $res->row;
                            $buyer_id = $buyer['id'];
                            $buyer_name = $buyer['fio'];
                            if ($buyer['active']==0)
                                $makeOrder = false;
                            else {
                                if ($email=='' && $buyer['email']!='')  $email = $buyer['email'];
                                $this->db->query('UPDATE `buyer` SET `count_ord`=`count_ord`+1, `summ`=`summ`+'.(($big_summ_after_dis>0) ? $big_summ_after_dis : $big_summ).' '.(($email!='') ? ',`email`="'.$this->db->escape($email).'"' : '').' WHERE `phone_n`='.$phone);
                            }
                        } else {
                            $this->db->query('
                                INSERT INTO `buyer` SET 
                                    `phone_n`='.$phone.',
                                    `phone_txt`="'.$phone.'",
                                    `fio`="'.$this->db->escape($order['delAdrData']['buyer_name']).'",
                                    `email`="'.$email.'",
                                    `summ`='.(($big_summ_after_dis>0) ? $big_summ_after_dis : $big_summ).',
                                    `count_ord`=1,
                                    `time`='.time()
                            );
                            $buyer_name = $order['delAdrData']['buyer_name'];
                            $buyer_id = $this->db->insertId();
                        }
                        if ($makeOrder) {
                            $sql = ' INSERT INTO `orders` SET  
                            `id_shop`    = '.$shop['id'].',
                            `id_buyer`   = '.$buyer_id.',
                            `buyer_name` = "' . $this->db->escape($buyer_name) . '" ,
                            `cart_code`  = "' . $this->db->escape($_COOKIE['cart_code']) . '" ,
                            `country`    = "Украина" ,
                            `city`   	 = "' . $this->db->escape($order['delAdrData']['city']) . '" ,
                            `street`  	 = "" ,
                            `home_number`= "' . $this->db->escape($order['delAdrData']['adr']) . '" ,
                            `delType`    = "' . $this->db->escape($order['delTypeData']['type']) . '" ,
                            `delPrice`   = ' . round(floatval($order['delTypeData']['price']),2) . ' ,
                            `summ`       = ' . $big_summ . ' ,
                            `discount`   = ' . intval($order['discount']['discount']) . ' ,
                            `time`        = ' . time() . ',
                            `confirmed`  = 1,
                            `seller_paid` = '.(($order['delTypeData']['type']!='shop') ? config::$order_cost_for_seller : config::$shop_order_cost_for_seller).',
                            `code_for_com` = "'.rand(111111111,999999999).'",
                            `ip`    	 = "' . $_SERVER['REMOTE_ADDR'] . '"
                            ';
                            $this->db->query($sql);
                            $new_order_id = $this->db->insertId();

                            $sql = 'INSERT INTO `order_pds` (id_order, pd_code, price, quantity) VALUE ';
                            for ($i = 0; $i < count($order['prices']); $i++) {
                                $sql .= (($i != 0) ? ',' : '') . ' (' . $new_order_id . ', "' . $this->db->escape($order['prices'][$i]['pd_code']) . '", ' . round(floatval($order['prices'][$i]['price']), 2) . ', ' . intval($order['prices'][$i]['quantity']) . ') ';
                                $this->db->query('update prices set quantity = quantity-' . intval($order['prices'][$i]['quantity']) . ' where id_shop=' . $shop['id'] . ' and pd_code="' . $this->db->escape($order['prices'][$i]['pd_code']) . '" ');
                            }
                            $this->db->query($sql);
                            $this->db->query('delete from cart where cart_code="' . $this->db->escape($_COOKIE['cart_code']) . '"');
                            $res = $this->db->query('select id_seller from shop where id=' . $shop['id']);
                            $id_seller = $res->row['id_seller'];
                            $res = $this->db->query('select balance from seller where id=' . $id_seller);
                            $all_balance = bcsub(floatval($res->row['balance']), ($order['delTypeData']['type']!='shop') ? config::$order_cost_for_seller : config::$shop_order_cost_for_seller, 2);
                            $this->db->query('update  seller set balance=' . $all_balance . ' where id=' . $id_seller);
                            $this->db->query('insert into balance_history set id_seller=' . $id_seller . ', summ='.(($order['delTypeData']['type']!='shop') ? config::$order_cost_for_seller : config::$shop_order_cost_for_seller).', type="-", all_balance=' . $all_balance . ', time=' . time() . ', comment="Комиссия #' . $new_order_id . '"');
                            $count_not_viewed = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `orders` WHERE  `viewed`=0 AND `delType`<>"shop" AND `id_shop`='.$shop['id']);
                            if (($count_not_viewed->row['count']>=config::$shop_viewed_limit) || ($all_balance<=config::$seller_balance_minus_limit))
                                $this->db->query('UPDATE `shop` SET `active`=0 WHERE `id`='.$shop['id']);
                            $_SESSION['confirm_seller_phone'] = $shop['phone'];
                            if ($order['delTypeData']['type']!='shop')
                                $this->sms->send_sms($shop['phone'], 'У вас новый заказ!');
                            setcookie("map_label", "", 1);
                            setcookie("city", "", 1);
                            setcookie("subgrs", "", 1);
                            setcookie("cats", "", 1);
                            setcookie("brands", "", 1);
                            return 'ORDERED';
                        }
                    }
                }

            }
        }

        return 'ORDER_WASNT_GOTTEN';
    }
}