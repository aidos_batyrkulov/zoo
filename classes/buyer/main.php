<?php
class main extends frame {
    private $brands;
    private $cats;

	public function get_content() {
	    if (isset($_POST['get_brands'])) $this->get_brands();

		$this->metaTitle='ZOOSKOP.com - сервис поиска и заказа зоотоваров';
		$this->metaDescription='&#9989;Добавьте зоотовары в корзину, &#9989;Сравните предложения зоомагазинов, &#9989;Закажите в один клик без регистрации! &#9989;Все зоомагазины на одном сайте';
		
		$this->getData();
		echo $this->showData();
	}

	private function getData() {
	    $this->cleanCartIfWereNotChecked();

        $res=$this->db->query('select * from category where id_super=0');
        if ($res->num_rows>0) {
            $supers = $res->rows;
            for ($i=0; $i<count($supers); $i++) {
                $res2=$this->db->query('select * from category where id_super='.$supers[$i]['id']);
                if ($res2->num_rows>0) {
                    $supers[$i]['sub_cats']=$res2->rows;
                    // checking the categories
                    if (isset($_COOKIE['cats']) && $cats = json_decode($_COOKIE['cats'])) {
                        for ($i2=0; $i2<count($supers[$i]['sub_cats']); $i2++) {
                            for($i3=0; $i3<count($cats); $i3++) {
                                if ($supers[$i]['sub_cats'][$i2]['id']==$cats[$i3]) {
                                    $supers[$i]['sub_cats'][$i2]['checked']=true;
                                }
                            }
                        }
                    }
                }
            }
        }
        $this->cats = $supers;
        $res=$this->db->query('select * from brand ORDER BY `level` DESC');
        $brands = array();
        if ($res->num_rows>0) {
            $brands = $this->clear_brands($res->rows);
            $result = $this->checking_brands($brands);
            $brands = $result['brands'];
        }
        $this->brands = $brands;
    }

    private function cleanCartIfWereNotChecked () {
	    if (!isset($_COOKIE['cats']) && !isset($_COOKIE['brands']) && isset($_COOKIE['cart_code'])) {
	        $this->db->query('DELETE FROM `cart` WHERE `cart_code`="'.intval($_COOKIE['cart_code']).'"');
        }
    }


	private function checking_brands($brands) {
        $checked_ids = [];
        if (isset($_COOKIE['brands']) && $c_brs = json_decode($_COOKIE['brands'])) {
            for ($i=0; $i<count($brands); $i++) {
                for ($i2 = 0; $i2 < count($c_brs); $i2++) {
                    if ($brands[$i]['id'] == $c_brs[$i2]) {
                        $brands[$i]['checked'] = true;
                        $checked_ids[]= $brands[$i]['id'];
                    }
                }
            }
        }
        return array('brands'=>$brands, 'checked_ids'=>$checked_ids);
    }

	private  function clear_brands($brands) {
        if (is_array($brands) && count($brands)>0 && isset($_COOKIE['cats']) && $cats = json_decode($_COOKIE['cats'])) {
            // removing some brands
            $new_arr = [];
            foreach ($brands as $brand) {
                $br_cats = explode(',', $brand['sub_cats']);
                foreach ($br_cats as $br_cat) {
                    foreach ($cats as $cat) {
                        if ($br_cat == $cat) {
                            $new_arr[] = $brand;
                            break 2;
                        }
                    }
                }
            }
            return $new_arr;
        } else {
            return $brands;
        }
    }


	private  function brand_html($brands) {
        ob_start();
        ?>
        <?php for($i=0; $i<count($brands); $i++) { ?>
            <label class="checkBrand">
                <input class="chBrand" type="checkbox"  data-id="<?=$brands[$i]['id']?>" <?=(isset($brands[$i]['checked']) ? ' checked="checked" ' : '')?>  />
                <span style="background-image:url(brand/<?=$brands[$i]['id']?>.png)"></span>
            </label>
        <?php  } ?>
        <?php
        $html = ob_get_clean();
        return $html;
    }


    private function showData(){
        ob_start();
        ?>
        <?php $supers  = $this->cats; ?>
        <div class="leftCategory">
            <?php for ($i=0; $i<count($supers); $i++) { ?>
                <h1><?=$supers[$i]['name']?>:</h1>
                <?php for ($i2=0; $i2<count($supers[$i]['sub_cats']); $i2++) { ?>
                    <label class="checkCategory">
                        <input class="chCategory" type="checkbox" data-id="<?=$supers[$i]['sub_cats'][$i2]['id']?>" <?=(isset($supers[$i]['sub_cats'][$i2]['checked']) ? 'checked' : '')?>  />
                        <span><?=$supers[$i]['sub_cats'][$i2]['name']?></span>
                    </label>
                <?php } ?>
            <?php } ?>
        </div>

        <div class="rightBrand">
            <h1></h1>
            <span id="brands">
                <?=$this->brand_html($this->brands)?>
            </span>
        </div>

        <script>
            // click category
            $(document).ready(function () {

                $("body").on("change", ".chCategory", function(event){
                        if(this.checked) {
                            cats = $.cookie("cats");
                            if (cats != undefined) {
                                cats = JSON.parse(cats);
                                cats.push($(this).attr("data-id"));
                            } else {
                                cats = [];
                                cats.push($(this).attr("data-id"));
                            }
                            cats = JSON.stringify(cats);
                            $.cookie("cats", cats, { expires: 90, path: "/" });
                        } else {
                            var id = $(this).attr("data-id");

                            cats = $.cookie("cats");
                            if (cats != undefined) {
                                // delete subcat from cookie cats
                                cats = JSON.parse(cats);
                                var new_arr = [];
                                for (i = 0; i < cats.length; i++) {
                                    if (cats[i] != id) {
                                        new_arr.push(cats[i]);
                                    }
                                }

                                if (new_arr.length > 0) {
                                    new_arr = JSON.stringify(new_arr);
                                    $.cookie("cats", new_arr, {expires: 90, path: "/"});
                                } else {
                                    $.removeCookie("cats", {path: "/"});
                                    if ($.cookie("brands")==undefined)
                                        document.location.href = '/';
                                }
                            }
                            subgrs = $.cookie("subgrs");
                            if (subgrs != undefined) {
                                // delete the same subcat (with subgrs) from cookie subgrs
                                subgrs = JSON.parse(subgrs);
                                var new_arr = [];
                                for (i=0; i<subgrs.length; i++) {
                                    if (subgrs[i]['id_subcat']!=id) {new_arr.push(subgrs[i]);}
                                }

                                if (new_arr.length>0) {
                                    new_arr = JSON.stringify(new_arr);
                                    $.cookie("subgrs", new_arr, { expires: 90, path: "/" });
                                } else {
                                    $.removeCookie("subgrs", { path: "/" });
                                }

                            }
                        }
                        $.ajax( {
                            url: "/",
                            cache: false,
                            dataType: "text",
                            type: "post",
                            data: "get_brands=on",
                            global: false,
                            success: function(data, textStatus, XHR) {
                                if (data!="EMPTY_CONTENT") {
                                    $("#brands").html(data);
                                }
                                else {
                                    $("#brands").html("Пусто");
                                }
                                recountCat();
                            },
                            beforeSend: function(XMLHttpRequest) {
                                var html = "";
                                html += "<div class= \"cssload-thecube\">";
                                html += "<div class=\"cssload-cube cssload-c1\"></div>";
                                html += "<div class=\"cssload-cube cssload-c2\"></div>";
                                html += "<div class=\"cssload-cube cssload-c4\"></div>";
                                html += "<div class=\"cssload-cube cssload-c3\"></div>";
                                html += "</div>";
                                $("#brands").html(html);

                            },
                            error: function( XHR, textStatus, errorThrown) {
                                $("#brands").html("Нет подключения к интернету");
                                recountCat();
                            }
                        });
                });

                // click brand
                $("body").on("change", ".chBrand", function(event){
                    if(this.checked) {
                        brands = $.cookie("brands");
                        if (brands != undefined) {
                            brands = JSON.parse(brands);
                            brands.push($(this).attr("data-id"));
                        } else {
                            brands = [];
                            brands.push($(this).attr("data-id"));
                        }
                        brands = JSON.stringify(brands);
                        $.cookie("brands", brands, { expires: 90, path: "/" });
                    } else {
                        brands = $.cookie("brands");
                        if (brands != undefined) {
                            brands = JSON.parse(brands);
                            var new_arr = [];
                            var id = $(this).attr("data-id");
                            for (i=0; i<brands.length; i++) {
                                if (brands[i]!=id) {new_arr.push(brands[i]);}
                            }

                            if (new_arr.length>0) {
                                new_arr = JSON.stringify(new_arr);
                                $.cookie("brands", new_arr, { expires: 90, path: "/" });
                            } else {
                                $.removeCookie("brands", { path: "/" });
                                if ($.cookie("cats")==undefined)
                                    document.location.href = '/';
                            }
                        }
                    }
                    recountCat();
                });

            });

            recountCat();
        </script>
        <?php
        $html = ob_get_clean();
        return $html;

    }

    private function get_brands()  {
        $res=$this->db->query('select * from brand  ORDER BY `level` DESC');
        $brands = $this->clear_brands($res->rows);

        if (count($brands)>0) {
            $result = $this->checking_brands($brands);
            $brands = $result['brands'];
            if (count($result['checked_ids'])>0) {
                setcookie('brands', json_encode($result['checked_ids']), time()+99999999,'/');
            } else {
                setcookie('brands', null, -1,'/');
            }
            echo $this->brand_html($brands);
        } else {
            echo  'EMPTY_CONTENT';
        }
        exit;
    }
}
