<?php
class catalog extends frame {
    private $pds;
    private $filter;

	public function get_content() {
        if (isset($_POST['id_pd']) && isset($_POST['id_pd_subgr'])) {exit($this->buy());}

		$this->metaTitle='Зоотовары | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
		$this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';


		if ((!(isset($_COOKIE['cats']))) && (!(isset($_COOKIE['brands'])))) {
		    header('location: /');
		    exit;
        }

		$this->getData();
		echo $this->showData();
	}


	private function getData() {
        $this->pds =$this->get_pds();
        $this->get_filter_data();
    }

	private function get_pds () {
        $sql ='select * from product where';
        $sql_where='';

	    if (isset($_COOKIE['brands']) && $brands = json_decode($_COOKIE['brands'])) {
            $sql_brands ='';
	        for ($i=0; $i<count($brands); $i++) {
	            $sql_brands.= (($i!=0) ? ',' : '').intval($brands[$i]);
            }
            $sql_where=' (brand in ('.$sql_brands.')) ';
        }

        if (isset($_COOKIE['subgrs']) && ($subgrs = json_decode($_COOKIE['subgrs'], true)) && (is_array($subgrs)) && (is_array($subgrs[0]['id_subgrs']))) {
	        /* $subgrs[]['id_subcat']
	         *          ['id_subgrs'][]
	         *
	         * a rule for every subcat's groups and subgroups:
	         *      the subgroups of one group are written with OR
	         *      the super groups are written with AND
	         *
	         *
	         * SELECT * FROM `product` WHERE (brand.....)
	         * if one chose only brands
	         *
	         * SELECT * FROM `product` WHERE (brand.....) and (two select or two select .... )
	         * if one chose brands, subcats and subgrs from every subcats
	         *
	         * SELECT * FROM `product` WHERE (brand.....) and ( (subcat....) or (two select or two select .... ))
	         * if one chose brands, subcats and subgrs from some subcats
	         *
	         * SELECT * FROM `product` WHERE (brand.....) and (subcat..... )
	         * if one chose brands and subcats
	         */

	        /*
	        this `for` will add that element to the array
	        $subgrs[]['id_subcat']
	                 ['grouped_grs'][]['id_super']
	                                  ['children'][]
	        */
            for ($i=0; $i<count($subgrs); $i++) {
                $grouped_grs = [];
                for ($i2=0; $i2<count($subgrs[$i]['id_subgrs']); $i2++) {
                    $id = intval($subgrs[$i]['id_subgrs'][$i2]);
                    $res = $this->db->query('select id_super from `group` where `id_super`<>0 and `id`='.$id);
                    if ($res->num_rows>0) {
                        $added = false;
                        for ($i3=0; $i3<count($grouped_grs); $i3++) {
                            if ($grouped_grs[$i3]['id_super']==$res->row['id_super']) {
                                $grouped_grs[$i3]['children'][]=$id;
                                $added = true;
                                break;
                            }
                        }
                        if ($added==false) {
                            $new_el =[];
                            $new_el['id_super'] = $res->row['id_super'];
                            $new_el['children'][]=$id;
                            $grouped_grs[]= $new_el;
                        }
                    }
                }
                if (count($grouped_grs)>0) {
                    $subgrs[$i]['grouped_grs'] = $grouped_grs;
                }
            }

            /*
             * now we merge sub groups which are in the select tags.
             * For example, sub groups of super groups size and weight (Their id is 1 and 2).
             * And we just set 1 as id_super in the array bellow
             */

            for ($i=0; $i<count($subgrs); $i++) {
                $one_and_two = [];
                $other = [];
                for ($i2=0; $i2<count($subgrs[$i]['grouped_grs']); $i2++) {
                    if($subgrs[$i]['grouped_grs'][$i2]['id_super']==1 || $subgrs[$i]['grouped_grs'][$i2]['id_super']==2) {
                        $one_and_two = array_merge($one_and_two, $subgrs[$i]['grouped_grs'][$i2]['children'] );
                    } else {
                        $other[] =  $subgrs[$i]['grouped_grs'][$i2];
                    }
                }
                if (count($one_and_two)>0) {
                    $new_arr = [];
                    $new_arr['id_super'] = 1; // here we just set 1
                    $new_arr['children'] = $one_and_two;
                    $other[]= $new_arr;
                }
                $subgrs[$i]['grouped_grs'] = $other;
            }
            /*
             * (
             * .........
             * OR
             * ........
             * OR
             * (
	         * (id IN (SELECT id_pd FROM pd_subcats WHERE id_subcat=9999))
	         *  AND
	         *  (
	         *    (     id IN (SELECT id_pd FROM pd_subgrs WHERE  id_subgr IN (2,7,5) )       )
	         *    and
	         *    (     id IN (SELECT id_pd FROM pd_subgrs WHERE  id_subgr IN (1,6,4) )       )
	         *    ...
	         *    ...
	         *  )
	         *
	         * )
	         * O R
             * ........
             * O R
             * ........
             * )
             */
	        if (isset($subgrs[0]['grouped_grs'])) {
                $sql_grs = '';
                for ($i=0; $i<count($subgrs); $i++) {
                    $sql_grs.=(($i!=0) ? ' OR ' : '').'
                        (
	                     (id IN (SELECT id_pd FROM pd_subcats WHERE id_subcat='.intval($subgrs[$i]['id_subcat']).'))
	                     AND
	                     (
                    ';
                    $and='';
                    for ($i2=0; $i2<count($subgrs[$i]['grouped_grs']); $i2++) {
                        $list='';
                        for ($i3=0; $i3<count($subgrs[$i]['grouped_grs'][$i2]['children']); $i3++) {
                            $list.=(($i3!=0) ? ',' : '').$subgrs[$i]['grouped_grs'][$i2]['children'][$i3];
                        }
                        $and.=(($i2!=0) ? ' and ' : '').'(     id IN (SELECT id_pd FROM pd_subgrs WHERE id_subgr IN ('.$list.') )       )';
                    }

                    $sql_grs.=$and. ') )';
                }
            }
        }

        if (isset($_COOKIE['cats']) && $cats = json_decode($_COOKIE['cats'])) {
            $sql_cats ='';
            for ($i=0; $i<count($cats); $i++) {
                $add = true;
                if ((isset($subgrs))&&(is_array($subgrs)) && (is_array($subgrs[0]['id_subgrs']))) {
                    for ($i2=0; $i2<count($subgrs); $i2++) {
                        if (intval($cats[$i])==intval($subgrs[$i2]['id_subcat'])) {
                            $add = false;
                            break;
                        }
                    }
                }
                if ($add==true) {
                    $sql_cats.= (($sql_cats!='') ? ',' : '').intval($cats[$i]);
                }

            }
            if ($sql_cats!='') {
                $sql_cats= ' id in (select id_pd from pd_subcats where id_subcat in('.$sql_cats.'))';
            }
        }


        if (@$sql_grs!='' && @$sql_cats!='') {
            $sql_where.=((@$sql_brands!='') ? ' and ' : '').' ( ( '.$sql_cats.' ) or ( '.$sql_grs.' ))';
        } else if (@$sql_grs!='' || @$sql_cats!='') {
            $sql_where.=((@$sql_brands!='') ? ' and ' : '').' ('.@$sql_grs.@$sql_cats.') ';
        }


        $sql .= $sql_where.' ORDER BY `level` ASC '.$this->getLimitByPage();
        $res=$this->db->query($sql);
        if ($res->num_rows>0) {
            $pds = $res->rows;
            for ($i=0; $i<count($pds); $i++) {
                $res=$this->db->query('select name from brand where id='.$pds[$i]['brand']);
                $pds[$i]['brand'] = $res->row['name'];

                $res=$this->db->query('select id_subgr from pd_subgrs where id_pd='.$pds[$i]['id'].' and id_subgr NOT in (select id from `group` where id_super='.$pds[$i]['id_group'].' )');
                $pds[$i]['id_subgrs'] = $res->rows;

                $res=$this->db->query('select id,name from `group` where id in (  select id_subgr from pd_subgrs where id_pd='.$pds[$i]['id'].' and id_subgr  in (select id from `group` where id_super='.$pds[$i]['id_group'].'))');
                $pds[$i]['selects'] = $res->rows;

                $res=$this->db->query('select name from `group` where id='.$pds[$i]['id_group']);
                $pds[$i]['group_name'] = $res->row['name'];
            }
            return $pds;
        } else {
            return false;
        }
    }

    private function showData() {
        ob_start();
        ?>
        <?=$this->run($this->pds)?>
        <?php if ($this->page>1) exit; ?>
        <?=$this->get_filter()?>

        <script>
            $(document).ready(function () {
                $(".check_subgr").change(function() {
                    id_subcat =$(this).attr('data-subcat-id');
                    id_subgr = $(this).attr('data-id');
                    if (this.checked) {
                        subgrs = $.cookie("subgrs");
                        if (subgrs != undefined) {
                            subgrs= JSON.parse(subgrs);
                            added= false;
                            for (i=0; i<subgrs.length; i++) {
                                if (subgrs[i]['id_subcat']==id_subcat) {
                                    subgrs[i]['id_subgrs'].push(id_subgr);
                                    added = true;
                                    break;
                                }
                            }
                            if (added==false) {
                                var arr = {};
                                arr['id_subcat'] = id_subcat;
                                arr['id_subgrs'] = [];
                                arr['id_subgrs'].push(id_subgr);
                                subgrs.push(arr);
                            }
                        } else {
                            subgrs = [];
                            arr = {};
                            arr['id_subcat'] = id_subcat;
                            arr['id_subgrs'] = [];
                            arr['id_subgrs'].push(id_subgr);
                            subgrs.push(arr);
                        }
                        subgrs = JSON.stringify(subgrs);
                        $.cookie("subgrs", subgrs, { expires: 90, path: "/" });
                    } else {
                        subgrs = $.cookie("subgrs");
                        if (subgrs != undefined) {
                            subgrs = JSON.parse(subgrs);
                            var new_arr = [];
                            for (i = 0; i < subgrs.length; i++) {
                                add_to_new = true;
                                if (subgrs[i]['id_subcat'] == id_subcat) {
                                    var new_index = [];
                                    for (i2 = 0; i2 < subgrs[i]['id_subgrs'].length; i2++) {
                                        if (subgrs[i]['id_subgrs'][i2] != id_subgr) {
                                            new_index.push(subgrs[i]['id_subgrs'][i2]);
                                        }
                                    }
                                    if (new_index.length>0) {
                                        subgrs[i]['id_subgrs'] = new_index;
                                    } else {
                                        add_to_new = false;
                                    }
                                }
                                if (add_to_new== true) {
                                    new_arr.push(subgrs[i]);
                                }
                            }

                            if (new_arr.length > 0) {
                                new_arr = JSON.stringify(new_arr);
                                $.cookie("subgrs", new_arr, {expires: 90, path: "/"});
                            } else {
                                $.removeCookie("subgrs", {path: "/"});
                            }
                        }
                    }
                });

                // when user clickes close button of the filter, the page will be updated with ajax
                $('body').on("click", ".filter .close", function(event){
                    document.location.href='/?option=catalog';
                });

                // we show the buy button when version was selected
                $('body').on("change", "#id-pd-subgr", function(event){
                    if ($(this).val()!=0) $(this).parent().children('#addtocart').css('display', 'block');
                    else  $(this).parent().children('#addtocart').css('display', 'none');
                });
                // firefox saves selected status even after updating page. This will turn it off
                $('select').filter('#id-pd-subgr').each(function () {$(this).prop('selectedIndex', 0);});

                // button "add to cart"
                $('body').on("click", "#addtocart", function(event){
                    var th = $(this);
                    var id_pd = $(this).attr('data-id-pd');
                    var id_pd_subgr = $(this).parent().children('#id-pd-subgr').val();

                    if (id_pd_subgr!=0) {
                        $.ajax( {
                            url: "/?option=catalog",
                            data: "id_pd="+id_pd+"&id_pd_subgr="+id_pd_subgr,
                            success: function(data, textStatus, XHR) {
                                if (data.indexOf('SIGNAL_OK')+1) {
                                    $('#countCatalog').css('display', 'block');
                                    $('#countCatalog').html(+$('#countCatalog').html()+1);
                                    $(".navShops").css('display', 'block');
                                }
                                else {
                                    alert('Не удалось добавить в корзину');
                                }
                            },
                            beforeSend: function(XMLHttpRequest) {
                                th.val('...');
                            },
                            complete: function( XHR, textStatus ) {
                                th.val('В корзину');
                            }
                        });
                    }  else  alert('Сначала выберите вариант товара');
                });
            });

            $(function() {
                $('body').on("click", ".goods div img", function(event){
                    $(this).toggleClass("min");
                });
            });
        </script>

        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function run($pds) {
        ob_start();
        ?>

        <?php if ($pds==false) { ?>
            <?=(($this->page>1) ? 'PAGE_END' : 'Пусто')?>
        <?php } else { ?>
            <?php for($i=0; $i<count($pds); $i++) { ?>
                <div class="cell">
                    <div class="goods">
                        <div>
                            <span><?=$pds[$i]['brand']?></span>
                            <span><?=$pds[$i]['name']?></span>
                        </div>
                        <div>
			        		<img src="catalog/<?=$pds[$i]['photo']?>.png">
			        		<span><?=$pds[$i]['des']?></span>
			        	</div>

                        <?php foreach ($pds[$i]['id_subgrs'] as $id_subgr) { ?>
                            <?php //<img class="prGoods" src="properties/'.$id_subgr['id_subgr'].'.png">?>
                        <?php } ?>

                        <select class=".just_for_firefox" id="id-pd-subgr">
			        		<option value="0" selected>Выбрать <?=$pds[$i]['group_name']?></option>
                            <?php for ($i2=0; $i2<count($pds[$i]['selects']); $i2++) { ?>
                                <option value="<?=$pds[$i]['selects'][$i2]['id']?>"><?=$pds[$i]['selects'][$i2]['name']?></option>
                            <?php } ?>
			        	</select>
			        	<input  id="addtocart" style="display: none;"  data-id-pd="<?=$pds[$i]['id']?>"  type="button" value="В корзину"/>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>

        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function get_filter_data() {
        if (isset($_COOKIE['cats']) && $cats = json_decode($_COOKIE['cats'])) {
            $this->filter = [];
            /*
             * $this->filter[]['name']
             *                ['subgrs'][]['id']
             *                            ['name']
             *                            ['checked']   this boolean element will be  if it was checked
             */

            // the subgroups which are in the select of a product must not be in the filter
            $res=$this->db->query('select `id`  from `group` where id_super=0 and `super_for_select`=1');
            $supers_with_for_select = '';
            for ($b=0; $b<count($res->rows); $b++) $supers_with_for_select.=(($b==0) ? '' : ',').$res->rows[$b]['id'];
            $res=$this->db->query('select `id`  from `group` where id_super IN ('.$supers_with_for_select.') ');
            $idSubgrsInSelects = '';
            for ($b=0; $b<count($res->rows); $b++) $idSubgrsInSelects.=(($b==0) ? '' : ',').$res->rows[$b]['id'];

            for ($i=0; $i<count($cats); $i++) {
                $res=$this->db->query('select `name`, `id_super`  from category where id_super<>0 and id='.intval($cats[$i]));
                if ($res->num_rows>0) {
                    $res2=$this->db->query('select name from category where id='.$res->row['id_super']);
                    $name = $res->row['name'].' '.$res2->row['name'];
                    $res=$this->db->query('select id, name from `group` where id_super<>0 and id in (select id_subgr from `subcat_subgrs` where `id_subcat`='.intval($cats[$i]).') and `id` NOT IN ('.$idSubgrsInSelects.')');
                    if ($res->num_rows>0) {
                        $new_arr = [];
                        $new_arr['name']= $name;
                        $new_arr['id'] = intval($cats[$i]);
                        $new_arr['subgrs'] = $res->rows;
                        $this->filter[] = $new_arr;
                    }
                }
            }
            if (count($this->filter)>0) {
                for ($i=0; $i<count($this->filter); $i++) {
                    /*
                     * checking subgrs
                     */
                    if (isset($_COOKIE['subgrs']) && ($subgrs = json_decode($_COOKIE['subgrs'], true)) && (is_array($subgrs)) && (is_array($subgrs[0]['id_subgrs']))) {
                        for ($a=0; $a<count($subgrs); $a++ ) {
                            if ($subgrs[$a]['id_subcat']==$this->filter[$i]['id']) {
                                for ($a2=0; $a2<count($subgrs[$a]['id_subgrs']); $a2++) {
                                    for ($a3=0; $a3<count($this->filter[$i]['subgrs']); $a3++) {
                                        if ($subgrs[$a]['id_subgrs'][$a2]==$this->filter[$i]['subgrs'][$a3]['id']) {
                                            $this->filter[$i]['subgrs'][$a3]['checked']= true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
	}


    private  function get_filter() {
        ob_start();
        ?>
        <!-- <<<<< MODAL FILTER <<<<< -->
        <div class="modal filter" id="modalFilter" style="display:none;">
            <div class="close" onclick="toggle(modalFilter)"></div>
            <?php for ($i=0; $i<count($this->filter); $i++) { ?>
                <div class="item">
                    <span><?=$this->filter[$i]['name']?>: </span>
                    <?php for ($i2=0; $i2<count($this->filter[$i]['subgrs']); $i2++) { ?>
                        <label class="checkCategory">
                            <input class="check_subgr" type="checkbox"  data-subcat-id="<?=$this->filter[$i]['id']?>" data-id="<?=$this->filter[$i]['subgrs'][$i2]['id']?>" <?=(isset($this->filter[$i]['subgrs'][$i2]['checked']) ? ' checked ' : '')?> />
                            <span><?=$this->filter[$i]['subgrs'][$i2]['name']?></span>
                        </label>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <!-- <<<<< END MODAL FILTER <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private  function buy() {
	    $id_pd = intval($_POST['id_pd']);
        $id_pd_subgr = intval($_POST['id_pd_subgr']);

        $res = $this->db->query('select pd_code from pd_subgrs where id_pd='.$id_pd.' and  id_subgr='.$id_pd_subgr.' and pd_code<>"" limit 1');

        if ($res->num_rows>0) {
            $pd_code = $res->row['pd_code'];
            if (isset($_COOKIE['cart_code']) && mb_strlen($_COOKIE['cart_code'])==10) {
                $cart_code = $_COOKIE['cart_code'];
                $this->db->query('update cart set quantity=quantity+1 where   cart_code="'.$this->db->escape($cart_code).'" and pd_code="'.$pd_code.'"');
                if ($this->db->affectedRows()<1) {
                    $this->db->query('insert into cart set cart_code="'.$this->db->escape($cart_code).'",  pd_code="'.$pd_code.'", time='.time());
                }
            } else {
                $cart_code = rand(1111111111,9999999999);
                setcookie('cart_code', $cart_code, time()+9999999999, '/');
                $this->db->query('insert into cart set cart_code="'.$this->db->escape($cart_code).'",  pd_code="'.$pd_code.'", time='.time());
            }

            return 'SIGNAL_OK';
        }
        return 'SIGNAL_BAD';
    }

}