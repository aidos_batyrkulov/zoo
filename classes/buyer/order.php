<?php
class order extends frame {
	public function get_content() {
		$this->metaTitle='Заказ | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
		$this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';
		
		$this->pageScript='<script src="//zooskop.com/js/jquery.maskedinput.min.js" type="text/javascript"></script>';
		
		$this->orderActive='navConfirm navActive';
		echo'
		<!-- content - BEGIN -->
		<div class="content">
			<h1>Подтвердите заказ #33421</h1>
			<p><center>Вам отправленно SMS с КОДОМ подтверждения, введите код и нажмите "Подтвердить заказ"</center></p>
			<form method="post" action="#">
				<input name="sms_code" required="required" type="text" placeholder="Введите SMS код"/>
				<input type="button" onclick="toggle(hiddenStep)" value="Подтвердить заказ"/>
			</form>
			<span style="display:none;" id="hiddenStep">
				<h2>Отправить SMS код еще раз:</h2>
				<form method="post" action="#">
					<input name="phone" required="required" id="phone" type="text" placeholder="Телефон"/>
					<input type="button" onclick="location.href=\'/?option=confirm\'" value="Повторить"/>
				</form>
			</span>
		</div>
		<script>
			$("#phone").mask("+38(099)999-99-99", {completed: function(){}});
		</script>
		<!-- content - END -->
		';
	}
}