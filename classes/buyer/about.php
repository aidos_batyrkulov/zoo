<?php
class about extends frame {
    public function get_content() {
        $this->metaTitle='О проекте | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='&#9989;Интернет портал зоомагазинов Украины, &#9989;широкий ассортимент, &#9989;сравнение Вашей корзины покупок по всем зоомагазинам Украины';

        $this->linkCategory='onclick="location.href=\'/?option=main\'" ';
        $this->linkCatalog='onclick="location.href=\'/?option=catalog\'" ';
        $this->linkShop='onclick="location.href=\'/?option=cart\'" ';

        echo'
		<div class="content">
			<h1>О проекте "ZooSkop.com"</h1>
			<h2>Кто мы</h2>
			<p>ZooSkop.com – онлайн сервис поиска и заказа товаров в Зоомагазинах Украины, построенный на единой товарной базе, что исключает ошибки, неточности или дублирования зоотоваров.</p>
			<h2>Что мы делаем?</h2>
			<p>Данная площадка объединяет Покупателей зоотоваров, которым необходимы один или несколько товаров и Зоомагазины Украины, у которых есть в наличии все данные товары.</p>
			<p>С помощью сервиса ZooSkop.com каждый Покупатель найдет оптимальное, близкое по расположению или с подходящими условиями доставки предложение от сотен Зоомагазинов Украины.</p>
			<p>Зоомагазины после быстрой регистрации и добавления товаров в наличии – сразу попадают в выдачу предложений, и уже могут получать заказы, не отвлекаясь на рекламу, раскрутку и продвижение товаров.</p>
			<h2>Зачем мы это делаем?</h2>
			<p>Наша цель – дать возможность каждому Зоомагазину быть представленным в Интернет, упростить получение и обработку заказов до минимума, сократить расходы на объявления, рекламу и продвижение товаров до «нуля», что позволит предлагать Покупателям лучшие товары дешевле, тратить меньше времени на ненужные действия и сконцентрироваться на главном – обслуживании Клиента.</p>
			<hr>
			<p>Если у Вас возникли вопросы - пишите office@zooskop.com</p>
			<p>Также мы открыты для Ваших идей и предложений</p>
		</div>
		';
    }
}