<?php
class admsettings extends frame {
    private $reg= true;
    private $buy= true;

    public function get_content() {
        require_once 'PHPExcel.php';

        if (isset($_POST['reg']) || isset($_POST['buy'])) $this->check_opt();
        if (isset($_FILES['cats']) || isset($_FILES['grs'])  || isset($_FILES['pds'])  || isset($_FILES['brs'])) $this->excel();
        if (isset($_GET['load_cats']) || isset($_GET['load_grs'])  || isset($_GET['load_pds'])  || isset($_GET['load_brs'])) $this->load_ex();

        $this->metaTitle = 'Админ настройки | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        $this->initData();
        $this->setData();
        echo $this->showData();

    }


    private  function getData() {
        $res = $this->db->query('SELECT * FROM `admin_settings` WHERE `type`="seller_reg_active" OR `type`="buyer_cart_active"');
        if ($res->num_rows>1) {
            foreach ($res->rows as $row) {
                if ($row['type']=='seller_reg_active') $this->reg = (($row['data']=='1') ? true : false);
                else if ($row['type']=='buyer_cart_active') $this->buy = (($row['data']=='1') ? true : false);
            }
        }
    }



    private function initData() {

    }

    private function setData() {

    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <div class="item catalogSettings">
            <div class="subgroups">
                <span>1</span>
                <input type="button" onclick="document.location.href='/?option=admsettings&load_cats=1'" value="Скачать"/>
                <form method="post" action="/?option=admsettings" enctype="multipart/form-data">
                    <input  type="submit" value="Загрузить"/>
                    <label><input name="cats" type="file">Выбрать</label>
                </form>
            </div>
            <div class="brands">
                <span>1</span>
                <input type="button" onclick="document.location.href='/?option=admsettings&load_brs=1'"  value="Скачать"/>
                <form method="post" action="/?option=admsettings" enctype="multipart/form-data">
                    <input type="submit" value="Загрузить"/>
                    <label><input name="brs" type="file">Выбрать</label>
                </form>
            </div>
            <div class="properties">
                <span>1</span>
                <input type="button" onclick="document.location.href='/?option=admsettings&load_grs=1'"  value="Скачать"/>
                <form method="post" action="/?option=admsettings" enctype="multipart/form-data">
                    <input type="submit" value="Загрузить"/>
                    <label><input name="grs" type="file">Выбрать</label>
                </form>
            </div>
            <div class="goods">
                <span>1</span>
                <input type="button" onclick="document.location.href='/?option=admsettings&load_pds=1'"  value="Скачать"/>
                <form method="post" action="/?option=admsettings" enctype="multipart/form-data">
                    <input type="submit" value="Загрузить"/>
                    <label><input name="pds" type="file">Выбрать</label>
                </form>
            </div>
        </div>
        <div class="item siteSettings">
            <div class="cartOff">
                <label class="checkbox"><input id="buy" type="checkbox" <?=(($this->buy==true) ? 'checked' : '')?> /><div class="checked"></div></label>
            </div>
            <div class="registerOff">
                <label class="checkbox"><input id="reg" type="checkbox"  <?=(($this->reg==true) ? 'checked' : '')?>  /><div class="checked"></div></label>
            </div>
        </div>

        <!-- <<<<< CONTENT <<<<< -->

        <script>
            $(document).ready(go());

            function go() {
                $('body').on("click", "#reg, #buy", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    var opt = 'reg';
                    if (th.attr('id')=='buy') opt = 'buy';
                    $.post('/?option=admsettings', opt+'='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                });
            }

        </script>

        <?php
        $html = ob_get_clean();
        return $html;
    }





    private function check_opt() {
        if (isset($_POST['reg']))  {$type='seller_reg_active'; $data = $_POST['reg'];}
        else  {$type = 'buyer_cart_active'; $data = $_POST['buy'];}
        $this->db->query('UPDATE `admin_settings` SET `data`="'.(($data==1) ? '1' : '0'). '" WHERE `type`="'.$type.'"');
        exit;
    }

    private function excel() {
        $file='';
        if (isset($_FILES['cats']))   $file = 'cats';
        else if (isset($_FILES['brs'])) $file = 'brs';
        else if (isset($_FILES['grs'])) $file = 'grs';
        else if (isset($_FILES['pds'])) $file = 'pds';

        if ($_FILES[$file]['error']==UPLOAD_ERR_OK && (
                (preg_match("/.xlsx\$/i", $_FILES[$file]['name'])) ||
                (preg_match("/.xlsm\$/i", $_FILES[$file]['name']))  ||
                (preg_match("/.xls\$/i", $_FILES[$file]['name'])) ) &&
            ($_FILES[$file]['size']<=10728640) ) {

            $inputFileType = PHPExcel_IOFactory::identify($_FILES[$file]['tmp_name']);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($_FILES[$file]['tmp_name']);
            $ar = $objPHPExcel->getActiveSheet()->toArray();


            $error = false; // if there is one incorrect line at least, we will not write all!
            $values = '';
            $table = '';
            $sql = '';
            $first = true; // In excel files the first line is the column name. We use this var to know that

            // working on execel file of the categories and groups
            if (isset($_FILES['cats']) || isset($_FILES['grs']))    {
                // in this cycle we check all lines and write them to the $values var
                foreach($ar as $str) {
                    if ($first) {$first = false;continue;}
                    if ( mb_strlen($str[1])<255 &&  mb_strlen($str[1])>0 && intval($str[2])>=0 && floatval($str[3])>0 ) {
                        if ($values!='') $values.=',';
                        $values.=' ( "'.$this->db->escape($str[1]).'", '.intval($str[2]).', '.floatval($str[3]);
                        if (isset($_FILES['grs'])) $values.=' ,'.intval($str[4]);
                        $values.=' ) ';
                    } else {
                        $error = true;
                        break;
                    }
                }
                if (isset($_FILES['cats'])) $table = '`category`';
                else $table ='`group`';
                $sql = 'INSERT INTO '.$table.' (`name`, `id_super`, `level` '.((isset($_FILES['grs']))  ? ',`super_for_select` ' : '').'  ) VALUES '.$values;

            } else if (isset($_FILES['brs'])) { // working on brands
                // in this cycle we check all lines and write them to the $values var
                foreach($ar as $str) {
                    if ($first) {$first = false;continue;}
                    if ( mb_strlen($str[1])<255 &&  mb_strlen($str[1])>0 && floatval($str[2])>0 ) {
                        if ($values!='') $values.=',';
                        $values.=' ( "'.$this->db->escape($str[1]).'", '.floatval($str[2]).', "" )';
                    } else {
                        $error = true;
                        break;
                    }
                }
                $table = 'brand';
                $sql = 'INSERT INTO '.$table.' (`name`, `level`, `sub_cats`) VALUES '.$values;

            } else if ($_FILES['pds']) { // working on pds. We have to upload brands` excel before pds
                // in this cycle we check all lines and write them to the $values var
                foreach($ar as $str) {
                    if ($first) {$first = false;continue;}
                    if ( intval($str[0])>0 && mb_strlen($str[1])<=255 &&  mb_strlen($str[1])>0 &&  mb_strlen($str[2])>0 && intval($str[3])>0 &&  mb_strlen($str[4])>0 && floatval($str[5])>0 ) {
                        if ($values!='') $values.=',';
                        $namesOfSelectGroups = explode(',', $str[8]);
                        $res= $this->db->query('SELECT `id_super` FROM `group` WHERE `name`="'.$this->db->escape($namesOfSelectGroups[0]).'" LIMIT 1');
                        if ($res->num_rows==0) {
                            $error = true;
                            break;
                        }
                        $values.=' ( "'.$this->db->escape($str[1]).'",  "'.$this->db->escape($str[2]).'", '.intval($res->row['id_super']).', '.intval($str[3]).', "'.$this->db->escape($str[4]).'", '.floatval($str[5]).' )';
                    } else {
                        $error = true;
                        break;
                    }
                }

                $table = 'product';
                $sql = 'INSERT INTO '.$table.' (`name`, `des`, `id_group`, `brand`, `photo`, `level`) VALUES '.$values;

                if ($error==false) {
                    $pd_subcats_vals =''; //  for pd_subcats
                    $pd_subgrs_vals = ''; // for pd_subgrs
                    $subcat_subgrs_vals = ''; // for subcat_subgrs
                    $subcats = []; // it is also for subcat_subgrs
                    $first = true; // In excel files the first line is the column name. We use this var to know that
                    foreach($ar as $str) {
                        if ($first) {$first = false;continue;}
                        $scts = explode(',',$str[6]);
                        $subgrs = explode(',', $str[7].((!empty($str[7]) && (!empty($str[8]))) ? ',' : '' ).$str[8]);
                        $in_name = '';
                        foreach ($subgrs as $subgr) {
                            $in_name.='"'.$subgr.'",';
                        }
                        $in_name = substr($in_name, 0, -1);
                        $rrr = $this->db->query('SELECT `id` FROM `group` WHERE `name` IN ('.$in_name.')');
                        $subgrs = [];
                        foreach ($rrr->rows as $row) {
                            $subgrs[] = $row['id'];
                        }
                        foreach ($scts as $sct) {
                            if ($pd_subcats_vals!='') $pd_subcats_vals.=',';
                            $pd_subcats_vals.='( '.intval($str[0]).', '.intval($sct).' )';

                            // START! now the code bellow is for updating the `subcat_subgrs` table
                            $add = true;
                            foreach ($subcats as &$subcat) {
                                if ($subcat['subcat'] == $sct) {
                                    foreach ($subgrs as $subgr2) {
                                        $add_subgr2 = true;
                                        foreach ($subcat['subgrs'] as $subgr) {
                                            if ($subgr2 == $subgr) {$add_subgr2 = false; break;}
                                        }
                                        if ($add_subgr2)
                                            $subcat['subgrs'][]= $subgr2;
                                    }

                                    $add=false;
                                    break;
                                }
                            }
                            unset($subcat);

                            if ($add) {
                                $new_arr = [];
                                $new_arr['subcat'] = $sct;
                                $new_arr['subgrs'] = $subgrs;
                                $subcats[] = $new_arr;
                            }
                            // END!
                        }

                        $sgrs = explode(',',$str[7]);
                        $in_name='';
                        foreach ($sgrs as $sgr) {
                            $in_name.='"'.$sgr.'",';
                        }
                        $in_name = substr($in_name, 0, -1);
                        $rrr = $this->db->query('SELECT `id` FROM  `group` WHERE `name` IN ('.$in_name.')');
                        $sgrs = [];
                        foreach ($rrr->rows as $row) {
                            $sgrs[] = $row['id'];
                        }

                        $select_sgrs = explode(',', $str[8]);
                        $in_name='';
                        foreach ($select_sgrs as $select_sgr) {
                            $in_name.='"'.$select_sgr.'",';
                        }
                        $in_name = substr($in_name, 0, -1);
                        $rrr = $this->db->query('SELECT `id` FROM  `group` WHERE `name` IN ('.$in_name.')');
                        $select_sgrs = [];
                        foreach ($rrr->rows as $row) {
                            $select_sgrs[] = $row['id'];
                        }

                        $pd_codes = explode(',', $str[9]);
                        for($b=0; $b<(count($sgrs)+count($select_sgrs)); $b++) {
                            if ($pd_subgrs_vals!='') $pd_subgrs_vals.=',';
                            $pd_subgrs_vals.='( "'.((isset($pd_codes[$b]))? intval($pd_codes[$b]) : '').'", '.intval($str[0]).', '.((isset($select_sgrs[$b])) ? intval($select_sgrs[$b]) : intval($sgrs[$b-count($select_sgrs)])).' )';
                        }

                        // now we update the sub_cats field of the brand table
                        $res = $this->db->query('SELECT `sub_cats` FROM `brand` WHERE `id`='.intval($str[3]).' LIMIT 1');
                        $pd_scts = explode(',',$str[6]);
                        if (mb_strlen($res->row['sub_cats'])>0) {
                            $brand_scts = explode(',',$res->row['sub_cats']);
                            $new_scts = array_unique(array_merge($pd_scts, $brand_scts));
                            $value_sub_cats = '';
                            foreach ($new_scts as $new_sct) {
                                if ($value_sub_cats!='') $value_sub_cats.=',';
                                $value_sub_cats.=intval($new_sct);
                            }
                        } else
                            $value_sub_cats = $str[6];
                        $this->db->query('UPDATE `brand` SET  `sub_cats`="'.$this->db->escape($value_sub_cats).'" WHERE `id`='.intval($str[3]));

                    }

                    if  ($pd_subcats_vals!='') {
                        //  we rewrite the table pd_subcats
                        $this->db->query('TRUNCATE TABLE `pd_subcats`');
                        $this->db->query('INSERT INTO `pd_subcats` (`id_pd`, `id_subcat`) VALUES '.$pd_subcats_vals);
                    }

                    if  ($pd_subgrs_vals!='') {
                        //  we rewrite the table pd_subgrs
                        $this->db->query('TRUNCATE TABLE `pd_subgrs`');
                        $this->db->query('INSERT INTO `pd_subgrs` (`pd_code`, `id_pd`, `id_subgr`) VALUES '.$pd_subgrs_vals);
                    }

                    foreach ($subcats as $subcat) {
                        foreach ($subcat['subgrs'] as $subgr) {
                            if ($subcat_subgrs_vals!='') $subcat_subgrs_vals.=',';
                            $subcat_subgrs_vals.='( '.intval($subcat['subcat']).', '.intval($subgr).' )';
                        }
                    }

                    if  ($subcat_subgrs_vals!='') {
                        //  we rewrite the table subcat_subgrs
                        $this->db->query('TRUNCATE TABLE `subcat_subgrs`');
                        $this->db->query('INSERT INTO `subcat_subgrs` (`id_subcat`, `id_subgr`) VALUES '.$subcat_subgrs_vals);
                    }
                }
            }

            // now we check if all lines are correct
            if ($error==false && $values!='') {
                // start writng
                $this->db->query('TRUNCATE TABLE '.$table);
                $this->db->query($sql);
                $_SESSION['msg_ok'] = 'Добавлено: '.(count($ar)-1);
            } else $_SESSION['msg_error']= 'Не удалось выполнить запрос. Некоторые столбцы содержить некорректные данные!';
        } else {
            $_SESSION['msg_error']= 'Некорректный файл или файл весит больше 10мб';
        }
    }

    private function load_ex() {
        $table ='';
        if (isset($_GET['load_cats']))   $table = '`category`';
        else if (isset($_GET['load_brs'])) $table = '`brand`';
        else if (isset($_GET['load_grs'])) $table = '`group`';
        else if (isset($_GET['load_pds'])) $table = '`product`';

        $res= $this->db->query('SELECT * FROM '.$table);

        if ($res->num_rows>0) {
            $phpexcel = new PHPExcel();

            $page = $phpexcel->setActiveSheetIndex(0);

            $page->getColumnDimension('A')->setAutoSize(true);
            $page->getColumnDimension('B')->setAutoSize(true);
            $page->getColumnDimension('C')->setAutoSize(true);
            $page->getColumnDimension('D')->setAutoSize(true);
            $page->getColumnDimension('E')->setAutoSize(true);
            $page->getColumnDimension('F')->setAutoSize(true);
            $page->getColumnDimension('G')->setAutoSize(true);
            $page->getColumnDimension('H')->setAutoSize(true);
            $page->getColumnDimension('I')->setAutoSize(true);
            $page->getColumnDimension('J')->setAutoSize(true);
            $page->getColumnDimension('K')->setAutoSize(true);

            if (isset($_GET['load_cats']) || isset($_GET['load_grs']))    {
                $page->setCellValue("A1", "ID");
                $page->setCellValue("B1", "name");
                $page->setCellValue("C1", "ID_super");
                $page->setCellValue("D1", "level");
                if (isset($_GET['load_grs']))
                    $page->setCellValue("E1", "super_for_select");
                for($i=0; $i<$res->num_rows; $i++) {
                    $page->setCellValue("A".($i+2), $res->rows[$i]['id']);
                    $page->setCellValue("B".($i+2), $res->rows[$i]['name']);
                    $page->setCellValue("C".($i+2), $res->rows[$i]['id_super']);
                    $page->setCellValue("D".($i+2), $res->rows[$i]['level']);
                    if (isset($_GET['load_grs']))
                        $page->setCellValue("E".($i+2), $res->rows[$i]['super_for_select']);
                }
                if (isset($_GET['load_grs']))  $page->setTitle("Group");
                if (isset($_GET['load_cats']))  $page->setTitle("Category");
            }
            else if (isset($_GET['load_brs'])){
                $page->setCellValue("A1", "ID");
                $page->setCellValue("B1", "name");
                $page->setCellValue("C1", "level");
                for($i=0; $i<$res->num_rows; $i++) {
                    $page->setCellValue("A".($i+2), $res->rows[$i]['id']);
                    $page->setCellValue("B".($i+2), $res->rows[$i]['name']);
                    $page->setCellValue("C".($i+2), $res->rows[$i]['level']);
                }

                $page->setTitle("Brand");
            }
            else if (isset($_GET['load_pds'])){
                $page->setCellValue("A1", "id");
                $page->setCellValue("B1", "name");
                $page->setCellValue("C1", "des");
                $page->setCellValue("D1", "brand");
                $page->setCellValue("E1", "photo");
                $page->setCellValue("F1", "level");
                $page->setCellValue("G1", "id_subcats");
                $page->setCellValue("H1", "groups");
                $page->setCellValue("I1", "select_groups");
                $page->setCellValue("J1", "pd_codes");

                for($i=0; $i<$res->num_rows; $i++) {
                    $page->setCellValue("A".($i+2), $res->rows[$i]['id']);
                    $page->setCellValue("B".($i+2), $res->rows[$i]['name']);
                    $page->setCellValue("C".($i+2), $res->rows[$i]['des']);
                    $page->setCellValue("D".($i+2), $res->rows[$i]['brand']);
                    $page->setCellValue("E".($i+2), $res->rows[$i]['photo']);
                    $page->setCellValue("F".($i+2), $res->rows[$i]['level']);

                    $res2 = $this->db->query('SELECT `id_subcat` FROM `pd_subcats` WHERE `id_pd`='.$res->rows[$i]['id']);
                    $res3= $this->db->query('SELECT `id_subgr`, `pd_code` FROM `pd_subgrs` WHERE  `id_pd`='.$res->rows[$i]['id']);

                    $id_subcats = '';
                    for($t=0; $t<$res2->num_rows; $t++) {
                        $id_subcats.=(($t!=0) ? ',' : '').$res2->rows[$t]['id_subcat'];
                    }

                    $id_groups = '';
                    $id_select_grs = '';
                    $pd_codes  ='';
                    for($t=0; $t<$res3->num_rows; $t++) {
                        $res4 = $this->db->query('SELECT `name` FROM `group` WHERE `id`='.$res3->rows[$t]['id_subgr']);
                        if (!empty($res3->rows[$t]['pd_code'])) {
                            $id_select_grs.=((!empty($id_select_grs)) ? ',' : '').$res4->row['name'];
                            $pd_codes.=((!empty($pd_codes)) ? ',' : '').$res3->rows[$t]['pd_code'];
                        } else {
                            $id_groups.=((!empty($id_groups)) ? ',' : '').$res4->row['name'];
                        }
                    }

                    $page->setCellValueExplicit("G".($i+2), $id_subcats,PHPExcel_Cell_DataType::TYPE_STRING);
                    $page->setCellValueExplicit("H".($i+2), $id_groups,PHPExcel_Cell_DataType::TYPE_STRING);
                    $page->setCellValueExplicit("I".($i+2), $id_select_grs,PHPExcel_Cell_DataType::TYPE_STRING);
                    $page->setCellValueExplicit("J".($i+2), $pd_codes,PHPExcel_Cell_DataType::TYPE_STRING);
                }

                $page->setTitle("Product");
            }

            $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
            $rd = abs(rand(111111111111,999999999999));
            $n = "files/admin/excel/".$rd.".xlsx";
            $objWriter->save($n);
            $res= $this->db->query('SELECT name from files where id_shop=0');
            if ($res->num_rows>0) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/'.$res->row['name']);
                $this->db->query('delete from files where id_shop=0');
            }
            $sql = 'insert into files set `name`="'.$n.'", `id_shop`=0, `time`='.time();
            $this->db->query($sql);
            header('location: /'.$n);
        }	 else {
            $_SESSION['msg_error']= 'Пусто!';
        }

    }


}