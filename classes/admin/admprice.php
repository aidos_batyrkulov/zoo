<?php
class admprice extends frame {
    private $seller;
    private $shop;
    private $pds;

    public function get_content() {require_once 'PHPExcel.php';
        if (isset($_POST['pd_code'])) $this->del_upd();
        if (isset($_FILES['ex'])) $this->excel();
        if (isset($_POST['load'])) $this->load_ex();

        $this->metaTitle = 'Админ Цены | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData() {
        $id = intval($_GET['id']);
        $res = $this->db->query('select * from `shop`  WHERE `id`='.$id);
        if ($res->num_rows==0) {header('location: /'); exit;}
        $this->shop= $res->row;
        $res = $this->db->query('select * from `seller`  WHERE `id`='.$this->shop['id_seller']);
        $this->seller= $res->row;
        $res = $this->db->query('select * from `prices` where `id_shop`='.$id.' ORDER BY `id` DESC '.$this->getLimitByPage());
        $this->pds = $res->rows;
        $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `orders` WHERE  `confirmed`=1 AND  `id_shop`='.$id);
        $this->shop['count_orders'] = $res->row['count'];
        $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `prices` WHERE   `id_shop`='.$id);
        $this->shop['count_prices'] = $res->row['count'];
        $day_n = date('N');
        $time = intval(date('Hi'));
        if ($this->shop['d'.$day_n.'_start']<$time && $this->shop['d'.$day_n.'_end']>$time ) $this->shop['open']= true;
        else $this->shop['open']= false;
        // get pd name,  option name, brand name, id
        foreach ($this->pds as &$pd) {
            $res = $this->db->query('select name from `group` where id=(select id_subgr from pd_subgrs where pd_code="'.$pd['pd_code'].'")');
            $pd['subgr_name'] = $res->row['name'];
            $res = $this->db->query('select id,name,photo from `product` where id=(select id_pd from pd_subgrs where pd_code="'.$pd['pd_code'].'")');
            $pd['name'] = $res->row['name'];
            $pd['id']= $res->row['id'];
            $pd['photo']= $res->row['photo'];
            $res = $this->db->query('select name from `brand` where id=(select brand from product where id=(select id_pd from pd_subgrs where pd_code="'.$pd['pd_code'].'"))');
            $pd['brand_name'] = $res->row['name'];
        }
        unset($pd);
    }


    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <?php $seller = $this->seller; $shop = $this->shop; ?>
        <?php if ($this->page==1) { ?>
        <div class="item user seller">
            <label class="checkbox"><input data-id-seller="<?=$seller['id']?>" id="the_button" type="checkbox" <?=(($seller['active']==1) ? 'checked' : '')?> /><div class="checked"></div></label>
            <div class="part1">
                <span><?=date('H', $seller['time'])?><sup><?=date('i', $seller['time'])?></sup><br><?=date('d.m.Y',$seller['time'])?></span>
                <span><?=$seller['balance']?></span>
            </div>
            <div class="part2">
                <span class="cursor" onclick="location.href='/?option=admseller&id=<?=$seller['id']?>'"><?=$seller['fio']?></span>
                <span><?=$seller['email']?></span>
            </div>
            <div class="part3">
                <span><?=$seller['phone']?></span>
            </div>
        </div>

        <div class="item user shop <?=(($shop['open']!=true) ? 'off' : '')?>">
            <div class="rate">
                <div style="width:<?=$shop['rating']?>%"></div>
                <span><?=$shop['rating']?></span>
            </div>
            <div class="part1">
                <span><?=date('H', $shop['time'])?><sup><?=date('i', $shop['time'])?></sup><br><?=date('d.m.Y',$shop['time'])?></span>
                <span class="cursor" onclick="location.href='/?option=admshop&id=<?= $shop['id']?>'"><?=$shop['count_orders']?></span>
            </div>
            <div class="part2">
                <span class="cursor" onclick="location.href='/?option=admchshop&id=<?= $shop['id']?>'"><?=$shop['name']?><sup><?=$shop['id']?></sup></span>
                <span class="cursor" onclick="location.href='/?option=admmap&id=<?= $shop['id']?>'">Украина, <?=$shop['city']?>, <?=$shop['street_home']?></span>
            </div>
            <div class="part3">
                <span class="cursor" ><?=$shop['count_prices']?></span>
            </div>
        </div>

        <div class="item price">
            <form method="post" id="form-load" action="/?option=admprice">
                <input type="hidden" name="load"/>
                <input type="hidden" name="id_shop" value="<?=intval($_GET['id'])?>"/>
                <input type="button" value="Скачать"  onclick="document.getElementById('form-load').submit()" />
            </form>
            <form method="post" action="/?option=admprice&id=<?=intval($_GET['id'])?>" enctype="multipart/form-data">
                <input type="submit" value="Загрузить"/>
                <input type="hidden" name="id_shop" value="<?=intval($_GET['id'])?>"/>
                <label><input name="ex" type="file">Выбрать</label>
            </form>
        <?php  } ?>
            <?php if (count($this->pds)==0) echo  (($this->page>1) ? 'PAGE_END' : 'Пусто'); ?>
            <?php foreach ($this->pds as $key => $pd ) { ?>
                <div class="table <?=(($pd['quantity']==0) ? 'empty' : '')?>" onclick="setModalData(this)" data-price="<?=$pd['price']?>" data-quantity="<?=$pd['quantity']?>" data-pd-code="<?=$pd['pd_code']?>" data-id-pd="<?=$pd['id']?>" data-brand-name="<?=$pd['brand_name']?>" data-pd-name="<?=$pd['name']?>" data-subgr-name="<?=$pd['subgr_name']?>">
                    <div class="no"><?=($key+1)?></div>
                    <div class="name">
                        <span><?=$pd['brand_name']?></span>
                        <span><?=$pd['name']?></span>
                        <span><?=$pd['subgr_name']?></span>
                    </div>
                    <div class="details">
                        <span><?=$pd['pd_code']?></span>
                        <span id="qtty"><?=$pd['quantity']?></span>
                        <span id="price"><?=$pd['price']?></span>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->page>1) exit; ?>
        </div>

        <div class="priceEdit" id="modalEdit" style="display:none;" >
            <div class="delete" onclick="deletePd()"></div>
            <div class="close" onclick="toggle(modalEdit)"></div>
            <img>
            <div class="specifications">
                <div>
                    <span id="modalBrand"></span>
                    <span id="pd-name" ></span>
                </div>
                <div>
                    <span id="pd-code"></span>
                    <span id="pd-subgr-name"></span>
                </div>
            </div>
            <input required="required" id="qtty" type="text" placeholder="1 шт."/>
            <input required="required" id="price" type="text" placeholder="177,89 грн"/>
            <input type="submit" value="Сохранить" onclick="updatePd()"/>
        </div>

        <script>
            var id_shop =<?=intval($_GET['id'])?>;
            var pd_code;
            var price;
            var quantity;
            $(document).ready(go());


            function setModalData(el) {
                el = $(el);
                pd_code = el.attr('data-pd-code');
                price = el.attr('data-price');
                quantity = el.attr('data-quantity');

                m = $('#modalEdit');
                m.find('img').src='catalog/'+el.attr('data-id-pd')+'.png';
                m.find('#modalBrand').html(el.attr('data-brand-name'));
                m.find('#pd-name').html(el.attr('data-pd-name'));
                m.find('#pd-subgr-name').html(el.attr('data-subgr-name'));
                m.find('#pd-code').html(pd_code);
                m.find('#qtty').val(quantity);
                m.find('#price').val(price);

                toggle(modalEdit);
            }

            function deletePd() {
                the_ajax('delete=on');
            }

            function updatePd() {
                m = $('#modalEdit');
                quantity = m.find('#qtty').val();
                price = m.find('#price').val();
                the_ajax('update=on');
            }

            function the_ajax(label_data) {
                $.post('/?option=admprice', 'id_shop='+id_shop+'&'+label_data+'&pd_code='+pd_code+'&qtty='+quantity+'&price='+price, function () {
                    var el = $('div').find("[data-pd-code='" + pd_code + "']");
                    if (label_data==='delete=on') {
                        el.css('display', 'none');
                    } else {
                        el.find('#qtty').html(quantity);
                        el.find('#price').html(price);
                    }
                    toggle(modalEdit);
                });
            }



            function go() {
                addPaginationNewElementsTo = '.price';
                callBackFunAfterPagination = window['reNumbering'];

                $('body').on("click", "#the_button", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=admsellers', 'id_seller='+th.attr('data-id-seller')+'&the_btn='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                    event.stopPropagation();
                });
            }

            function reNumbering() {
                var number = 1;
                $('body').find('.no').each(function () {
                    $(this).html(number);
                    number++;
                });
            }
        </script>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;

    }








    private function del_upd() {
        $id = $_POST['id_shop'];
        $pd_code = intval($_POST['pd_code']);
        $qtty = intval($_POST['qtty']);
        $price = round(floatval($_POST['price']), 2);

        if (isset($_POST['update']) && $qtty>-1 && $price>-1) {
            $sql ='UPDATE `prices` SET `quantity`='.$qtty.', `price`='.$price;
        } else if (isset($_POST['delete'])) {
            $sql ='DELETE FROM `prices` ';
        }
        $sql.=' WHERE `pd_code`="'.$pd_code.'" AND `id_shop`='.$id;
        $res =$this->db->query($sql);
        exit;
    }


    private function excel() {
        $id = $_POST['id_shop'];
        if ($_FILES['ex']['error']==UPLOAD_ERR_OK && (
                (preg_match("/.xlsx\$/i", $_FILES['ex']['name'])) ||
                (preg_match("/.xlsm\$/i", $_FILES['ex']['name']))  ||
                (preg_match("/.xls\$/i", $_FILES['ex']['name'])) ) &&
            ($_FILES['ex']['size']<=10728640) ) {

            $inputFileType = PHPExcel_IOFactory::identify($_FILES['ex']['tmp_name']);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($_FILES['ex']['tmp_name']);
            $ar = $objPHPExcel->getActiveSheet()->toArray();

            $i=0;
            $res_ar = [];
            foreach($ar as $str) {
                if (intval($str[0])>0 && intval($str[1])>0 && floatval($str[2]) >0 ) {
                    $res_ar[$i]['code']= intval($str[0]);
                    $res_ar[$i]['quantity']= intval($str[1]);
                    $res_ar[$i]['price']= floatval($str[2]);
                    $i++;
                }
            }

            if (count($res_ar)>0) {
                $to_email= array();
                $to_base = array();

                $c_sql='';
                for($i=0; $i<count($res_ar); $i++) {
                    $c_sql.=($i!=0 ? ',' : '').' "'.$res_ar[$i]['code'].'" ';
                }
                $sql ='select `pd_code` from `pd_subgrs` where `pd_code` in ('.$c_sql.') ';
                $res= $this->db->query($sql);

                if ($res->num_rows==0) {
                    foreach($res_ar as $r) {
                        $to_email[]=$r['code'];
                    }
                } else {
                    $i=0;
                    foreach($res_ar as $res_array) {
                        $found=false;
                        foreach($res->rows as $row) {
                            if ($res_array['code']==$row['pd_code']) {
                                $to_base[$i]['code'] = $res_array['code'];
                                $to_base[$i]['quantity'] = $res_array['quantity'];
                                $to_base[$i]['price'] = $res_array['price'];
                                $i++;
                                $found=true;
                                break;
                            }
                        }
                        if ($found==false)
                            $to_email[] = $res_array['code'];
                    }
                }

                if (count($to_base)>0 ) {
                    $c_sql='';
                    for($i=0; $i<count($to_base); $i++) {
                        $c_sql.=($i!=0 ? ' , ' : '').' ('.$id.', "'.intval($to_base[$i]['code']).'", '.intval($to_base[$i]['quantity']).', '.floatval($to_base[$i]['price']).' ) ';
                    }
                    $this->db->query('delete from `prices` where `id_shop`='.$id);
                    $this->db->query('insert into `prices` (`id_shop`,  `pd_code`, `quantity`, `price`) values '.$c_sql);
                    include_once $_SERVER['DOCUMENT_ROOT'].'/classes/shop/info.php';
                    info::tryToActive($id);

                    $res= $this->db->query('SELECT name from files where id_shop='.$id);
                    if ($res->num_rows>0) {
                        unlink($_SERVER['DOCUMENT_ROOT'].'/'.$res->row['name']);
                        $this->db->query('delete from files where id_shop='.$id);
                    }
                }
                $res= $this->db->query('select email from admin_emails where type="new_code"');
                if ((count($to_email)> 0)  && ($res->num_rows>0)) {
                    for($i=0; $i<count($to_email); $i++) {
                        $con.=($i!=0 ? ' , ' : '').' '.$to_email[$i];
                    }
                    $message ='Продавец пытался установить цены на следующие отсутствующие товары (штрих-код): '.$con;
                    $header = 'Рекомендуем добавить эти товары на сайт!';
                    $mail = $res->row['email'];
                    $this->mail->send_mail($mail, $header, $message);
                }
                $_SESSION['msg_ok']='Добавлено товаров: '.count($to_base);

            }  else {
                $_SESSION['msg_error']= 'Пустой файл или содержить некорректные данные';
            }

        } else {
            $_SESSION['msg_error']= 'Некорректный файл или файл весит больше 10мб';
        }
    }

    private function load_ex() {
        $id = $_POST['id_shop'];
        $res= $this->db->query('SELECT pd_code, quantity, price FROM  prices WHERE id_shop='.$id);

        if ($res->num_rows>0) {
            $phpexcel = new PHPExcel();

            $page = $phpexcel->setActiveSheetIndex(0);
            $page->setCellValue("A1", "Code");
            $page->setCellValue("B1", "Quantity");
            $page->setCellValue("C1", "Price");
            for($i=0; $i<$res->num_rows; $i++) {
                $page->setCellValue("A".($i+2), $res->rows[$i]['pd_code']);
                $page->setCellValue("B".($i+2), $res->rows[$i]['quantity']);
                $page->setCellValue("C".($i+2), $res->rows[$i]['price']);
            }

            $page->setTitle("Price list");
            $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
            $rd = abs(rand(111111111111,999999999999));
            $n = "files/admin/excel/".$rd.".xlsx";
            $objWriter->save($n);
            $res= $this->db->query('SELECT name from files where id_shop='.$id);
            if ($res->num_rows>0) {
                unlink($_SERVER['DOCUMENT_ROOT'].'/'.$res->row['name']);
                $this->db->query('delete from files where id_shop='.$id);
            }
            $sql = 'insert into files set `name`="'.$n.'", `id_shop`='.$id.', `time`='.time();
            $this->db->query($sql);
            header('location: /'.$n);
        }	 else {
            $_SESSION['msg_error']= 'Ваш прайс пуст!';
        }
        exit;
    }
}