<?php
class admbuyers extends frame {
    private $buyers;

    public function get_content() {
        if (isset($_POST['the_btn'])) $this->check_opt();

        $this->metaTitle = 'Админ Покупатели | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData() {
        $res = $this->db->query('select * from `buyer`  ORDER BY `id` DESC');
        $this->buyers= $res->rows;
        foreach ($this->buyers as &$buyer) {
            $res = $this->db->query('select COUNT(`id`) AS `count`  FROM `orders` WHERE `confirmed`=1 AND `id_buyer`='.$buyer['id']);
            $buyer['count_orders'] = $res->row['count'];
            $res = $this->db->query('select SUM(`summ`) AS `summ`  FROM `orders` WHERE `confirmed`=1 AND `id_buyer`='.$buyer['id']);
            $buyer['big_summ'] = $res->row['summ'];
        }
        unset($buyer);
    }


    private function showData() {
        ob_start();
        ?>
        <?php foreach ($this->buyers as $buyer) {  ?>
            <div class="item user buyer">
                <label class="checkbox"><input data-id-buyer="<?=$buyer['id']?>" id="the_button" type="checkbox" <?=(($buyer['active']==1) ? 'checked' : '')?> /><div class="checked"></div></label>
                <div class="part1">
                    <span><?=date('H', $buyer['time'])?><sup><?=date('i', $buyer['time'])?></sup><br><?=date('d.m.Y',$buyer['time'])?></span>
                    <span><?=$buyer['big_summ']?></span>
                </div>
                <div class="part2">
                    <span class="cursor" onclick="location.href='/?option=admbuyer&id=<?=$buyer['id']?>'"><?=$buyer['fio']?><sup><?=$buyer['count_orders']?></sup></span>
                    <span><?=$buyer['email']?></span>
                </div>
                <div class="part3">
                    <span><?=$buyer['phone_txt']?></span>
                </div>
            </div>
        <?php } ?>

        <script>
            $(document).ready(go());

            function go() {
                $('body').on("click", "#the_button", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=admbuyers', 'id_buyer='+th.attr('data-id-buyer')+'&the_btn='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                    event.stopPropagation();
                });
            }
        </script>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }


    private function check_opt() {
        $label = intval($_POST['the_btn']);
        $id_buyer = intval($_POST['id_buyer']);
        $this->db->query('UPDATE `buyer` SET `active`='.(($label==1) ? 1 : 0).' WHERE `id`='.$id_buyer);
        exit;
    }
}