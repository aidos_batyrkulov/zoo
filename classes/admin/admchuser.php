<?php
class admchuser extends frame {
    private $seller;

    public function get_content() {
        $this->metaTitle=' Админ Настройки покупателя | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='';

        if ($_SERVER['REQUEST_METHOD']=='POST')
            $this->setData();
        $this->getData();
        echo $this->showData();

    }

    private function setData() {
        $sql = '';
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $sql = ' `email`="'.$this->db->escape($_POST['email']).'" ';
        }
        if (mb_strlen($_POST['phone'])>4 && mb_strlen($_POST['phone'])<20) {
            $sql.= (($sql!='') ? ',' : '' ).' `phone`="'.$this->db->escape($_POST['phone']).'"';
        }
        if (($_POST['pass']===$_POST['pass2']) && (preg_match('/^[a-z0-9]*$/i' ,$_POST['pass'])) && (mb_strlen($_POST['pass'])>=6 && mb_strlen($_POST['pass'])<=32)) {
            $sql.= (($sql!='') ? ',' : '' ).' `password`="'.$this->db->escape($this->security->pass($_POST['pass'])).'"';
        }
        if ($sql!='') {
            $this->db->query('UPDATE `seller` SET '.$sql.' WHERE `id`='.intval($_GET['id']));
        }
    }

    private function getData() {
        $id = intval($_GET['id']);
        $res = $this->db->query('select * from `seller`  WHERE `id`='.$id);
        $this->seller= $res->row;
        if ($res->num_rows==0) header('location: /');
    }


    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <?php $seller = $this->seller;  ?>
        <div class="item user seller">
            <label class="checkbox"><input data-id-seller="<?=$seller['id']?>" id="the_button" type="checkbox" <?=(($seller['active']==1) ? 'checked' : '')?> /><div class="checked"></div></label>
            <div class="part1">
                <span><?=date('H', $seller['time'])?><sup><?=date('i', $seller['time'])?></sup><br><?=date('d.m.Y',$seller['time'])?></span>
                <span><?=$seller['balance']?></span>
            </div>
            <div class="part2">
                <span class="cursor" onclick="location.href='/?option=admseller&id=<?=$seller['id']?>'"><?=$seller['fio']?></span>
                <span><?=$seller['email']?></span>
            </div>
            <div class="part3">
                <span><?=$seller['phone']?></span>
            </div>
        </div>


        <form method="post" action="/?option=admchuser&id=<?=intval($_GET['id'])?>">
            <div class="item settingsContact">
                <input id="phone" name="phone"  type="text" value="<?=$seller['phone']?>"/>
                <input name="email"  type="text" value="<?=$seller['email']?>"/>
            </div>
            <div class="item settingsPassword">
                <input name="pass"  type="password" placeholder="новый пароль"/>
                <input name="pass2" type="password" placeholder="новый пароль (еще раз)"/>
            </div>
            <input type="submit" value="Сохранить изменения"/>
        </form>
            <!-- <<<<< CONTENT <<<<< -->

        <script>
            $( document ).ready(function() {
                go();
            });

            function go() {
                $("#phone").mask("+38(099)999-99-99", {completed: function(){}});
                $('body').on("click", "#the_button", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=admsellers', 'id_seller='+th.attr('data-id-seller')+'&the_btn='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                    event.stopPropagation();
                });
            }
        </script>

        <?php
        $html = ob_get_clean();
        return $html;
    }


}