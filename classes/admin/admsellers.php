<?php
class admsellers extends frame {
    private $sellers;

    public function get_content() {
        if (isset($_POST['the_btn'])) $this->check_opt();

        $this->metaTitle = 'Админ Продавцы | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData() {
        $res = $this->db->query('select * from `seller`  ORDER BY `id` DESC '.$this->getLimitByPage());
        $this->sellers= $res->rows;
        foreach ($this->sellers as &$seller) {
            $res = $this->db->query('select COUNT(`id`) AS `count`  FROM `shop` WHERE `id_seller`='.$seller['id']);
            $seller['count_shop'] = $res->row['count'];
        }
        unset($seller);
    }


    private function showData() {
        ob_start();
        ?>
        <?php if (count($this->sellers)==0) echo  (($this->page>1) ? 'PAGE_END' : 'Пусто'); ?>
        <?php foreach ($this->sellers as $seller) {  ?>
            <div class="item user seller">
                <label class="checkbox"><input data-id-seller="<?=$seller['id']?>" id="the_button" type="checkbox" <?=(($seller['active']==1) ? 'checked' : '')?> /><div class="checked"></div></label>
                <div class="part1">
                    <span><?=date('H', $seller['time'])?><sup><?=date('i', $seller['time'])?></sup><br><?=date('d.m.Y',$seller['time'])?></span>
                    <span><?=$seller['balance']?></span>
                </div>
                <div class="part2">
                    <span class="cursor" onclick="location.href='/?option=admseller&id=<?=$seller['id']?>'"><?=$seller['fio']?><sup><?=$seller['count_shop']?></sup></span>
                    <span><?=$seller['email']?></span>
                </div>
                <div class="part3">
                    <span><?=$seller['phone']?></span>
                </div>
            </div>
        <?php } ?>
        <?php if ($this->page>1) exit; ?>
        <script>
            $(document).ready(go());

            function go() {
                $('body').on("click", "#the_button", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=admsellers', 'id_seller='+th.attr('data-id-seller')+'&the_btn='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                    event.stopPropagation();
                });
            }
        </script>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }


    private function check_opt() {
        $label = intval($_POST['the_btn']);
        $id_seller = intval($_POST['id_seller']);
        $this->db->query('UPDATE `seller` SET `active`='.(($label==1) ? 1 : 0).' WHERE `id`='.$id_seller);
        if ($label==0) {
            $this->db->query('UPDATE `shop` SET `active`=0 WHERE `id_seller`='.$id_seller);
        } else {
            $res = $this->db->query('SELECT `id` FROM `shop` WHERE `id_seller`='.$id_seller);
            include_once $_SERVER['DOCUMENT_ROOT'].'/classes/shop/info.php';
            foreach ($res->rows as $item) {
                info::tryToActive($item['id']);
            }
        }
        exit;
    }
}