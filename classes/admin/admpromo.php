<?php
class admpromo extends frame
{
    private $codes;

    public function get_content(){
        if (isset($_POST['summ'])) $this->genPromo();
        $this->metaTitle = 'Админ Промо-коды | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData(){
        $res = $this->db->query('select * from promo ORDER BY id DESC');
        $this->codes = $res->rows;
        foreach ($this->codes as &$code) {
            if ($code['id_seller']>0) {
                $res = $this->db->query('select `fio`, `phone` from seller WHERE `id`='.$code['id_seller']);
                $code['seller_fio'] = $res->row['fio'];
                $code['seller_phone'] = $res->row['phone'];
            }
        }
        unset($code);
    }


    private function showData()
    {
        ob_start();
        ?>
        <div class="item promoCode">
            <form action="/?option=admpromo" method="post">
                <div>
                    <input name="summ" required="required" type="text" placeholder="00.00"/>
                </div>
                <div>
                    <input type="submit" value="Сгенерировать промо-код"/>
                </div>
            </form>
        </div>

        <div class="item">
        <?php foreach ($this->codes as $code) { ?>
            <div class="transfer">
                <div class="no"><?=$code['id']?></div>
                <div class="name">
                    <?php if (isset($code['seller_fio'])) { ?>
                        <span onclick="location.href='/?option=admseller&id=<?=$code['id_seller']?>'">СПД <?=$code['seller_fio']?></span>
                        <span><?=$code['seller_phone']?></span>
                    <?php } ?>
                </div>
                <div class="details">
                    <span><?=$code['code']?></span>
                    <span><?=$code['summ']?></span>
                </div>
            </div>
        <?php } ?>
        </div>
        <script>
            $(document).ready(go());

            function go() {
            }
        </script>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function genPromo() {
        $promo = rand(111111111,999999999);
        $summ = floatval($_POST['summ']);
        $this->db->query('INSERT INTO `promo` SET `code`="'.$promo.'", `summ`='.$summ.', `time`='.time());
    }

}