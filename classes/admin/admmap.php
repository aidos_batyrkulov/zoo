<?php
class admmap extends frame {
    private $shop;
    private $seller;

    public function get_content() {
        if ($_SERVER['REQUEST_METHOD']=='POST') {$this->setData();}
        $this->metaTitle='Админ Адрес магазина | ZOOSKOP.com - сервис поиска и заказа зоотоваров';
        $this->metaDescription='';

        $this->getData();
        echo $this->showData();
    }

    private function getData() {
        $id = intval($_GET['id']);
        $res = $this->db->query('select * from `shop`  WHERE `id`='.$id);
        if ($res->num_rows==0) header('location: /');
        $this->shop= $res->row;
        $res = $this->db->query('select * from `seller`  WHERE `id`='.$this->shop['id_seller']);
        $this->seller= $res->row;
        $res = $this->db->query('select * FROM `orders` WHERE `confirmed`=1 AND `id_shop`='.$id.' ORDER BY `id` DESC');
        $this->shop['orders'] = $res->rows;
        $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `orders` WHERE  `confirmed`=1 AND  `id_shop`='.$id);
        $this->shop['count_orders'] = $res->row['count'];
        $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `prices` WHERE   `id_shop`='.$id);
        $this->shop['count_prices'] = $res->row['count'];
        $day_n = date('N');
        $time = intval(date('Hi'));
        if ($this->shop['d'.$day_n.'_start']<$time && $this->shop['d'.$day_n.'_end']>$time ) $this->shop['open']= true;
        else $this->shop['open']= false;
    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <?php $seller = $this->seller; $shop = $this->shop; ?>
        <div class="item user seller">
            <label class="checkbox"><input data-id-seller="<?=$seller['id']?>" id="the_button" type="checkbox" <?=(($seller['active']==1) ? 'checked' : '')?> /><div class="checked"></div></label>
            <div class="part1">
                <span><?=date('H', $seller['time'])?><sup><?=date('i', $seller['time'])?></sup><br><?=date('d.m.Y',$seller['time'])?></span>
                <span><?=$seller['balance']?></span>
            </div>
            <div class="part2">
                <span class="cursor" onclick="location.href='/?option=admseller&id=<?=$seller['id']?>'"><?=$seller['fio']?></span>
                <span><?=$seller['email']?></span>
            </div>
            <div class="part3">
                <span><?=$seller['phone']?></span>
            </div>
        </div>

        <div class="item user shop <?=(($shop['open']!=true) ? 'off' : '')?>">
            <div class="rate">
                <div style="width:<?=$shop['rating']?>%"></div>
                <span><?=$shop['rating']?></span>
            </div>
            <div class="part1">
                <span><?=date('H', $shop['time'])?><sup><?=date('i', $shop['time'])?></sup><br><?=date('d.m.Y',$shop['time'])?></span>
                <span class="cursor" onclick="location.href='/?option=admshop&id=<?= $shop['id']?>'"><?=$shop['count_orders']?></span>
            </div>
            <div class="part2">
                <span class="cursor" onclick="location.href='/?option=admchshop&id=<?= $shop['id']?>'"><?=$shop['name']?><sup><?=$shop['id']?></sup></span>
                <span class="cursor" >Украина, <?=$shop['city']?>, <?=$shop['street_home']?></span>
            </div>
            <div class="part3">
                <span class="cursor" onclick="location.href='/?option=admprice&id=<?=$shop['id']?>'"><?=$shop['count_prices']?></span>
            </div>
        </div>


        <form method="post" action="/?option=admmap&id=<?=intval($_GET['id'])?>">
            <script type="text/javascript" src="/js/mapbox.js"></script>
            <link href='/css/mapbox.css' rel='stylesheet' />
            <div class="item addShopMap" style="height: 250px !important;" id="map">

            </div>
            <input type="hidden" name="map_label" id="map_label">
            <div class="item addShopCity">
                <input name="city" id="city" required="required"  placeholder="Город" type="text" value="<?=$shop['city']?>"/>
            </div>
            <div class="item addShopAddress">
                <textarea name="street_home" id="str_home" placeholder="Адрес магазина" required="required" maxlength="200"><?=$shop['street_home']?></textarea>
            </div>
            <input type="submit" value="Сохранить изменения"/>
        </form>
        <!-- <<<<< CONTENT <<<<< -->


        <script>
            $( document ).ready(function() {
                go();
                var lngLat;
                var m =0;

                mapboxgl.accessToken = 'pk.eyJ1Ijoiem9vc2tvcCIsImEiOiJjam92bmp1MnAwamdoM3BvZDJhNWtsM2NhIn0.J4nUkH5CmjS6Z7bFmaQF2w';

                var map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/light-v9',
                    center: [<?=$shop['map_label']?>],
                    zoom: 7
                });


                map.on('mousemove', function (e) {
                    lngLat = e.lngLat;
                });

                $('body').on("click", "#map", function(event){
                    // lngLat is like "LngLat(-23.3443,34.3434)" so we have to do like  "-23.3443,34.3434"
                    var newArr = String(lngLat).split(',');
                    var newArr2 = newArr[0].split('(');
                    var newArr3 = newArr[1].split(')');
                    var newArr4 = newArr3[0].split(' ');
                    var my_lng_lat =  newArr2[1]+','+newArr4[1];
                    $('#map_label').val(my_lng_lat);

                    var el = document.createElement('div');
                    el.className = 'marker';
                    if (m!=0) {m.remove();}
                    m = new mapboxgl.Marker(el);
                    m.setLngLat(lngLat);
                    m.addTo(map);

                    show_adr(my_lng_lat);
                });

                function show_adr(lngLat) {
                    var arr;
                    $.post('/map.php', 'map_label='+lngLat, function(data) {
                        if ((data.indexOf('CANNOT_READ_ADR')+1)==0) {
                            arr = $.parseJSON(data);
                            $('#city').val(arr.city);
                        } else {
                            $('#city').val('');
                            $('#city').attr('placeholder','Заполните вручную');
                        }
                    }, 'text');
                }

                //
                var el = document.createElement('div');
                el.className = 'marker';
                if (m!=0) {m.remove();}
                m = new mapboxgl.Marker(el);
                m.setLngLat([<?=$shop['map_label']?>]);
                m.addTo(map);
            });




            function go() {
                $('body').on("click", "#the_button", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=admsellers', 'id_seller='+th.attr('data-id-seller')+'&the_btn='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                    event.stopPropagation();
                });
            }
        </script>
        <?php
        $html = ob_get_clean();
        return $html;
    }

    private function setData() {
        if ((!empty($_POST['map_label']))
            && (!empty($_POST['city']))
            && (!empty($_POST['street_home']))) {

            $arr = explode(',', $_POST['map_label']);
            $lng = floatval($arr[0]);
            $lat = floatval($arr[1]);
            $this->db->query('UPDATE `shop` SET  `map_label`="' . $lng . ',' . $lat . '",  `city`="' . $this->db->escape($_POST['city']) . '",  `street_home`="' . $this->db->escape($_POST['street_home']) . '" WHERE `id`='.intval($_GET['id']));
        }
    }

}