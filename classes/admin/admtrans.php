<?php
class admtrans extends frame{
    private $histories;

    public function get_content(){
        $this->metaTitle = 'Админ Платежи | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData(){
        $res = $this->db->query('select * from `balance_history` WHERE `type`="+" ORDER BY id DESC');
        $this->histories = $res->rows;
        foreach ($this->histories as &$history) {
            $res = $this->db->query('select `fio`, `phone` from seller WHERE `id`='.$history['id_seller']);
            $history['seller_fio'] = $res->row['fio'];
            $history['seller_phone'] = $res->row['phone'];
        }
        unset($history);
    }


    private function showData()
    {
        ob_start();
        ?>
        <div class="item">
            <?php foreach ($this->histories as $history) { ?>
                <div class="transfer">
                    <div class="no"><?=$history['id']?></div>
                    <div class="name">
                        <span onclick="location.href='/?option=admseller&id=<?=$history['id_seller']?>'">СПД <?=$history['seller_fio']?></span>
                        <span><?=$history['seller_phone']?></span>
                    </div>
                    <div class="details">
                        <span><?=date('H:i  d.m.Y', $history['time'])?></span>
                        <span><?=$history['summ']?></span>
                    </div>
                </div>
            <?php } ?>
        </div>
            <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }

}