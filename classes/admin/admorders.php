<?php
class admorders extends frame {
    private $orders;

    public function get_content() {

        $this->metaTitle = 'Админ Заказы | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData() {
        $res = $this->db->query('select * from orders WHERE `confirmed`=1 ORDER BY `id` DESC ');
        $this->orders= $res->rows;
        foreach ($this->orders as &$order) {
            $res= $this->db->query('SELECT `id`,`id_seller`, `name`, `country`, `city`, `street_home` FROM `shop` WHERE `id`='.$order['id_shop']);
            $order['shop_data'] = $res->row;
            $b_res= $this->db->query('SELECT `phone_txt` FROM `buyer` WHERE `id`='.$order['id_buyer']);
            $order['buyer_phone'] = $b_res->row['phone_txt'];
            $this->initData($order);
            $res = $this->db->query('select * from order_pds where id_order='.$order['id']);
            $order['pds'] = $res->rows;
            $order['dis_big_summ'] = 0;
            $order['big_summ']=0;
            // get pd name,  option name, brand name, id and set big summ
            foreach ($order['pds'] as &$pd) {
                $res = $this->db->query('select name from `group` where id=(select id_subgr from pd_subgrs where pd_code="'.$pd['pd_code'].'")');
                $pd['subgr_name'] = $res->row['name'];
                $res = $this->db->query('select id,name,photo from `product` where id=(select id_pd from pd_subgrs where pd_code="'.$pd['pd_code'].'")');
                $pd['name'] = $res->row['name'];
                $pd['id']= $res->row['id'];
                $pd['photo']= $res->row['photo'];
                $res = $this->db->query('select name from `brand` where id=(select brand from product where id=(select id_pd from pd_subgrs where pd_code="'.$pd['pd_code'].'"))');
                $pd['brand_name'] = $res->row['name'];
                $pd['summ'] = bcmul($pd['price'], $pd['quantity'],2);
                $order['big_summ'] = bcadd($order['big_summ'], $pd['summ'],2);
            }
            unset($pd);
            // if there is a discount
            if ($order['discount']>0) {
                $v = bcdiv($order['big_summ'], 100,2);
                $v2 = bcmul($v,$order['discount'], 2 );
                $order['dis_big_summ'] =  bcsub($order['big_summ'], $v2,2);
            }
        }
        unset($order);
    }

    // The func is used by getData()
    private function initData(&$order) {
        $delText='';
        switch ($order['delType']) {
            case 'ex': $delText='Экспресс доставка '.(($order['delPrice']==0) ? '(бесплатно)' : '('.$order['delPrice'].' грн)');break;
            case 'td': $delText='Доставка сегодня '.(($order['delPrice']==0) ? '(бесплатно)' : '('.$order['delPrice'].' грн)');break;
            case 'tm': $delText='Доставка на завтра '.(($order['delPrice']==0) ? '(бесплатно)' : '('.$order['delPrice'].' грн)');break;
            case 'ukr': $delText='Пересылка по Украине '.(($order['delPrice']==0) ? '(бесплатно)' : '(тариф + '.$order['delPrice'].' грн)');break;
            case 'shop': $delText='Самовывоз';break;
        }
        $delText.=' из магазина '.$order['shop_data']['name'];
        $adr ='';
        if ($order['delType']== 'shop') $adr = $order['shop_data']['city'].', '.$order['shop_data']['street_home'];
        else $adr = $order['city'].', '.$order['street'].', '.$order['home_number'];
        $order['adr'] = $adr;
        $order['delText'] = $delText;
        $order['hour'] = date('H', $order['time']);
        $order['minute'] = date('i', $order['time']);
    }

    private function showData() {
        ob_start();
        ?>
        <!-- >>>>> CONTENT >>>>> -->
        <?php foreach ($this->orders as $order) {?>
            <div data-id-order="<?=$order['id']?>" class="item <?=((date('d.m.Y')!=date('d.m.Y', $order['time'])) ? 'old' : '')?>" >
                <div class="orderContact">
                    <div class="time">
                        <span><?=$order['hour']?><sup><?=$order['minute']?></sup><br><?=date('d.m.Y',$order['time'])?></span>
                        <span  onclick="location.href='/?option=admshop&id=<?=$order['shop_data']['id']?>'"><?=$order['id']?></span>
                    </div>
                    <div class="order delivery">
                        <span>Украина, <?=$order['adr']?></span>
                        <span><?=$order['delText']?></span>
                    </div>
                    <div class="order phone">
                        <span><?=$order['buyer_phone']?></span>
                        <span onclick="location.href='/?option=admbuyer&id=<?=$order['id_buyer']?>'" ><?=$order['buyer_name']?></span>
                    </div>
                </div>
                <?php foreach ($order['pds'] as $pd) { ?>
                    <div class="orderProduct">
                        <img src="catalog/<?=$pd['photo']?>.png">
                        <div class="specifications">
                            <div>
                                <span><?=$pd['brand_name']?></span>
                                <span><?=$pd['name']?></span>
                            </div>
                            <div>
                                <span><?=$pd['pd_code']?></span>
                                <span><?=$pd['subgr_name']?></span>
                            </div>
                        </div>
                        <div class="cost">
                            <span><?=$pd['price']?></span>
                            <span><?=$pd['quantity']?></span>
                            <span><?=$pd['summ']?></span>
                        </div>
                    </div>
                <?php } ?>
                <div class="orderTotal">
                    <?php if ($order['dis_big_summ']>0) { ?>
                        <span class="old"><?=$order['big_summ']?></span>
                        <span><?=$order['dis_big_summ']?></span>
                    <?php } else { ?>
                        <span><?=$order['big_summ']?></span>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <script>

        </script>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;

    }
}