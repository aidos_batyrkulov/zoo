<?php
class admseller extends frame {
    private $seller;

    public function get_content() {
        $this->metaTitle = 'Админ Продавец | ZOOSKOP.com';
        $this->metaDescription = '';

        $this->getData();
        echo $this->showData();

    }


    private function getData() {
        $id = intval($_GET['id']);
        $res = $this->db->query('select * from `seller`  WHERE `id`='.$id);
        $this->seller= $res->row;
        $res = $this->db->query('select *  FROM `shop` WHERE `id_seller`='.$id.' ORDER BY `id` DESC');
        $this->seller['count_shop'] = $res->num_rows;
        $this->seller['shops'] = $res->rows;
        $day_n = date('N');
        $time = intval(date('Hi'));
        foreach ($this->seller['shops'] as &$shop) {
            $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `orders` WHERE  `confirmed`=1 AND `id_shop`='.$shop['id']);
            $shop['count_orders'] = $res->row['count'];
            $res = $this->db->query('SELECT COUNT(`id`) AS `count` FROM `prices` WHERE   `id_shop`='.$shop['id']);
            $shop['count_prices'] = $res->row['count'];
            if ($shop['d'.$day_n.'_start']<$time && $shop['d'.$day_n.'_end']>$time ) $shop['open']= true;
            else $shop['open']= false;
        }
        unset($shop);
    }


    private function showData() {
        ob_start();
        ?>
        <?php $seller = $this->seller; ?>
        <div class="item user seller">
            <label class="checkbox"><input data-id-seller="<?=$seller['id']?>" id="the_button" type="checkbox" <?=(($seller['active']==1) ? 'checked' : '')?> /><div class="checked"></div></label>
            <div class="part1">
                <span><?=date('H', $seller['time'])?><sup><?=date('i', $seller['time'])?></sup><br><?=date('d.m.Y',$seller['time'])?></span>
                <span><?=$seller['balance']?></span>
            </div>
            <div class="part2">
                <span class="cursor" onclick="location.href='/?option=admchuser&id=<?=$seller['id']?>'"><?=$seller['fio']?><sup><?=$seller['count_shop']?></sup></span>
                <span><?=$seller['email']?></span>
            </div>
            <div class="part3">
                <span><?=$seller['phone']?></span>
            </div>
        </div>

        <?php foreach ($seller['shops'] as $shop) {  ?>
            <div class="item user shop <?=(($shop['open']!=true) ? 'off' : '')?>">
                <div class="rate">
                    <div style="width:<?=$shop['rating']?>%"></div>
                    <span><?=$shop['rating']?></span>
                </div>
                <div class="part1">
                    <span><?=date('H', $shop['time'])?><sup><?=date('i', $shop['time'])?></sup><br><?=date('d.m.Y',$shop['time'])?></span>
                    <span class="cursor" onclick="location.href='/?option=admshop&id=<?= $shop['id']?>'"><?=$shop['count_orders']?></span>
                </div>
                <div class="part2">
                    <span class="cursor" onclick="location.href='/?option=admchshop&id=<?= $shop['id']?>'"><?=$shop['name']?><sup><?=$shop['id']?></sup></span>
                    <span class="cursor" onclick="location.href='/?option=admmap&id=<?= $shop['id']?>'">Украина, <?=$shop['city']?>, <?=$shop['street_home']?></span>
                </div>
                <div class="part3">
                    <span class="cursor" onclick="location.href='/?option=admprice&id=<?=$shop['id']?>'"><?=$shop['count_prices']?></span>
                </div>
            </div>
        <?php } ?>

        <script>
            $(document).ready(go());

            function go() {
                $('body').on("click", "#the_button", function(event){
                    event.preventDefault();
                    th = $(this);
                    var active =0;
                    if (th.is(':checked')) active=1;
                    $.post('/?option=admsellers', 'id_seller='+th.attr('data-id-seller')+'&the_btn='+active, function () {
                        if (active===1) th.prop('checked', true);
                        else th.prop('checked', false);
                    });
                    event.stopPropagation();
                });
            }
        </script>
        <!-- <<<<< CONTENT <<<<< -->
        <?php
        $html = ob_get_clean();
        return $html;
    }

}