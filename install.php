<?php
include 'libs/config.class.php';
$mi= new mysqli(config::$db_host, config::$db_user, config::$db_password, config::$db_name);

$mi->set_charset(config::$db_charset);


$mi->query('drop table `admin_settings`');
$sql ='
CREATE TABLE `admin_settings` (
`id`  		   INT(11) 			NOT NULL AUTO_INCREMENT,
`data`         VARCHAR(100) 	NOT NULL,
`type`         ENUM("seller_reg_active","buyer_cart_active") 	NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET= utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `seller`');
echo  $mi->error;
$sql ='
CREATE TABLE `seller` (
`id`  		INT(11) 		NOT NULL AUTO_INCREMENT,
`password`	VARCHAR(255)	NOT NULL,
`phone`		VARCHAR(20)		NOT NULL,
`fio`       VARCHAR(100) 	NOT NULL,
`email`     VARCHAR(100) 	NOT NULL,	
`balance`   DECIMAL(12,2)   NOT NULL    DEFAULT 0,
`active`    INT(1)          NOT NULL    DEFAULT 1,
`time` 		INT(11)         NOT NULL,
            
            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `buyer`');
echo  $mi->error;
$sql ='
CREATE TABLE `buyer` (
`id`  		INT(11) 		NOT NULL AUTO_INCREMENT,
`phone_n` 	BIGINT(16)      NOT NULL,
`phone_txt`	VARCHAR(20)		NOT NULL,
`fio`       VARCHAR(100) 	NOT NULL,
`email`     VARCHAR(100) 	NOT NULL    DEFAULT "",	
`summ`      DECIMAL(12,2)   NOT NULL    DEFAULT 0,
`active`    INT(1)          NOT NULL    DEFAULT 1,
`count_ord` INT(11)         NOT NULL    DEFAULT 0,
`time` 		INT(11)         NOT NULL,
            
            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `shop`');
echo  $mi->error;
$sql ='
CREATE TABLE `shop` (
`id`  		INT(11) 		NOT NULL AUTO_INCREMENT,
`id_seller` INT(11) 		NOT NULL,
`password`	VARCHAR(255)	NOT NULL,
`phone`		VARCHAR(32)		NOT NULL,
`name`      VARCHAR(32) 	NOT NULL,
`map_label` VARCHAR(255)	NOT	NULL	DEFAULT "50.45445,50.34534",
`country`   VARCHAR(255)	NOT	NULL	DEFAULT "",
`city`		VARCHAR(255)	NOT	NULL	DEFAULT "",
`street_home` VARCHAR(255)	NOT	NULL	DEFAULT "",
`type`      ENUM("internet","uni") NOT NULL DEFAULT "internet",
`price_on`  INT(1)		    NOT NULL	DEFAULT 1, /* warning: when it`s 0 it does not mean that shop has not set prices */	
`rating`    INT(3)          NOT NULL    DEFAULT 75,
`time`      INT(11)         NOT NULL,
`active`    INT(1)		    NOT NULL	DEFAULT 0, 
        /*if the shop is active, it means:
        1 - at least the shop has one row of the prices table where 
                both price and quantity are more than 0 
        2 - the shop`s price_on field is 1
        3 - the shop has opened all of new orders (except shop type) and able to get new ones
        4 - the shop`s owner`s balance is available to get new orders
        5 - the shop supports at least one of the types of deliveries or the shop`s type is uni
        6 - the shop must work at least one day for a week
        7 - the shop`s owner is active*/	

`ex_radius`    INT(11)  	 NOT NULL    DEFAULT 0,
`ex_min_summ`  DECIMAL(12,2) NOT NULL    DEFAULT 0,
`ex_price`     DECIMAL(12,2) NOT NULL    DEFAULT 0,
`ex_free`      DECIMAL(12,2) NOT NULL    DEFAULT 0,

`td_get_order_until` INT(11) NOT NULL    DEFAULT 0,
`td_min_summ`  DECIMAL(12,2) NOT NULL    DEFAULT 0,
`td_price`     DECIMAL(12,2) NOT NULL    DEFAULT 0,
`td_free`      DECIMAL(12,2) NOT NULL    DEFAULT 0,

`tm_get_order_until` INT(11) NOT NULL    DEFAULT 0,
`tm_min_summ`  DECIMAL(12,2) NOT NULL    DEFAULT 0,
`tm_price`     DECIMAL(12,2) NOT NULL    DEFAULT 0,
`tm_free`      DECIMAL(12,2) NOT NULL    DEFAULT 0,

`ukr_pay`       ENUM("after", "") NOT NULL DEFAULT "",
`ukr_min_summ`  DECIMAL(12,2) NOT NULL    DEFAULT 0,
`ukr_price`     DECIMAL(12,2) NOT NULL    DEFAULT 0,
`ukr_free`      DECIMAL(12,2) NOT NULL    DEFAULT 0,

`d1_start`      INT(11)         NOT NULL    DEFAULT 0,
`d2_start`      INT(11)         NOT NULL    DEFAULT 0,
`d3_start`      INT(11)         NOT NULL    DEFAULT 0,
`d4_start`      INT(11)         NOT NULL    DEFAULT 0,
`d5_start`      INT(11)         NOT NULL    DEFAULT 0,
`d6_start`      INT(11)         NOT NULL    DEFAULT 0,
`d7_start`      INT(11)         NOT NULL    DEFAULT 0,

`d1_end`      INT(11)         NOT NULL    DEFAULT 0,
`d2_end`      INT(11)         NOT NULL    DEFAULT 0,
`d3_end`      INT(11)         NOT NULL    DEFAULT 0,
`d4_end`      INT(11)         NOT NULL    DEFAULT 0,
`d5_end`      INT(11)         NOT NULL    DEFAULT 0,
`d6_end`      INT(11)         NOT NULL    DEFAULT 0,
`d7_end`      INT(11)         NOT NULL    DEFAULT 0,



            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `product`');
echo  $mi->error;
$sql ='
CREATE TABLE `product` (
`id`        INT(11)         NOT NULL AUTO_INCREMENT,
`name`		VARCHAR(255)	NOT NULL,
`des`       text		    NULL,
`id_group`  int(11)         NOT NULL,
`brand`     int(11)	    	NOT	NULL,
`photo`     VARCHAR(20)     NOT NULL,
`level`     DECIMAL(12,5) 	NOT NULL,

            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `pd_subgrs`');
echo  $mi->error;
$sql ='
CREATE TABLE `pd_subgrs` (
`id`        INT(11)         NOT NULL AUTO_INCREMENT,
`pd_code`   VARCHAR(16)     NOT NULL    DEFAULT "",
`id_pd`     int(11)         NOT NULL,
`id_subgr`  int(11)         NOT NULL,

            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;



$mi->query('drop table `pd_subcats`');
echo  $mi->error;
$sql ='
CREATE TABLE `pd_subcats` (
`id`        INT(11)         NOT NULL AUTO_INCREMENT,
`id_pd`     int(11)         NOT NULL,
`id_subcat`  int(11)         NOT NULL,

            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `subcat_subgrs`');
echo  $mi->error;
$sql ='
CREATE TABLE `subcat_subgrs` (
`id`        INT(11)         NOT NULL AUTO_INCREMENT,
`id_subcat` int(11)         NOT NULL,
`id_subgr`  int(11)         NOT NULL,

            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;



$mi->query('drop table `category`');
$sql ='
CREATE TABLE `category` (
`id`  		 INT(11) 		NOT NULL AUTO_INCREMENT,
`name`    	 VARCHAR(255)	NOT NULL,
`id_super`   INT(11) 		NOT NULL,
`level`      DECIMAL(12,5) 	NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `group`');
$sql ='
CREATE TABLE `group` (
`id`  		 INT(11) 		NOT NULL AUTO_INCREMENT,
`name`    	 VARCHAR(255)	NOT NULL,
`id_super`   INT(11) 		NOT NULL,
`level`      DECIMAL(12,5) 	NOT NULL,
`super_for_select` TINYINT(1) 	NOT NULL    DEFAULT 0,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `brand`');
$sql ='
CREATE TABLE `brand` (
`id`  		 INT(11) 		NOT NULL AUTO_INCREMENT,
`name`    	 VARCHAR(255)	NOT NULL,
`level`      DECIMAL(12,5) 	NOT NULL,
`sub_cats`   text       	NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;



$mi->query('drop table `prices`');
$sql ='
CREATE TABLE `prices` (
`id`  		 INT(11) 		NOT NULL AUTO_INCREMENT,
`id_shop`    INT(11) 		NOT NULL,
`pd_code`    VARCHAR(16)    NOT NULL,
`price`  	 DECIMAL(12,2) 	NOT NULL,
`quantity`   INT(11)        NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `files`');
$sql ='
CREATE TABLE `files` (
`id`  		 INT(11) 		NOT NULL AUTO_INCREMENT,
`name`  	 varchar(255)   NOT NULL,
`time` 		 INT(11)        NOT NULL,
`id_shop`    INT(11)        NOT NULL    DEFAULT 0, /*IF THE FILE BELONGS TO ADMIN*/

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `cart`');
$sql ='
CREATE TABLE `cart` (
`id`  		   INT(11) 		 NOT NULL AUTO_INCREMENT,
`cart_code`    VARCHAR(10)   NOT NULL,
`pd_code`      VARCHAR(16)   NOT NULL,
`quantity`     INT(11) 		 NOT NULL           DEFAULT 1,   
`time`         INT(11) 		 NOT NULL, 

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;




$mi->query('drop table `orders`');
$sql ='
CREATE TABLE `orders` (
`id`  		   INT(11) 		 NOT NULL AUTO_INCREMENT,
`id_shop`      INT(11) 		 NOT NULL,
`id_buyer`     INT(11) 		 NOT NULL,
`buyer_name`   VARCHAR(73)   NOT NULL,
`cart_code`    VARCHAR(10)   NOT NULL,
`country`      VARCHAR(255)   NOT NULL  DEFAULT "",
`city`   	   VARCHAR(255)   NOT NULL   DEFAULT "",
`street`  	   VARCHAR(255)   NOT NULL  DEFAULT "",
`home_number`  VARCHAR(255)   NOT NULL  DEFAULT "",
`delType`      varchar(20)   NOT NULL,
`delPrice`     DECIMAL(12,2) NOT NULL,
`summ`         DECIMAL(12,2) NOT NULL,
`discount`     INT(11)       NOT NULL,
`time`         INT(11) 		 NOT NULL, 
`viewed`       INT(1) 		 NOT NULL	default 0, 
`confirmed`    INT(1) 		 NOT NULL	default 0, 
`seller_paid`  DECIMAL(12,2) NOT NULL   DEFAULT 0,
`code_for_com` VARCHAR(10)   NOT NULL, 
`ip`    	   VARCHAR(25)   NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `order_pds`');
$sql ='
CREATE TABLE `order_pds` (
`id`  		   INT(11) 		NOT NULL AUTO_INCREMENT,
`id_order`     INT(11) 		NOT NULL,
`pd_code`      VARCHAR(16)  NOT NULL,
`quantity`     INT(11) 		NOT NULL,   
`price`        DECIMAL(12,2) NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `ip_sms`');
$sql ='
CREATE TABLE `ip_sms` (
`id`  		   INT(11) 			NOT NULL AUTO_INCREMENT,
`ip`     	   VARCHAR(50) 		NOT NULL,
`sms_id`       VARCHAR(255) 	NOT NULL,
`count`        INT(11) 			NOT NULL,
`time`         INT(11) 			NOT NULL,   

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


$mi->query('drop table `admin_emails`');
$sql ='
CREATE TABLE `admin_emails` (
`id`  		   INT(11) 			NOT NULL AUTO_INCREMENT,
`email`        VARCHAR(100) 	NOT NULL,
`type`         ENUM("new_seller","new_code","new_order") 	NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `balance_history`');
$sql ='
CREATE TABLE `balance_history` (
`id`  		   INT(11) 			NOT NULL AUTO_INCREMENT,
`id_seller`    INT(11) 			NOT NULL,
`summ`         DECIMAL(12,2) 	NOT NULL,
`all_balance`  DECIMAL(12,2) 	NOT NULL,
`type`         ENUM("+","-") 	NOT NULL,
`time`  	   INT(11) 			NOT NULL,
`comment`      varchar(255)     NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `discounts`');
$sql ='
CREATE TABLE `discounts` (
`id`  		   INT(11) 			NOT NULL AUTO_INCREMENT,
`id_shop`  	   INT(11) 			NOT NULL,
`discount`     INT(11) 			NOT NULL,
`summ`         DECIMAL(12,2) 	NOT NULL,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `comment`');
echo  $mi->error;
$sql ='
CREATE TABLE `comment` (
`id`  		INT(11) 		NOT NULL AUTO_INCREMENT,
`id_order`  INT(11) 		NOT NULL,
`id_shop`  	INT(11) 		NOT NULL,
`name`	    VARCHAR(255)	NOT NULL    default "",
`comment`   text    		NOT NULL,
`time`      INT(11) 	    NOT NULL,
`ball`      INT(3) 	        NOT NULL,
`shop_name` VARCHAR(32)     NOT NULL,
`shop_adr`  VARCHAR(100)    NOT NULL,

            
            PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;

$mi->query('drop table `promo`');
$sql ='
CREATE TABLE `promo` (
`id`  		   INT(11) 			NOT NULL AUTO_INCREMENT,
`time`  	   INT(11) 			NOT NULL,
`code`     	   VARCHAR(100) 	NOT NULL,
`summ`         DECIMAL(12,2) 	NOT NULL,
`id_seller`    INT(11) 			NOT NULL	DEFAULT 0,

PRIMARY KEY (`id`)
)
			
			ENGINE = MyISAM
			DEFAULT CHARSET=utf8
			COLLATE=utf8_unicode_ci
';
$mi->query($sql);
echo  $mi->error;


echo 'END INSTALL';

include 'installData.php';