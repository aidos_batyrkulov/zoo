<?php
// Класс синглтон
final class db {
    private static $_obj_db;
    private $_obj_mysqli;
    private $_obj_stmt;
    
    private $db_host;
    private $db_user;
    private $db_password;
    private $db_name;
    private $charset;
    
    private function __construct($data) {
        $this->_obj_mysqli= new mysqli($data['db_host'], $data['db_user'], $data['db_password'], $data['db_name']);
        $this->_obj_mysqli->set_charset($data['charset']);
        $this->_obj_stmt=  $this->_obj_mysqli->stmt_init();
        if ($this->_obj_mysqli->connect_error) {
            exit('connect_error: '.$this->_obj_mysqli->connect_error);
        }
    }
    
    // Статическая функция - возвращает объект тек. класса 
    public static function getObject($db_host, $db_user, $db_password, $db_name, $charset) {
        $data['db_host']=$db_host;
        $data['db_user']=$db_user;
        $data['db_password']=$db_password;
        $data['db_name']=$db_name;
        $data['charset']=$charset;
        
        if (empty(self::$_obj_db)) {
            self::$_obj_db=new db($data); 
        }
        return self::$_obj_db;
    }

    public function query($sql) {
        $query_res=$this->_obj_mysqli->query($sql);
        
        if (!$this->_obj_mysqli->errno) {
            if ($query_res instanceof mysqli_result) {
                $data= array();
                
                while ($row= $query_res->fetch_assoc()) {
                    $data[]=$row;
                }
                
                $res= new stdClass();
                $res->num_rows= $query_res->num_rows;
                $res->row= isset($data[0]) ? $data[0] : array();
                $res->rows= $data;
                
                $query_res->close(); 
            }
            else {
                $res=TRUE;
            }
            
        }  
        else {
            $res=array();
            $res['errno']=  $this->_obj_mysqli->errno;
            $res['error']= $this->_obj_mysqli->error;
        }
        
        return $res;
    }
    
    public function escape($value) {
        return $this->_obj_mysqli->real_escape_string($value);
    }
    
    public function affectedRows() {
        return $this->_obj_mysqli->affected_rows;
    }
    
    public function insertId() {
        return $this->_obj_mysqli->insert_id;
    }
    
    public function __destruct() {
        $this->_obj_mysqli->close();
    }
    
    
    
    
}
