<?php 
class security {
	public function pass($pass) {
		return password_hash($pass, PASSWORD_DEFAULT);
	}
	
	public function dehtml($text) {
		return htmlentities($text,ENT_QUOTES, 'UTF-8');
	}
	
	public function pass_verify($pass, $hash) {
		return password_verify($pass, $hash);
	}
	
	
	
}