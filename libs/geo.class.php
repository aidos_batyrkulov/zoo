<?php
class geo {
    public function get_meter($ml_1, $ml_2) {
        // NOTE: in vars $ml_1 and $ml_2 lng  goes first and then lat
        $result = false;

        $ml_1 = explode(',', $ml_1);
        $ml_2 = explode(',', $ml_2);

        if (count($ml_1)==2 && count($ml_2)==2) {
            $point1_lat = $ml_1[1];
            $point1_long = $ml_1[0];
            $point2_lat = $ml_2[1];
            $point2_long = $ml_2[0];
            $unit = 'km';
            $decimals = 3;
            // Calculate the distance in degrees
            $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

            // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
            switch($unit) {
                case 'km':
                    $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                    break;
                case 'mi':
                    $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
                    break;
                case 'nmi':
                    $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
            }
            $result =  round($distance, $decimals); // it's km
            $result = str_replace('.', '', $result);
            $result = intval($result); // now it's meter
        }

        return $result;
    }
}