<?php
if (!function_exists('random_int')) {
    function random_int($min, $max) {
        return mt_rand($min, $max);
    }
}

if (!function_exists('random_bytes')) {
    function random_bytes($len) {
        $random='';
        $radio=TRUE;
        
        for($i=0; $i<$len; $i++) {
            
            if ($radio===TRUE) {
                
            
            $int_1 = mt_rand(1, 26);
            switch ($int_1) {
                case 1: $random.='q';                    break;
                case 2: $random.='w';                    break;
                case 3: $random.='e';                    break;
                case 4: $random.='r';                    break;
                case 5: $random.='t';                    break;
                case 6: $random.='y';                    break;
                case 7: $random.='u';                    break;
                case 8: $random.='i';                    break;
                case 9: $random.='o';                    break;
                case 10: $random.='p';                    break;
                case 11: $random.='a';                    break;
                case 12: $random.='s';                    break;
                case 13: $random.='d';                    break;
                case 14: $random.='f';                    break;
                case 15: $random.='g';                    break;
                case 16: $random.='h';                    break;
                case 17: $random.='j';                    break;
                case 18: $random.='k';                    break;
                case 19: $random.='l';                    break;
                case 20: $random.='z';                    break;
                case 21: $random.='x';                    break;
                case 22: $random.='c';                    break;
                case 23: $random.='v';                    break;
                case 24: $random.='b';                    break;
                case 25: $random.='n';                    break;
                case 26: $random.='m';                    break;
            }
            $radio=FALSE;
            }
            else {
                $int_2= mt_rand(0, 9);
                $random.=$int_2;
                $radio=TRUE;
            }
            
            
        }
        
        return $random;
    }
}