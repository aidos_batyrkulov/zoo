<?php
if(isset($_POST['map_label'])) {
    // map_label is like -23.3443,34.3434
    $arr = explode(',',$_POST['map_label']);
    $lng = floatval($arr[0]);
    $lat = floatval($arr[1]);
    $a = file_get_contents('https://api.mapbox.com/geocoding/v5/mapbox.places/'.$lng.','.$lat.'.json?access_token=pk.eyJ1Ijoiem9vc2tvcCIsImEiOiJjam92bmp1MnAwamdoM3BvZDJhNWtsM2NhIn0.J4nUkH5CmjS6Z7bFmaQF2w');
    $b = json_decode($a, true);

    $city = '';

    if (isset($b['features']) && is_array($b['features'])) {
        foreach ($b['features'] as $feature) {
            if (mb_stripos($feature['id'], 'region')!==false || mb_stripos($feature['id'], 'place')!==false) {
                $city = $feature['text'];
                break;
            }
        }
    }
    if (!empty($city)) {
        echo '{"city":"'.$city.'"}';
    } else {
        echo 'CANNOT_READ_ADR';
    }
    exit;
}