<?php
ini_set('default_charset', 'utf-8');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
//error_reporting(E_ALL & ~E_NOTICE);
error_reporting(E_ALL);
ob_start(null, 0, PHP_OUTPUT_HANDLER_CLEANABLE);
//date_default_timezone_set("Europe/Kiev");
//date_default_timezone_set("Asia/Bishkek");
date_default_timezone_set('Asia/Seoul');
//date_default_timezone_set('America/Los_Angeles');
if (isset($_COOKIE['PHPSESSID'])) {
	session_start();
}

require_once 'classes/frame.php';
require_once("libs/db.class.php");
require_once("libs/mail.class.php");
require_once("libs/config.class.php");
require_once("libs/security.class.php");
require_once("libs/sms.class.php");
require_once("libs/geo.class.php");
require_once("helper/functions.php");

if(isset($_GET['option']) && $_GET['option']) {
	$class = trim(strip_tags($_GET['option']));
} else if (@$_SESSION['role']== 'shop'){
	$class = 'info';
} else  if (@$_SESSION['role']=='user'){
	$class = 'user';
} else if (@$_SESSION['role']=='admin') {
    $class ='admsellers';
} else {
    $class='main';
}
if ($class=='admchshop' || $class=='admchuser' || $class=='admmap' || $class=='admtrans' || $class=='admpromo' || $class=='admbuyer' || $class=='admbuyers' || $class=='admprice' || $class=='admsellers' || $class=='admseller' || $class=='admsettings' || $class=='admorders' || $class=='admshop') {
    $folder='admin';
}

if($class=='main' or $class=='catalog' or  $class=='cart' or $class=='login' or  $class=='register' or $class=='terms' or $class=='about' or $class=='confirm' or $class=='feedback') {
	$folder='buyer';
}

if($class=='addgoods' or $class=='brands' or $class=='orders' or $class=='goods'  or $class== 'info')  {
	$folder= 'shop';
}

if ($class=='addshop' or $class=='balance' or $class=='feedbacks' or $class=='change' or $class=='shops' or $class=='user'  or $class=='settings' ) {
	$folder='user';
}

if ((isset($_SESSION['role']) ) && $folder=='buyer') {
	if ($_SESSION['role']== 'shop'){
		header('location: /?option=orders');
	}  else if ($_SESSION['role']=='user') {header('location: /?option=user');}
	exit;
}
if ((!isset($_SESSION['role'])) && (($folder== 'shop' && ($class!== 'info') ) || $folder=='user')) {
	header('location: /?option=main');
	exit;
}

if ((@$_SESSION['role']== 'shop') && ($folder=='user')) {
	header('location: /?option=orders');
	exit;
}

if ((@$_SESSION['role']=='user') && ($folder== 'shop')) {
    header('location: /?option=user');
    exit;
}

if (@$_SESSION['role']!=='admin' && ($folder=='admin')) {
	header('location: /');
	exit;
}

if(file_exists("classes/".$folder."/".$class.".php")) { 
	///
	include("classes/".$folder."/".$class.".php");
	if(class_exists($class)) {
		
		$obj = new $class;
		$obj->get_body();
	}
	else {
		header('Location: /');
	}
}
else {
	header('Location: /');
}
